function areNumbers() {
  for (let arg of [ ...arguments ])
    if (isNaN( arg ))
      return false;

  return true;
}

function parseInts() {
  return [ ...arguments ]
    .map(n => parseInt(n));
}

module.exports = {
  areNumbers,
  parseInts
};
