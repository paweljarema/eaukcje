// TODO send quick email with file through sengrid
const Mailer = require('../services/Mailer');

function quickEmail(email, title, html, file) {
  let mailer = new Mailer({
    recipients: [{ email }],
    subject: title
  }, html);

  if (file)
    mailer.addFile(file);

  mailer.send();
}

module.exports.quickEmail = quickEmail;
