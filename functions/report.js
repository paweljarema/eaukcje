const { dateRangeMonthsToMillis } = require('./date');

async function getFinancialReport(mongoose, { from, to }) {
  let { dateFrom, dateTo } = dateRangeMonthsToMillis(from, to);

  return await mongoose
    .model('creditTransaction')
    .aggregate([
      {
        $match: {
          date: { $gte: dateFrom, $lte: dateTo },
          done: true
        }
      },
      {
        $group: {
          _id: "$vat",
          totalSum: { $sum: "$total" },
          count: { $sum: 1 }
        }
      },
      {
        $project: {
          _id: 0,
          vat: "$_id",
          count: 1,
          total: { $divide: [ "$totalSum", 100 ] }
        }
      }
    ]);
}

async function getInvoicesForBatchExport(mongoose, { from, to }) {
  let { dateFrom, dateTo } = dateRangeMonthsToMillis(from, to);

  return await mongoose
    .model('invoice')
    .find({ created: { $gte: dateFrom, $lte: dateTo }})
    .select('-html -pdf')
    .lean();
}

module.exports = {
  getFinancialReport,
  getInvoicesForBatchExport
};
