const { areNumbers } = require('./numbers');

function dateRangeMonthsToMillis(from, to) {
  if (areNumbers(from, to)) {
    let
      currentYear = new Date().getFullYear(),
      dateFrom = new Date(currentYear, parseInt(from)).getTime(),
      dateTo = new Date(currentYear, parseInt(to) + 1, 0, 23, 59, 59, 999).getTime();

    return { dateFrom, dateTo, parsed: true };
  } else return { parsed: false };
}

module.exports = {
  dateRangeMonthsToMillis
};
