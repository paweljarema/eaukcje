function saveSettings({ mongoose, settings, res, message }) {
  mongoose
    .model('settings')
    .findOneAndUpdate(
      {},
      { $set: settings },
      { upsert: true },

      function (err, doc) {
        if (err) {
          console.log(err);
          if (res)
            res.send({ error: err });
        } else {
          if (res)
            res.send(message || { created: true });
        }
      }
    );
}

function loadSettings({ mongoose }) {
  return mongoose
    .model('settings')
    .findOne({});
}

module.exports = {
  saveSettings,
  loadSettings
};
