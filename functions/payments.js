const
  business = require('../client/src/constants/business'),
  { newPolmarketInvoiceTo } = require('./invoice');

  /*
    TRANSACTION CODES  -  to change later
    0 - buy credits
    1 - promote ad 7 days
    2 - promote ad forever
  */
function getDescription(transactionCode) {
  switch(parseInt(transactionCode)) {
    case 0:
      return 'Zakup kredytów ' + business.name;
    case 1:
      return 'Promocja ogłoszenia przez 7 dni na ' + business.name;
    case 2:
      return 'Promocja ogłoszenia do końca jego instnienia na ' + business.name;
    default:
      return 'Błędny kod transakcji';
  }
}

async function produceInvoice(mongoose, { transaction, title, qty = 1, total = 0, vat = 23, inflateVat = true }) {
  let
    user = await mongoose.model('user').findById(transaction._user),
    description = title || getDescription(transaction.promoCode),
    products = [{
      name: description,
      qty: parseInt(qty),
      price: total
    }];

  await newPolmarketInvoiceTo(
    user, {
      products,
      description,
      vat,
      total: parseInt(total / 100),
      inflateVat,
      test: false
    }
  );
}

module.exports = function(mongoose) {
  return produceInvoice;
}
