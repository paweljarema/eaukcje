function username(user) {
  return `${ user.firstname || '' } ${ user.lastname || '' }`.trim() || 'Anonim';
}

function useraddress({ address }) {
  let { street, postal, city } = address || {};

  return `${ street || '' }\n${ postal || '' } ${ city || '' }`.trim();
}

function userToInvoice(user) {
  let firm = user.firm || {};
  return {
    fullname: firm.firm_name ? firm.firm_name : username(user),
    firm_name: firm.firm_name || '',
    pesel: user.pesel || '',
    NIP: firm.nip || '',
    address: useraddress(user)
  };
}

module.exports.username = username;
module.exports.useraddress = useraddress;
module.exports.userToInvoice = userToInvoice;
