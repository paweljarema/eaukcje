const
  mongoose = require('mongoose'),
  Invoice = mongoose.model('invoice'),

  { ObjectId } = mongoose.Types,
  { quickEmail } = require('./email'),
  { username, useraddress, userToInvoice } = require('./user'),

  invoiceHtml = require('../client/src/documents/invoice'),
  business = require('../client/src/constants/business'),

  pdf = require('html-pdf'),

  { OPTIONS } = require('../client/src/constants/pdf'),

  INVOICE_PREFIX = 'EAU';
  // sudo apt-get install libfontconfig <-- in linux env. you'll need this for phantom.js to work

async function sendPdf(email, html, invoice) {
  console.log('trying to create pdf.');

  pdf
    .create(html, OPTIONS)
    .toBuffer((err, buffer) => {
      if (err) console.log(err);
      else {
        console.log('trying to send pdf.');
        let base64 = buffer.toString('base64');

        sendInvoice(
          email,
          html,
          invoice,
          {
            name: invoicePdfName(invoice),
            mimetype: 'application/pdf',
            buffer: base64
          }
        );
      }
    });
}

async function sendInvoice(email, html, invoice, file) {
  quickEmail(
    email,
    `Nowa faktura ${ business.name } ${ invoice.number }`,
    html,
    file
  );

  Invoice.findByIdAndUpdate(
    invoice._id,
    {
      $set: {
        html: html,
        sent: true,
        pdf: ( file ? file.buffer : null )
      }
    }
  ).exec();

  console.log('invoice sent.');
}

async function newPolmarketInvoiceTo(user, { products, description, vat, total, inflateVat, test }) {
  let
    { invoiceNumber, invoice } = await newInvoice({ _user: user._id, description, vat, total, test, user }),

    data = {
      seller: polmarket(),
      buyer: userToInvoice(user),
      products: inflateVat && vat ? inflateProductVat(products, vat) : products,
      invoiceNumber: invoiceNumber
    },

    html = invoiceHtml(data);

  if (!test) {
    // sendInvoice(user.contact.email, html, invoice);
    sendPdf(user.contact.email, html, invoice);
  } else {
    // console.log(invoice);
  }

  return html;
}

async function newInvoice({ _user, description, vat, total, test, user }) {
  let
    lastInvoice = await Invoice.findOne({}).sort({ created: -1 }),
    lp;

  if (lastInvoice) {
    let
      yearNow = new Date().getFullYear(),
      invoiceYear = new Date(lastInvoice.created).getFullYear(),
      newYear = yearNow !== invoiceYear;

    if (newYear)
      lp = 1;
    else
      lp = lastInvoice.lp + 1;

  } else
    lp = 1;

  let
    invoiceNumber = getInvoiceNumber(lp),
    invoice = new Invoice({
      _user: _user,
      lp: lp,
      number: invoiceNumber,
      vat: vat,
      total: total,
      description: description,

      firstname: user.firstname,
      lastname: user.lastname,
      firm: user.firm,
      address: user.address
    });

  if (!test)
    await invoice.save();

  return { invoiceNumber, invoice };
}

function inflateProductVat(products, vat) {
  return products.map(({ name, qty, price }) => ({
    name,
    qty,
    price: price / ( 1 + ( Number(vat) / 100 )),
    vat: vat
  }));
}

function polmarket() {
  return {
    fullname: '',
    firm_name: business.invoice.firm,
    NIP: business.invoice.NIP,
    address: business.invoice.address
  };
}

function getInvoiceNumber(lp) {
  let date = new Date();
  return `${ INVOICE_PREFIX }/${ lp }/${ leadingZero( date.getMonth() + 1 ) }/${ date.getFullYear() }/`;
  // return `${ String(date.getFullYear()).slice(-2) }/${ leadingZero( date.getMonth() + 1 ) }/${ leadingZero( date.getDate() ) }/${ leadingZero( lp ) }`;
  // leadingZeroes( lp, 6 )
}

function leadingZero(num) {
  if (num < 10) return '0' + num;
  return num;
}

function symfonyDate(invoice) {
  let date = new Date(invoice.created);
  return `${ date.getFullYear() }-${ leadingZero( date.getMonth() + 1 ) }-${ leadingZero( date.getDate() ) }`;
}

function leadingZeroes(number, places) {
  let
    string = String(number),
    zeroes = places - string.length;

  return '0'.repeat(zeroes) + string;
}

function fileFriendlyNumber(number) {
  return number.replace(/\/|\s+/g, '_');
}

function invoicePdfName({ number }) {
  return ('faktura_' + fileFriendlyNumber(number) + '.pdf');
}

function invoiceSymfoniaName({ number }) {
  return ('symfonia_eksport_' + fileFriendlyNumber(number) + '.txt');
}

module.exports.invoicePdfName = invoicePdfName;
module.exports.newPolmarketInvoiceTo = newPolmarketInvoiceTo;
module.exports.newInvoice = newInvoice;
module.exports.sendInvoice = sendInvoice;
module.exports.polmarket = polmarket;
module.exports.userToInvoice = userToInvoice;
module.exports.symfonyDate = symfonyDate;
module.exports.invoiceSymfoniaName = invoiceSymfoniaName;
