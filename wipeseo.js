const
  mongoose = require('mongoose'),
  keys = require('./config/keys'),
  wipeseo = require('./routes/functions/wipeseoFunctions');

mongoose.connect(keys.mongoURI);

wipeseo();
