const
  iconv = require('iconv-lite'),
  business = require('../client/src/constants/business'),

  { symfonyDate } = require('../functions/invoice'),
  { userfirmname, usercity, userstreet, userhouse, userapartment, userpostcode, usernip } = require('../client/src/functions/user'),

  CHARS = {
    newLine: '\n',
    tab: '\t',
    delimiters: { start: '{', end: '}' }
  },

  CONSTANTS = {
    lang: 'PL',
    country: 'Polska',
    active: 1,
    kindname: 'Kontrahenci',
    cataloguename: '\\@Kontrahenci',
    cancelled: 0,
    documentkind: 'sprzedaż',
    topay: 0,
    settlement: 0,
    symbol: 'FVS',
    vat: 0,
    wn: 'WN',
    ma: 'MA',
    rejvat: 'rejestr VAT sprzedaży'
  },

  INFO = 'INFO',
  SINGULAR_FIELDS = ['Kontrahent', 'Dokument'],
  DOCUMENT_FIELDS = ['Dane nabywcy', 'Zapis', 'Rejestr', 'Transakcja'],

  INFO_DATA = (
    newValue('Nazwa programu') +
    newValue('Wersja_programu', 1) +
    newValue('wersja szablonu', 3.1) +
    newEntry(SINGULAR_FIELDS[0], (
      newValue('id', '', 2) +
      newValue('kod', business.invoice.name, 2) +
      newValue('nazwa', business.invoice.firm, 2)
    ), 1)
  );


function newValue(name, value = '', level = 1) {
  return CHARS.tab.repeat(level) + `${ name } = ${ value }` + CHARS.newLine;
}

function newEntry(name, content = '\n', level = 0) {
  return (
    CHARS.tab.repeat(level) +
    name +
    CHARS.delimiters.start + CHARS.newLine +
    content +
    CHARS.tab.repeat(level) + CHARS.delimiters.end + CHARS.newLine
  );
}

function newKontrahent(user) {
  let
    name = userfirmname(user),
    city = usercity(user),
    street = userstreet(user),
    house = userhouse(user),
    apartment = userapartment(user),
    postcode = userpostcode(user),
    nip = usernip(user);

  return newEntry(SINGULAR_FIELDS[0], (
    newValue('id') +
    newValue('kod', name) +
    newValue('nazwa', name) +
    newValue('miejscowosc', city) +
    newValue('ulica', street) +
    newValue('dom', house) +
    newValue('lokal', apartment) +
    newValue('kodpocz', postcode) +
    newValue('nip', nip) +
    newValue('region') +
    newValue('krajKod', CONSTANTS.lang) +
    newValue('krajNazwa', CONSTANTS.country) +
    newValue('aktywny', CONSTANTS.active) +
    newValue('NazwaRodzaju', CONSTANTS.kindname) +
    newValue('NazwaKatalogu', CONSTANTS.cataloguename)
  ));
}

function newDokument(invoice) {
  let
    date = symfonyDate(invoice),
    name = userfirmname(invoice),
    city = usercity(invoice),
    street = userstreet(invoice),
    house = userhouse(invoice),
    apartment = userapartment(invoice),
    postcode = userpostcode(invoice),
    nip = usernip(invoice),

    { total, net, vat, vatvalue } = stripPrice(invoice);

  return newEntry(SINGULAR_FIELDS[1], (
    newValue('anulowany', CONSTANTS.cancelled) +
    newValue('rodzaj_dok', CONSTANTS.documentkind) +
    newValue('id', invoice.number) +
    newValue('kwota', invoice.total) +
    newValue('dozaplaty', CONSTANTS.topay) +
    newValue('rozrachunek', CONSTANTS.settlement) +
    newValue('obsluguj jak', CONSTANTS.symbol) +
    newValue('symbol FK', CONSTANTS.symbol) +
    newValue('data', date) +
    newValue('datasp', date) +
    newValue('opis', invoice.description) +
    newValue('opis fk', invoice.description) +
    newValue('datarej', date) +
    newValue('FK nazwa', invoice.number) +
    newValue('kod', invoice.number) +
    newValue('naliczenie_VAT', CONSTANTS.vat) +
    newEntry(DOCUMENT_FIELDS[0], (
      newValue('khid', '', 2) +
      newValue('fk_ident', '', 2) +
      newValue('khkod', name, 2) +
      newValue('khnazwa', name, 2) +
      newValue('khmiasto', city, 2) +
      newValue('khadres', street, 2) +
      newValue('khdom', house, 2) +
      newValue('khlokal', apartment, 2) +
      newValue('khkodpocz', postcode, 2) +
      newValue('khnip', nip, 2)
    ), 1) +
    newEntry(DOCUMENT_FIELDS[1], (
      newValue('strona', CONSTANTS.wn, 2) +
      newValue('kwota', total, 2) +
      newValue('konto', 200, 2) +
      newValue('IdDlaRozliczen', 1, 2) +
      newValue('opis', invoice.description, 2) +
      newValue('NumerDok', invoice.number, 2) +
      newValue('pozycja', 0, 2) +
      newValue('TypRozb', 0, 2)
    ), 1) +
    newEntry(DOCUMENT_FIELDS[1], (
      newValue('strona', CONSTANTS.ma, 2) +
      newValue('kwota', net, 2) +
      newValue('konto', 702, 2) +
      newValue('IdDlaRozliczen', 2, 2) +
      newValue('opis', invoice.description, 2) +
      newValue('NumerDok', invoice.number, 2) +
      newValue('pozycja', 0, 2) +
      newValue('TypRozb', 0, 2)
    ), 1) +
    newEntry(DOCUMENT_FIELDS[1], (
      newValue('strona', CONSTANTS.ma, 2) +
      newValue('kwota', vatvalue, 2) +
      newValue('konto', '221-1', 2) +
      newValue('IdDlaRozliczen', 3, 2) +
      newValue('opis', invoice.description, 2) +
      newValue('NumerDok', invoice.number, 2) +
      newValue('pozycja', 0, 2) +
      newValue('TypRozb', 0, 2)
    ), 1) +
    newEntry(DOCUMENT_FIELDS[2], (
      newValue('Skrot', CONSTANTS.rejvat, 2) +
      newValue('Nazwa', CONSTANTS.rejvat, 2) +
      newValue('Rodzaj', 1, 2) +
      newValue('ABC', 1, 2) +
      newValue('Naliczony', 0, 2) +
      newValue('datarej', date, 2) +
      newValue('okres', date, 2) +
      newValue('brutto', total, 2) +
      newValue('netto', net, 2) +
      newValue('vat', vatvalue, 2) +
      newValue('stawka', vat, 2) +
      newValue('sumanetto', net, 2) +
      newValue('sumavat', vatvalue, 2)
    ), 1) +
    newEntry(DOCUMENT_FIELDS[3], (
      newValue('IdDlaRozliczen', -1, 2) +
      newValue('termin', date, 2)
    ), 1)
  ));
}

function exportInvoice(invoice) {
  let doc = '';

  doc += newKontrahent(invoice);
  doc += newDokument(invoice);

  return doc;
}

class Symfonia {
  constructor(invoices, options) {
    // do stuff
    this.invoices = invoices;
  }

  toString() {
    if (!this.invoices) throw 'No invoices.';
    let
      doc = '',
      len = this.invoices.length;

    doc += newEntry(INFO, INFO_DATA);

    for (let i = 0; i < len; i++)
      doc += exportInvoice(this.invoices[i]);

    return doc;
  }

  sendFileViaExpress(res, name = 'symfonia-export.txt') {
    if (!res) throw 'Pass a res object.';

    let
      buf = iconv.encode(
        this.toString(),
        'win1250'
      );

    res.contentType('text/plain');
    res.set({
      'Content-Type': 'text/plain; charset=windows-1250',
      'Content-Disposition': `attachment; filename="${ name }"`
    });

    res.send(buf);
  }
}

function stripPrice({ total, vat }) {
  let
    net = Number(total) / (vat / 100 + 1),
    vatvalue = Number(total) - net;

  return { total, net, vat, vatvalue };
}

module.exports = Symfonia;
