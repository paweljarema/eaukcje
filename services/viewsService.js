const
	mongoose = require('mongoose'),
	Auction = mongoose.model('auction'),
	Views = mongoose.model('views'),

	uuid 		 = require('uuid/v4'),

	{ ObjectId } = mongoose.Types,
	{ timestampQuery } = require('../routes/functions/filterFunctions');


const viewers = {};

function sessionUid(req) {
	if (req.user) {
		return String(req.user._id);
	} else {
		if (req.session.uid) {
			return req.session.uid;
		} else {
			req.session.uid = uuid();
			return req.session.uid;
		}
	}
}

function getViewers(auction_id) {
	if (viewers[auction_id]) {
		return viewers[auction_id].length;
	} else {
		return 0;
	}
}

function addViewer(auction_id, uid) {
	const entry = viewers[auction_id];
	if (entry) {
		if (entry.indexOf(uid) === -1) entry.push(uid);
	} else {
		viewers[auction_id] = [uid];
	}
}

function removeViewer(auction_id, uid) {
	const
		entry = viewers[auction_id],
		index = entry ? entry.indexOf(uid) : -1;

	if (index !== -1) viewers[auction_id].splice(index, 1);
}

module.exports = app => {

	app.get('/api/views_via_shortid/:shortid', async (req, res) => {
		res.send({ viewers: getViewers(req.params.shortid) })
	});

	app.post('/api/register_viewer_via_shortid/:shortid', async (req, res) => {
		const { shortid } = req.params;

		addViewer(shortid, sessionUid(req));
		res.send({ viewers: getViewers(shortid) });
	});

	app.post('/api/unregister_viewer_via_shortid/:shortid', async (req, res) => {
		const { shortid } = req.params;

		removeViewer(shortid, sessionUid(req));
		res.send({ viewers: getViewers(shortid) });
	});

	app.get('/api/get_total_views_via_shortid/:shortid', async(req, res) => {
		const
			auction = await Auction
				.findOne({ _shortid: req.params.shortid })
				.select('_id')
				.lean();

		if (!auction) {
			res.send(false);
			return;
		}

		const
				views = await Views
					.findOne({ _auction: auction._id })
					.select('views')
					.lean();

		res.send({ views: (views ? views.views : 0) });
	});

	app.post('/api/add_view_for_via_shortid/:shortid', async (req, res) => {
		const
			auction = await Auction
				.findOne({ _shortid: req.params.shortid })
				.select('_id')
				.lean();

		if (!auction) {
			res.send(false);
			return;
		}

		const
			doc = await Views
				.findOne({ _auction: auction._id })
				.lean(),

			views = doc ? doc.views : 0;

		if (views) {
			Views.updateOne({ _auction: auction._id }, { $inc: { views: 1 }}).exec();
		} else {
			new Views({ _auction: auction._id, views: 1 }).save();
		}

		res.send(true);
	});

	// LEGACY
	// app.get('/api/views/:auction_id', async (req, res) => {
	// 	const { auction_id } = req.params;
	//
	// 	res.send({ viewers: getViewers(auction_id) });
	// });
	//
	// app.get('/api/views_by_timestamp/:timestamp', async (req, res) => {
	// 	const { timestamp } = req.params;
	// 	res.send({ viewers: getViewers(timestamp) });
	// })

	// LEGACY
	// app.post('/api/register_viewer/:auction_id', async (req, res) => {
	// 	const
	// 		{ auction_id } 	= req.params,
	// 		uid 			= sessionUid(req);
	//
	// 	addViewer(auction_id, uid);
	// 	res.send({ viewers: getViewers(auction_id) });
	// });
	//
	// app.post('/api/register_viewer_by_timestamp/:timestamp', async (req, res) => {
	// 	const
	// 		{ timestamp } = req.params,
	// 		uid = sessionUid(req);
	//
	// 	addViewer(timestamp, uid);
	// 	res.send({ viewers: getViewers(timestamp) });
	// });

	// app.post('/api/unregister_viewer_by_timestamp/:timestamp', async (req, res) => {
	// 	const
	// 		{ timestamp } = req.params,
	// 		uid = sessionUid(req);
	//
	// 	removeViewer(timestamp, uid);
	// 	res.send({ viewers: getViewers(timestamp) })
	// });

	// LEGACY
	// app.post('/api/unregister_viewer/:auction_id', async (req, res) => {
	// 	const
	// 		{ auction_id }	= req.params,
	// 		uid 			= sessionUid(req);
	//
	// 	removeViewer(auction_id, uid);
	// 	res.send({ viewers: getViewers(auction_id) });
	// });
	//
	// app.get('/api/get_total_views_by_timestamp/:timestamp', async(req, res) => {
	// 	const
	// 		timestamp = req.params.timestamp,
	// 		auction = await Auction
	// 			.findOne(timestampQuery(timestamp))
	// 			.select('_id')
	// 			.lean();
	//
	// 	if (!auction) {
	// 		res.send(false);
	// 		return;
	// 	}
	//
	// 	const
	// 			views = await Views
	// 				.findOne({ _auction: auction._id })
	// 				.select('views')
	// 				.lean();
	//
	// 	res.send({ views: (views ? views.views : 0) });
	// });

	// LEGACY
	// app.post('/api/add_view_for/:auction_id', async (req, res) => {
	// 	const
	// 		{ auction_id } 	= req.params,
	// 		_id			   	= ObjectId(auction_id),
	// 		doc 			= await Views.findOne({ _auction: _id }).lean(),
	// 		views 			= doc ? doc.views : 0;
	//
	// 	if (views) {
	// 		await Views.updateOne({ _auction: _id }, { $set: { views: +views + 1 } });
	// 	} else {
	// 		new Views({
	// 			_auction: _id,
	// 			views: 1
	// 		}).save();
	// 	}
	//
	// 	res.send(true);
	// });

	// app.post('/api/add_view_for_by_timestamp/:timestamp', async (req, res) => {
	// 	const
	// 		{ timestamp } = req.params,
	// 		auction = await Auction
	// 			.findOne(timestampQuery(timestamp))
	// 			.select('_id')
	// 			.lean();
	//
	// 	if (!auction) {
	// 		res.send(false);
	// 		return;
	// 	}
	//
	// 	const
	// 		doc = await Views
	// 			.findOne({ _auction: auction._id })
	// 			.lean(),
	//
	// 		views = doc ? doc.views : 0;
	//
	// 	if (views) {
	// 		Views.updateOne({ _auction: auction._id }, { $inc: { views: 1 }}).exec();
	// 	} else {
	// 		new Views({ _auction: auction._id, views: 1 }).save();
	// 	}
	//
	// 	res.send(true);
	// });
}
