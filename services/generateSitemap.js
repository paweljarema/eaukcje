const
  mongoose = require('mongoose'),
  xmlbuilder = require('xmlbuilder'),
  fs = require('fs'),

  { host } = require('./emailTemplates/business'),
  { commonAdFilters } = require('../routes/functions/filterFunctions'),
  { friendlyAuctionLink, removeInvalidChars, makeCatLink } = require('../client/src/functions/links'),

  THIS_MODULE_PATH = 'services',
  ROOT_FILE_PATH = __dirname.replace(THIS_MODULE_PATH, '');

async function generateSitemap() {

  let urlset = xmlbuilder.create(
    'urlset',
    { version: '1.0', encoding: 'UTF-8' }
  );

  urlset.att('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');

  const
    STATIC_PAGES = await resolveStaticPages(),
    categoryUrls = await resolveCategoryUrls(),
    contentUrls = await generateContentUrls();

  let urls = [...STATIC_PAGES, ...categoryUrls, ...contentUrls];

  for (let url of urls)
    addUrl(url, urlset);

  saveSitemap(urlset.end({ pretty: true }));
}

async function resolveCategoryUrls() {
  const result = [];

  let cats = await mongoose
    .model('category')
    .find({ activeAuctions: { $gt: 0 }})
    .sort({ name: 1 })
    .lean();

  const getPath = (cats, path = []) => {
    for (let cat of cats) {
      if (cat.activeAuctions < 1) continue;
      result.push( makeCatLink( path.concat(cat.name) ));

      if (cat.subcategories)
        getPath( cat.subcategories, path.concat(cat.name) );
    }
  }

  getPath(cats);
  return result;
}

async function generateContentUrls() {
  result = [];

  let ads = await mongoose
    .model('auction')
    .find(commonAdFilters(null))
    .sort({ 'date.start_date': -1 })
    .select('_shortid title categories date created')
    .lean();

    for (let ad of ads)
      result.push(friendlyAuctionLink(ad, host));

  return result;
}

function saveSitemap(xmlString) {
  let path = ROOT_FILE_PATH + 'sitemap.xml';

  fs.writeFile(path, xmlString, (err) => {

    if (err) throw err;
    console.log('Sitemap was saved in: ' + path);
  });
}

function addUrl(url, xmlElement) {
  xmlElement
    .ele('url')
    .ele('loc', null, url);
}

async function resolveStaticPages() {
  return [ host, ...resolve('regulamin') ]; // , ...paths ];
}

function resolve(pathString) {
  return pathString
    .split(' ')
    .map(path => host + path);
}

function pad(num) {
  if (num < 9)
    return '0' + num;
  else
    return num;
}

function parseDate(date) {
  let
    year = date.getFullYear(),
    month = date.getMonth(),
    day = date.getDate(),

    millis = date.getTime() - new Date(year, month, day).getTime();

  return {
    year: year,
    month: month + 1,
    day: day,
    millis: millis
  };
}

module.exports = generateSitemap;
