module.exports.getFullReport = async function(input, response, progress) {
  let
    Client = require('node-regon'),

    key = input.key,
    nip = input.nip,

    gus, basicInfo, fullReport;

  gus = Client.createClient({ key, disableAsync: true });

  basicInfo = gus.findByNip(nip).response;

  // progress(basicInfo);
  if (basicInfo.Regon && basicInfo.Typ && basicInfo.SilosID) {

    fullReport = gus.getFullReport(
      basicInfo.Regon[0],
      basicInfo.Typ[0],
      Number(basicInfo.SilosID[0])
    ).response;

    // progress(fullReport);
    response(extractUsefulData(fullReport));
  }
  else
    response({ error: true });

  function extractUsefulData(fullReport) {
    let {
      fiz_nazwa, praw_nazwa,
      fizC_numerTelefonu, praw_numerTelefonu,
      fizC_numerWewnetrznyTelefonu, praw_numerWewnetrznyTelefonu,
      fiz_adSiedzKraj_Nazwa, praw_adSiedzKraj_Nazwa,
      fiz_adSiedzUlica_Nazwa, praw_adSiedzUlica_Nazwa,
      fiz_adSiedzNumerNieruchomosci, praw_adSiedzNumerNieruchomosci,
      fiz_adSiedzNumerLokalu, praw_adSiedzNumerLokalu,
      fiz_adSiedzKodPocztowy, praw_adSiedzKodPocztowy,
      fiz_adSiedzMiejscowosc_Nazwa, praw_adSiedzMiejscowosc_Nazwa,
      fiz_adSiedzGmina_Nazwa, praw_adSiedzGmina_Nazwa,
      fizC_adresEmail, fiz_adresEmail2, praw_adresEmail, praw_adresEmail2
    } = fullReport,

    firm_name = fiz_nazwa ? fixName(fiz_nazwa[0]) : praw_nazwa ? fixName(praw_nazwa[0]) : '',
    firm_country = fiz_adSiedzKraj_Nazwa ? capitalize(fiz_adSiedzKraj_Nazwa[0]) : praw_adSiedzKraj_Nazwa ? capitalize(praw_adSiedzKraj_Nazwa[0]) : '',
    firm_phone = fizC_numerTelefonu ? concat([fizC_numerTelefonu, fizC_numerWewnetrznyTelefonu]) : praw_numerTelefonu ? concat([praw_numerTelefonu, praw_numerWewnetrznyTelefonu]) : '',
    firm_address = (
      fiz_adSiedzUlica_Nazwa
      ?
      `${ fiz_adSiedzUlica_Nazwa[0] } ${ fiz_adSiedzNumerNieruchomosci[0] + (fiz_adSiedzNumerLokalu[0] ? '/' : '') + fiz_adSiedzNumerLokalu[0] } ${ fiz_adSiedzKodPocztowy[0] } ${ fiz_adSiedzMiejscowosc_Nazwa[0] || fiz_adSiedzGmina_Nazwa[0] } ${ (fiz_adSiedzMiejscowosc_Nazwa[0] || fiz_adSiedzGmina_Nazwa[0]) !== fiz_adSiedzGmina_Nazwa[0] && fiz_adSiedzGmina_Nazwa[0] ? 'gmina ' + fiz_adSiedzGmina_Nazwa[0] : '' }`.trim()
      :
      praw_adSiedzUlica_Nazwa
      ?
      `${ praw_adSiedzUlica_Nazwa[0] } ${ praw_adSiedzNumerNieruchomosci[0] + (praw_adSiedzNumerLokalu[0] ? '/' : '') + praw_adSiedzNumerLokalu[0] } ${ praw_adSiedzKodPocztowy[0] } ${ praw_adSiedzMiejscowosc_Nazwa[0] || praw_adSiedzGmina_Nazwa[0] } ${ (praw_adSiedzMiejscowosc_Nazwa[0] || praw_adSiedzGmina_Nazwa[0]) !== praw_adSiedzGmina_Nazwa[0] && praw_adSiedzGmina_Nazwa[0] ? 'gmina ' + praw_adSiedzGmina_Nazwa[0] : '' }`.trim()
      :
      ''
    ),
    email = fizC_adresEmail ? concat([fizC_adresEmail, fiz_adresEmail2]) : praw_adresEmail ? concat([praw_adresEmail, praw_adresEmail2]) : '';

    return {
      firm_name,
      firm_country,
      firm_phone,
      firm_address,
      email
    };
  }

  function concat(arr) {
    return arr
      .reduce((a, b) => {
        return [a, b[0].toLowerCase()]
          .filter(val => val)
          .join('; ');
      }, '');
  }

  function fixName(string) {
    return string
      .split(/\s+/)
      .map(str => capitalize(str))
      .join(' ');
  }

  function capitalize(string) {
    let startChar = string.slice(0, 1);

    if (/[a-zA-ZŁŃĆŹŻĄĘŚ]/i.test(startChar)) {
      return startChar.toUpperCase() + string.slice(1).toLowerCase();
    } else {
      return startChar + string.slice(1, 2).toUpperCase() + string.slice(2).toLowerCase();
    }

  }
}
