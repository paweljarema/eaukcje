let { spawn } = require('threads');

module.exports.composeThread = function composeThread(asyncFunction, input, callback, progressCallback) {
  let thread = spawn(asyncFunction);

  thread
    .send(input)
    .on('message', function(response) {
      if (callback && response) callback(response);
      thread.kill();
    })
    .on('progress', function(progress) {
      if (progressCallback) progressCallback(progress);
    })
    .on('error', function(error) {
      console.log('Worker thread error:', error);
      thread.kill();
    })
    .on('exit', function() {
      // console.log('Worker thread has been terminated.');
    });
}
