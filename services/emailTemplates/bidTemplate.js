const mainTemplate = require('./mainTemplate');
const business = require('./business');
const { friendlyAuctionLink } = require('../../client/src/functions/links');

module.exports = (auction, text, price) => mainTemplate(
	`Licytacja`,
	`<p>Zalicytowałeś w aukcji przedmiotu <a href="${ friendlyAuctionLink(auction, business.host) }">${ auction.title }</a> za ${ price } zł. ${text}</p>`
);
