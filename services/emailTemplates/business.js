const
  production = process.env.NODE_ENV === 'production',
  host = production ? 'https://eaukcje.pl/' : 'http://localhost:3000/',
  emailFormPath = 'kontakt';

module.exports = {
    name: 'Eaukcje.pl',
    email: 'noreply@eaukcje.pl',
    host: host,
    logo: 'http://drive.google.com/uc?export=view&id=1RZY5T3hBf2F9L5gVgD2I6DQYeLIBkgjF',
    emailFormPath: host + emailFormPath
};
// 1m1uMNGwcRDcjL8m2w7z2Rl4hKvzKwzA8
