const mainTemplate = require('./mainTemplate');
const business = require('./business');
const { friendlyAuctionLink } = require('../../client/src/functions/links');

module.exports = (auction, price) => mainTemplate(
	`Kup Teraz`,
	`<p>Gratulujemy. Kupiłeś Teraz przedmiot <a href="${ friendlyAuctionLink(auction, business.host) }">${ auction.title }</a>. Dokonaj przedpłaty ${price} zł, aby sprzedawca mógł wysłać towar.</p>`
);
