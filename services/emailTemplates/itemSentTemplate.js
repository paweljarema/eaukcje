const mainTemplate = require('./mainTemplate');
const business = require('./business');
const { friendlyAuctionLink } = require('../../client/src/functions/links');

module.exports = (auction) => mainTemplate(
	`Wysyłka`,
	`<p>Sprzedawca wysłał do Ciebie przedmiot <a href="${ friendlyAuctionLink(auction, business.host) }">${ auction.title }</a>.</p>
	 <p>Wkrótce powinienieś odebrać przesyłkę.</p>`
);
