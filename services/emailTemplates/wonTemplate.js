const mainTemplate = require('./mainTemplate');
const business = require('./business');
const { friendlyAuctionLink } = require('../../client/src/functions/links');

module.exports = (auction, price) => mainTemplate(
	`Koniec licytacji`,
	`<p>Gratulujemy, wygrałeś licytację <a href="${ friendlyAuctionLink(auction, business.host) }">${ auction.title }</a>. Dokonaj przedpłaty ${ price } zł, aby sprzedawca mógł wysłać towar. Pamiętaj, że nie przekażemy twoich środków Sprzedawcy, dopóki nie odbierzesz towaru i nie potwierdzisz jego zgodności z opisem.</p>`
);
