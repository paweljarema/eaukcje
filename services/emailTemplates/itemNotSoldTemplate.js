const mainTemplate = require('./mainTemplate');
const business = require('./business');
const { friendlyAuctionLink } = require('../../client/src/functions/links');

module.exports = (auction) => mainTemplate(
	`Koniec aukcji`,
	`<p>Zakończyła się licytacja przedmiotu <a href="${ friendlyAuctionLink(auction, business.host) }">${ auction.title }</a>. Niestety, nie udało się go sprzedać.</p>`
);
