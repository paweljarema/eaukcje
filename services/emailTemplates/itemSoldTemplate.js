const mainTemplate = require('./mainTemplate');
const business = require('./business');
const { friendlyAuctionLink } = require('../../client/src/functions/links');

module.exports = (auction, names, price) => mainTemplate(
	`Sprzedaż`,
	`<p>Gratulujemy, licytacja przedmiotu <a href="${ friendlyAuctionLink(auction, business.host) }">${ auction.title }</a> zakończyła się. ${ (names.length === 1 ? `${names[0]} kupił` : (names.reduce((n1, n2) => `${n1}, ${n2}` ) + ' kupili')) } od Ciebie przedmiot powyżej ustalonej przez Ciebie ceny minimalnej${ price ? `. ${ price } zł.` : '.' }</p>`
);
