const business = require('./business');
const footer = `Copyright © 2018 <a href="${business.host}">Eaukcje.pl.</a>`;

module.exports = (title, content, options = {}) => {
	const { emailForm = false } = options;

	return (
        `<html>
            <head>
                <style>
                    .container { width: 550px; margin: auto; padding: 20px; font-family: sans-serif; }
                    .head { margin-bottom: 60px; padding: 40px 10px; text-align: center; border-bottom: 2px solid #fc850a; background: #ffd9b0; }
                    .head h1 { display: inline-block; vertical-align: middle; color: #fff; }
                    p { padding: 0 10px; line-height: 1.5; }
										.img-wrapper { vertical-align: middle; margin-right: 30px; }
										img { text-align: center; margin-left: 20px; margin-right: 5px; width: 100px; text-align: center; }
                    a { color: #009bff; text-decoration: none; font-weight: 100; }
                    a:hover { text-decoration: underline; }
                    .quote { font-size: 14px; padding-left: 10px; border-left: 3px solid #eee; }
                    .content { text-align: center; margin-bottom: 60px; }
										footer { width: 100%; margin-top: 20px; padding: 20px 0; font-weight: 100; text-align: center; background: #fafafa; }
										.regards { border-top: 1px solid #eee; margin-left: 10px; margin-right: 10px; text-align: center; }
                </style>
            </head>
            <body>
                <div class="container">
                    <div class="head">
                        <div class="img-wrapper">
                            <img src="${business.logo}" alt="${business.name + ' logo'}" title="${business.name}"/>
                        </div>
                        <h1>${title}</h1>
                    </div>

										<div class="content">
                    	${content}
											${
												emailForm
												?
												`<div>
													<a href="${ business.emailFormPath }" target="_blank">Otwórz formularz kontaktowy</a>
												</div>`
												:
												''
											}
											<p>Życzymy wielu udanych transakcji!</p>
										</div>

										<div class="regards">
                    	<p>Z poważaniem:<br/>
                        Zespół ${ business.name }
											</p>
										</div>

                    <footer>${footer}</footer>
                </div>
            </body>
        </html>`
    );
};
