const mainTemplate = require('./mainTemplate');
const business = require('./business');
const { friendlyAuctionLink } = require('../../client/src/functions/links');

module.exports = (auction, name, price) => mainTemplate(
	`Sprzedaż`,
	`<p>${name} kupił od Ciebie <a href="${ friendlyAuctionLink(auction, business.host) }">${ auction.title }</a> za cenę Kup Teraz ${price} zł. Gratulujemy!</p>`
);
