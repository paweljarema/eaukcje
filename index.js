const fs = require('fs');
const http = require('http');
const https = require('https');
const express = require('express');
const app = express();
const upload = require('multer')();

const keys = require('./config/keys');
const path = require('path');
const cookieSession = require('cookie-session');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const passport = require('passport');
const requireAppAuth = require('./middleware/requireAppAuth');

const Business = require('./client/src/constants/business');
const in_production = process.env.NODE_ENV === 'production';

require('./services/Passport');

const PORT = process.env.PORT || process.env.NODE_ENV === 'production' ? 80 : 5000;

app.use(bodyParser.json({ limit: '5mb' }));
app.use(bodyParser.urlencoded({ limit: '5mb', extended: true, parameterLimit: 5000 }));
app.use(cookieSession({
    keys: [keys.cookieHash],
    maxAge: 7 * 24 * 60 * 60 * 1000
}));


if (in_production) {
  const checkSubdomain = require('./middleware/checkSubdomain');
	app.use(checkSubdomain);
}

mongoose.connect(keys.mongoURI);
require('./models/IntegerIdTracker');
require('./models/User');
require('./models/Auction');
require('./models/Category');
require('./models/Rate');
require('./models/Like');
require('./models/Views');
require('./models/Admin');
require('./models/Rate');
require('./models/Chat');
require('./models/Invoice');
require('./models/CreditTransaction');
require('./models/Settings');
require('./models/Violation');
require('./models/Seo');

app.use(passport.initialize());
app.use(passport.session());
app.use('/api/webapi', [upload.any(), requireAppAuth], require('./routes/apiRoutes'));

require('./services/Chat')(app);

require('./routes/dbRoutes')(app);
require('./routes/flashRoutes')(app);
require('./routes/authRoutes')(app);
require('./routes/userRoutes')(app);
require('./routes/categoryRoutes')(app);
require('./routes/auctionRoutes')(app);
require('./routes/otherUserRoutes')(app);
require('./routes/invoiceRoutes')(app);
require('./routes/chatRoutes')(app);
require('./routes/przelewy24Routes')(app);
require('./routes/adminRoutes')(app);
require('./routes/importExportRoutes')(app);

app.use('/gus', require('./routes/gusRoutes'));
app.use('/test', require('./routes/testRoutes'));
app.use('/frontpage', require('./routes/frontPageRoutes'));
app.use('/settings', require('./routes/settingRoutes'));
app.use('/violations', require('./routes/violationRoutes'));
app.use('/seo', require('./routes/seoRoutes'));


require('./services/cronScheduleTaskService')(app);
require('./services/viewsService')(app);

// sitemap.generate(app);
// sitemap.toFile();

if (process.env.NODE_ENV === 'production') {
    console.log('in production');
    app.use(express.static('client/build'));
    app.get('*', (req, res) => {
      if (req.host.startsWith('www.')) {
        res.redirect(301, `https://${ req.host.replace('www.', '') }${req.url}`)
          return;
      }

      if (req.originalUrl.includes('robots.txt')) {
			   res.sendFile(path.resolve(__dirname, 'robots.txt'));
         return;
      }

		  if (req.originalUrl.includes('sitemap.xml')) {
			   res.sendFile(path.resolve(__dirname, 'sitemap.xml'));
         return;
      }

      res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
    });

   // SSL CERT
   const privateKey = fs.readFileSync('./cert/private.key', 'utf8');
   const certificate = fs.readFileSync('./cert/certificate.crt', 'utf8');
   const ca = fs.readFileSync('./cert/ca_bundle.crt', 'utf8');

   const credentials = {
      key: privateKey,
      cert: certificate,
      ca: ca
   };

  const httpsServer = https.createServer(credentials, app);
  httpsServer.listen(443, () => {
      console.log('HTTPS Server running on port 443');
  });

  const httpServer = http.createServer((req, res) => {
    const host = (req.headers || {}).host || Business.domain;
    const url = req.url || '/';

    res.writeHead(301, `https://${ host }${ url }`);
    res.end();
  });

  httpServer.listen(80);

} else {
    app.listen(PORT, () => console.log(`Server launched on port ${ PORT }`));
}
