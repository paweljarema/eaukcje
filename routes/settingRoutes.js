const
  express = require('express'),
  settingRouter = express.Router(),

  mongoose = require('mongoose'),

  { loadSettings, saveSettings } = require('../functions/settings');

settingRouter.get('/', async (req, res) => {
  res.send(await loadSettings({ mongoose }));
});

settingRouter.post('/', async (req, res) => {
  console.log(req.body);

  await saveSettings({
    mongoose,
    settings: req.body,
    res,
    message: 'Zapisano cennik.'
  });
});

module.exports = settingRouter;
