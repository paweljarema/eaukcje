const
  express = require('express'),
  testRouter = express.Router(),

  mongoose = require('mongoose'),
  User = mongoose.model('user'),
  Invoice = mongoose.model('invoice'),
  Auction = mongoose.model('auction'),
  pdf = require('html-pdf'),

  bulkMailTemplate = require('../services/emailTemplates/bulkMailTemplate'),
  buyNowItemSoldTemplate = require('../services/emailTemplates/buyNowItemSoldTemplate'),
  mailUserTemplate = require('../services/emailTemplates/mailUserTemplate'),
  userDeletedTemplate = require('../services/emailTemplates/userDeletedTemplate'),
  paidSimpleTemplate = require('../services/emailTemplates/paidSimpleTemplate'),

  { loadSettings } = require('../functions/settings'),
  { newPolmarketInvoiceTo } = require('../functions/invoice'),
  { OPTIONS } = require('../client/src/constants/pdf'),

  Symfonia = require('../services/Symfonia');


// testRouter.use('*', async (req, res, next) => {
//   console.log(req.host, req.url);
//   next();
// });

testRouter.get('/symf', async (req, res) => {
  const faktury = await Invoice
    .find({})
    .select('-html -pdf')
    .lean();

  const symfonia = new Symfonia(faktury);
  symfonia.sendFileViaExpress(res);

});

testRouter.get('/', async (req, res) => {
  res.send({ route: 'test' });
});

testRouter.get('/email', async (req, res) => {
  const
    auction = await Auction.findOne({}),
    user = await User.findOne({}),
    name = user.firstname,
    price = auction.price.current_price || auction.price.start_price;

  res.send(
    paidSimpleTemplate(user.fullname, user.contact.phone, user.contact.email, user.address, 150, 'kurier DPS', auction, 1)
  );

  // res.send(
  //   userDeletedTemplate('powód')
  // );

  // res.send(
  //   mailUserTemplate('Some subject', 'Some message')
  // );

  // res.send(
  //   buyNowItemSoldTemplate(auction, name, price)
  // );

  // res.send(
  //   bulkMailTemplate('some subject', 'some message')
  // );
});

testRouter.get('/settings', async (req, res) => {
  let settings = await loadSettings({ mongoose });

  console.log(settings.promoPackets);

  let order = settings.promoPackets.find(packet => String(packet._id) === '5d8f543a7527b708472d4dc6');

  console.log(order);

  res.send(settings.promoPackets);

});

testRouter.get('/invoice', async (req, res) => {
  // let
  //   user = await User.findOne({}),
  //   products = [
  //     {
  //       name: 'Promocja ogłoszenia',
  //       qty: 1,
  //       unit: 'szt.',
  //       price: 240,
  //       vat: 23
  //     }
  //   ],
  //   description = 'Ala ma kota',
  //   vat = 23,
  //   total = null,
  //   inflateVat = true,
  //   test = true;
  //
  //   html = await newPolmarketInvoiceTo(user, { products, description, vat, total, inflateVat, test });
  //
  //   res.send(html);

  let
    user = await mongoose.model('user').findOne({}),
    description = `Selektywna promocja ogłoszenia w serwisie`,
    total = Number(21000) / 100,
    vat = 23,
    products = [{ name: description, qty: 1, price: total }],

    html = await newPolmarketInvoiceTo(
      user, {
        products,
        description,
        vat,
        total,
        inflateVat: true,
        test: true
      }
    );

  pdf
    .create(html, OPTIONS)
    .toStream((err, stream) => {
      if (err) {
        console.log(err);
        res.send({ err: err });
      } else {
        stream.pipe(res);
      }
    });
  // console.log('HTML', html)
  // res.send(html);
});

module.exports = testRouter;
