const axios 			= require('axios');
const requireLogin= require('../middleware/requireLogin');
const qs 					= require('qs');
const md5 				= require('md5');
const keys 				= require('../config/keys');
const business 		= require('../services/emailTemplates/business');
const P24 				= require('../services/Przelewy24')();

const mongoose 		= require('mongoose');
const { ObjectId }= mongoose.Types;

const User 				= mongoose.model('user');
const Admin 			= mongoose.model('admin');
const Auction 		= mongoose.model('auction');
const Transaction = mongoose.model('transaction');
const CreditTransaction	= mongoose.model('creditTransaction');

const Mailer 			= require('../services/Mailer');
const sendItemTemplate	= require('../services/emailTemplates/sendItemTemplate');
const paySimpleTemplate	= require('../services/emailTemplates/paySimpleTemplate');
const paidSimpleTemplate	= require('../services/emailTemplates/paidSimpleTemplate');
const helpers = require('../services/helpers/otherHelpers');

const produceInvoice = require('../functions/payments')(mongoose);

const { loadSettings } = require('../functions/settings');

const VAT = 23;

// 	verify: 'https://sandbox.przelewy24.pl/trnVerify'

function TransactionId(title, price, buyer, owner_id) {
	return md5([ title, price, buyer._id, owner_id ].join('|'));
}

function AggregatedTransactionId() {
	return md5([...arguments].join('|'));
}

function productIdHash() {
	return [...arguments]
		.filter(arg => arg)
		.map(arg => arg._id || '')
		.join('_');
}

function getAggregatedTitle(buy_auctions_data, buy_promos_data) {
	let titles = [];

	if (buy_auctions_data) {
		if (buy_auctions_data.unlimited)
			titles.push('Pakiet aukcje bez ograniczeń');
		else
			titles.push('Pakiet aukcje ' + buy_auctions_data.count + ' szt.')
	}

	if (buy_promos_data) {
		titles.push('Wyróżnienia: ' + buy_promos_data.name + ' ' + buy_promos_data.count + ' szt.');
	}

	return titles
		.filter(txt => txt)
		.join(' i ');
}

module.exports = app => {
	app.post('/przelewy24/ping', requireLogin, async (req, res) => {
		P24.testSoapAccess();
		P24.testRestAccess();

		res.send(true);
	});

	app.post('/przelewy24/justPromote', requireLogin, async (req, res) => {
		const { _auction, auction_url } = req.body;

		if (!_auction || !ObjectId.isValid(_auction)) {
			res.send(auction_url);
			return;
		}

		const auction = await Auction.findOne({ _id: ObjectId(_auction), _user: req.user._id });

		if (auction && auction.premium && auction.premium.isPremium) {
			res.send(auction_url);
			return;
		}

		if (!auction.premium) {
			auction.premium = {};
		}

		auction.premium.isPremium = true;
		auction.premium.forever = false;
		auction.premium.endDate = Date.now() + 7 * 24 * 60 * 60 * 1000;

		auction.save();

		const user = await User.findById(req.user._id);
		user.packets.promos = user.packets.promos - 1;
		user.save();

		res.send(auction_url);
	});

	app.post('/przelewy24/aggregatedOrder', requireLogin, async (req, res) => {
		let
			date = Date.now(),
			buyer = req.user,
			{
				buy_auctions, buy_promos, promote_auction,
				buy_auctions_data, buy_promos_data,
				auction_id, auction_title, auction_url,
				price
			} = req.body,

			title = getAggregatedTitle(buy_auctions_data, buy_promos_data),

			product_id_hash = productIdHash(buy_auctions_data, buy_promos_data),
			p24_session_id = AggregatedTransactionId(
				'aggregated order',
				price,
				buyer._id,
				date,
				product_id_hash
			),

			p24_url_return = auction_url ? (business.host + auction_url) : business.host;

		let data = {
			p24_merchant_id: keys.przelewy24Id,
			p24_pos_id: keys.przelewy24Id,
			p24_session_id: p24_session_id,
			p24_amount: parseInt(price) * 100,
			p24_currency: 'PLN',
			p24_description: title,
			p24_email: buyer.contact.email,
			p24_client: `${ buyer.firstname || '' } ${ buyer.lastname || buyer.firstname ? '' : 'Anonim' }`,
			p24_address: buyer.address ? buyer.address.street : '',
			p24_zip: buyer.address ? buyer.address.postal : '',
			p24_city: buyer.address ? buyer.address.city : '',
			p24_country: 'PL',
			p24_phone: buyer.contact.phone ? buyer.contact.phone : '',
			p24_url_return: p24_url_return,
			p24_url_status: business.host + 'przelewy24/aggregatedStatus',
			p24_wait_for_result: 0,
			p24_time_limit: 0,
			p24_channel: 16,
			p24_transfer_label: 'Ogloszenie PREMIUM eaukcje.pl',
			p24_api_version: '3.2',
			p24_regulation_accept: true
		};

		const token = await P24.registerTransaction(data);
		const transaction = new CreditTransaction({
			date,
			_user: buyer._id,
			_auction: auction_id ? ObjectId(auction_id) : undefined,
			_packet: buy_auctions ? ObjectId(buy_auctions_data._id) : undefined,
			_promo: buy_promos ? ObjectId(buy_promos_data._id) : undefined,
			title: title,
			p24_session_id: p24_session_id,
			token,
			qty: 1,
			total: data.p24_amount,
			vat: VAT,
			done: false
		});

		await transaction.save();

		console.log('request body:', req.body);
		console.log('request transaction:', transaction);

		req.session.message = 'Za chwilę zostaniesz przekierowany na stronę Przelewy24. Proszę czekać...';
		res.send(P24.requestTransactionUrl(token));

	});

	app.post('/przelewy24/aggregatedStatus', async (req, res) => {
		console.log('aggregated dbg:', req.body);

		const { p24_session_id, p24_description, p24_amount } = req.body;
		const transaction = await CreditTransaction.findOne({ p24_session_id });

		const user = await User.findById(ObjectId(transaction._user));
		const settings = await loadSettings({ mongoose });
		const { auctions, auctions_unlimited, promos } = user.packets;
		const { _auction, _packet, _promo, qty } = transaction;

		// buy auction packets
		if (_packet) {
			console.log('apply auction packet')
			let order = settings.auctionPackets.find(packet => String(packet._id) === String(_packet));
			if (order.unlimited) {
				user.packets.auctions = 0;
				user.packets.auctions_unlimited = true;
				user.packets.unlimited_date = Date.now() + 30 * 24 * 60 * 60 * 1000;
				console.log('unlimited auctions');
			} else {
				user.packets.auctions = (user.packets.auctions || 0) + order.count;
				console.log('added ' + order.count + ' auctions');
			}
		}

		// buy promotions, promote auction
		if (_promo) {
			console.log('apply promotion packet');
			console.log('settings', settings);
			console.log(_promo);

			let order = settings.promoPackets.find(packet => String(packet._id) === String(_promo));

			user.packets.promos = (user.packets.promos || 0) + order.count;
			console.log('added ' + order.count + ' promos');
			if (_auction) {
				const auction = await Auction.findById(ObjectId(_auction));

				auction.premium.isPremium = true;
				auction.premium.forever = false;
				auction.premium.endDate = Date.now() + 7 * 24 * 60 * 60 * 1000;
				console.log('promoted auction ' + auction._id);

				auction.save();
				console.log('saved auction');

				user.packets.promos = user.packets.promos - 1;
				console.log('decreased promo packets to ' + user.packets.promos);
			}
		}

		transaction.done = true;
		console.log('successfully closed transaction');

		user.save();
		console.log('saved user');
		transaction.save();
		console.log('saved transaction');

		await produceInvoice(mongoose, {
			transaction,
			title: transaction.title,
			total: parseInt(p24_amount) / 100,
			qty: qty,
			vat: VAT,
			inflateVat: true
		});
		console.log('produced invoice');
		console.log('all done.');

		if (req.session) req.session.message = "Zakup kredytów przebiegł pomyślnie";

		// if (P24.verifyTransaction(req.body)) {
		// 	console.log('pomyślnie zweryfikowano zakup kredytów');
		// } else {
		// 	console.log('zakup kredytów niezweryfikowany');
		// }

		res.send(true);
	});

	app.post('/przelewy24/promoteAuction', requireLogin, async (req, res) => {
		const
			qty = 1,
			date = Date.now(),
			buyer = req.user,
			{ auction_id, auction_title, auction_url, promoCode, price } = req.body,
			option = promoCode === '1' ? 'Premium 7 dni' : 'Premium Forever',
			title = `Promocja ogłoszenia ${ auction_title } w opcji ${ option }`,
			transaction_id = TransactionId('promote auction', +promoCode * +price, req.user, date);

		let data = {
			p24_merchant_id: keys.przelewy24Id,
			p24_pos_id: keys.przelewy24Id,
			p24_session_id: transaction_id,
			p24_amount: +price * 100,
			p24_currency: 'PLN',
			p24_description: title,
			p24_email: buyer.contact.email,
			p24_client: `${buyer.firstname || ''} ${buyer.lastname || buyer.firstname ? '' : 'Anonim'}`,
			p24_address: buyer.address ? buyer.address.street : '',
			p24_zip: buyer.address ? buyer.address.postal : '',
			p24_city: buyer.address ? buyer.address.city : '',
			p24_country: 'PL',
			p24_phone: buyer.contact.phone ? buyer.contact.phone : '',
			p24_url_return: business.host + auction_url,
			p24_url_status: business.host + 'przelewy24/promoStatus',
			p24_wait_for_result: 0,
			p24_time_limit: 0,
			p24_channel: 16,
			p24_transfer_label: 'Ogloszenie PREMIUM eaukcje.pl',
			p24_api_version: '3.2',
			p24_regulation_accept: true
		};

		const token = await P24.registerTransaction(data);
		const transaction = new CreditTransaction({
			date,
			_user: ObjectId(req.user._id),
			_auction: ObjectId(auction_id),
			p24_session_id: transaction_id,
			token,
			promoCode,
			qty: qty,
			total: data.p24_amount,
			vat: VAT,
			done: false
		});
		await transaction.save();

		req.session.message = 'Za chwilę zostaniesz przekierowany na stronę Przelewy24. Proszę czekać...';
		res.send(P24.requestTransactionUrl(token));
	});

	app.post('/przelewy24/promoStatus', async (req, res) => {
		console.log('Ad promo dbg', req.body);

		const
			{ p24_session_id, p24_description, p24_amount } = req.body,
			transaction = await CreditTransaction.findOne({ p24_session_id }),
			auction = await Auction.findOne({ _id: ObjectId(transaction._auction) }),
			forever = parseInt(transaction.promoCode) === 2;

		 let longerDate;

		 if (Date.now() + 7 * 24 * 60 * 60 * 1000 > auction.premium.endDate) {
		 	longerDate = Date.now() + 7 * 24 * 60 * 60 * 1000;
		 } else {
		 	longerDate = auction.premium.endDate;
		 }

		auction.premium.isPremium = true;
		auction.premium.forever = forever;
		auction.premium.endDate = longerDate;

		transaction.done = true;

		auction.save();
		transaction.save();

		await produceInvoice(mongoose, {
			transaction,
			title: p24_description,
			total: p24_amount,
			qty: 1,
			vat: VAT,
			inflateVat: true
		});

		if (req.session) req.session.message = "Pomyślnie wypromowano ogłoszenie";

		if (P24.verifyTransaction(req.body)) {
			console.log('pomyślnie zweryfikowano promocję');
		} else {
			console.log('promocja niezweryfikowana');
		}

		res.send(true);
	});

	app.post('/przelewy24/buyCredits', requireLogin, async (req, res) => {
		const buyer = req.user;
		const { qty, cost } = req.body;

		const title = 'Kupno kredytów eaukcje.pl w liczbie ' + qty;
		const transaction_id = TransactionId('buy credits', +qty * +cost, buyer, new Date().getTime());

		let data = {
			p24_merchant_id: keys.przelewy24Id,
			p24_pos_id: keys.przelewy24Id,
			p24_session_id: transaction_id,
			p24_amount: (+qty * +cost) * 100,
			p24_currency: 'PLN',
			p24_description: title,
			p24_email: buyer.contact.email,
			p24_client: `${buyer.firstname || ''} ${buyer.lastname || buyer.firstname ? '' : 'Anonim'}`,
			p24_address: buyer.address ? buyer.address.street : '',
			p24_zip: buyer.address ? buyer.address.postal : '',
			p24_city: buyer.address ? buyer.address.city : '',
			p24_country: 'PL',
			p24_phone: buyer.contact.phone ? buyer.contact.phone : '',
			p24_url_return: business.host + 'przelewy24/creditCallback',
			p24_url_status: business.host + 'przelewy24/creditStatus',
			p24_wait_for_result: 0,
			p24_time_limit: 0,
			p24_channel: 16,
			p24_transfer_label: 'Kupno kredytów eaukcje.pl',
			p24_api_version: '3.2',
			p24_regulation_accept: true
		};

		const token = await P24.registerTransaction(data);

		const transaction = new CreditTransaction({
			_user: ObjectId(req.user._id),
			p24_session_id: transaction_id,
			token,
			promoCode: 0,
			qty: qty,
			total: data.p24_amount,
			vat: VAT,
			date: new Date().getTime(),
			done: false
		});

		await transaction.save();

		req.session.message = 'Za chwilę zostaniesz przekierowany na stronę Przelewy24. Proszę czekać...';
		res.send(P24.requestTransactionUrl(token));
	});

	app.get('/przelewy24/creditCallback', async (req, res) => {
		res.redirect('/konto/aukcje/dodaj');
	});

	app.post('/przelewy24/creditStatus', async (req, res) => {
		console.log('creditStatus', req.body);

		const { p24_session_id, p24_description, p24_amount } = req.body;
		const transaction = await CreditTransaction.findOne({ p24_session_id });

		const user = await User.findOne({ _id: ObjectId(transaction._user) });
		const { credits } = user.balance;
		const { qty } = transaction;

		user.balance.credits = credits ? +credits + +qty : +qty;
		transaction.done = true;

		user.save();
		transaction.save();

		await produceInvoice(mongoose, {
			transaction,
			title: p24_description,
			total: p24_amount,
			qty: qty,
			vat: VAT,
			inflateVat: true
		});

		if (req.session) req.session.message = "Zakup kredytów przebiegł pomyślnie";

		if (P24.verifyTransaction(req.body)) {
			console.log('pomyślnie zweryfikowano zakup kredytów');
		} else {
			console.log('zakup kredytów niezweryfikowany');
		}

		res.send(true);
	});


	/*

	date: Number,
    token: String,
    transaction_id: String,
    _buyer: ObjectId,
    _seller: ObjectId,
    _auction: ObjectId,
    title: String,
    price: Number,
    delivery_price: Number,
    delivery_method: String,
    confirmed: Boolean

    */
	app.post('/przelewy24/registerTransaction', requireLogin, async (req, res) => {
		console.log(req.body);

		const { paySimple, title, price, shipping_price, shipping_method, qty, owner_id, auction_id } = req.body;
		const buyer = req.user;

		// no P24 Passage account
		if (paySimple) {
			const auction = await Auction.findOne({ _id: ObjectId(auction_id) });
			const owner = await User.findOne({ _id: ObjectId(owner_id) });

			const buynowpayee = auction.buynowpayees && auction.buynowpayees.indexOf(buyer._id) !== -1;
			const payee = auction.payees && auction.payees.indexOf(buyer._id) !== -1;

			let qty = 1;

			if (buynowpayee) {
				let initialPayees = auction.buynowpayees.length;
				auction.buynowpayees = auction.buynowpayees.filter(id => String(id) !== String(buyer._id));
				qty = initialPayees - auction.buynowpayees.length,
				qtyArray = new Array(qty).fill(buyer._id);

				if (auction.buynowpaid) {
		            auction.buynowpaid = auction.buynowpaid.concat(qtyArray);
		        } else {
		            auction.buynowpaid = qtyArray;
		        }
			}

			if (payee) {
				auction.payees = auction.payees.filter(id => String(id) !== String(buyer.id));
				if (auction.payees.length === 0) {
					auction.paid = true;
				}

				if (auction.auctionpaid) {
					auction.auctionpaid.push(buyer._id);
				} else {
					auction.auctionpaid = [buyer._id];
				}
			}

			if (auction.raters) {
				auction.raters.push(ObjectId(buyer._id));
			} else {
				auction.raters = [ObjectId(buyer._id)];
			}

			let
				sendRateData = {
					_auction: auction._id,
					_user: buyer._id,
					buynow: buynowpayee,
					count: qty
				};

			if (owner.toSend) {
				owner.toSend.push(sendRateData);
			} else {
				owner.toSend = [sendRateData];
			}

			owner.save();
			auction.save();

			const transaction = new Transaction({
				date: new Date().getTime(),
				_buyer: ObjectId(buyer._id),
				_seller: ObjectId(owner._id),
				_auction: ObjectId(auction._id),
				title,
				price,
				qty,
				delivery_price: shipping_price,
				delivery_method: shipping_method,
				confirmed: true
			});
			transaction.save();

			helpers.sendChatMessagesOnPay(buyer._id, owner._id, auction, +price + +shipping_price, qty, shipping_method);

			new Mailer(
				{ subject: 'Kupujący oznaczył aukcję jako opłaconą', recipients: [{ email: owner.contact.email }] },
				paySimpleTemplate(buyer.fullname, buyer.contact.phone, buyer.contact.email, buyer.address, (+price + +shipping_price), shipping_method, auction, qty)
			).send();

			new Mailer(
				{ subject: 'Opłaciłeś aukcję', recipients: [{ email: buyer.contact.email }]},
				paidSimpleTemplate(owner.fullname, owner.contact.phone, owner.contact.email, owner.address, (+price + +shipping_price), shipping_method, auction, qty)
			).send();

			req.session.message = 'Sprzedawca został powiadomiony o wpłacie i poproszony o sprawdzenie stanu konta';
			res.send(true);
			return;
		}

		const transaction_id = TransactionId(title, price, buyer, owner_id);

		let data = {
			p24_merchant_id: keys.przelewy24Id,
			p24_pos_id: keys.przelewy24Id,
			p24_session_id: transaction_id,
			p24_amount: (parseInt(price) + parseInt(shipping_price)) * 100,
			p24_currency: 'PLN',
			p24_description: title,
			p24_email: buyer.contact.email,
			p24_client: `${buyer.firstname || ''} ${buyer.lastname || buyer.firstname ? '' : 'Anonim'}`,
			p24_address: buyer.address ? buyer.address.street : '',
			p24_zip: buyer.address ? buyer.address.postal : '',
			p24_city: buyer.address ? buyer.address.city : '',
			p24_country: 'PL',
			p24_phone: buyer.contact.phone ? buyer.contact.phone : '',
			p24_url_return: business.host + 'przelewy24/callback',
			p24_url_status: business.host + 'przelewy24/status',
			p24_wait_for_result: 1,
			p24_time_limit: 0,
			p24_channel: 16,
			p24_transfer_label: title,
			p24_api_version: '3.2',
			p24_regulation_accept: true
		};

		const
			token = await P24.registerTransaction(data),
			transaction_data 	= {
				date: new Date().getTime(),
			    token,
			    transaction_id,
			    _buyer: ObjectId(buyer._id),
			    _seller: ObjectId(owner_id),
			    _auction: ObjectId(auction_id),
			    title,
			    price: parseInt(price),
			    delivery_price: parseInt(shipping_price),
			    delivery_method: shipping_method
			},
			transaction 		= await Transaction.findOneAndUpdate(
				{ transaction_id },
				transaction_data,
				{ upsert: true }
			);

		req.session.message = 'Za chwilę zostaniesz przekierowany na stronę Przelewy24. Proszę czekać...';
		//res.send(P24.requestTransactionUrl(token));

		// try to dispatchTransaction via passage SOAP
		const seller = await User.findOne({ _id: ObjectId(owner_id) });
		const provision = (await Admin.findOne({})).provision;

		const batch = {
			title,
			price,
			provision,
			buyer,
			seller
		};

		const resp = await P24.dispatchTransaction(batch);
		console.log(resp);
	});

	app.post('/przelewy24/status', async (req, res) => {
		console.log('P24 status + verification');
		const { p24_merchant_id, p24_pos_id, p24_session_id, p24_amount, p24_currency, p24_order_id, p24_method, p24_statement, p24_sign } = req.body;

		let data = {
			p24_merchant_id,
			p24_pos_id,
			p24_session_id,
			p24_amount,
			p24_currency,
			p24_order_id,
			p24_sign
		};

		const confirm = await P24.verifyTransaction(data);

		if (confirm) {
			const transaction = await Transaction.findOne({ transaction_id: p24_session_id });
			transaction.confirmed = true;
			await transaction.save();
			res.send(true);
		} else {
			res.send(false);
		}
	});

	app.get('/przelewy24/callback', async (req, res) => {
		const buyer = req.user;
		const transaction = await Transaction.findOne(
			{ _buyer: ObjectId(buyer._id) },
			{},
			{ sort: { date: -1 } }
		);

		const name = `${buyer.firstname || ''} ${buyer.lastname || (buyer.firstname ? '' : 'Anonim')}`;
		const { address } = buyer;
		const { phone, email } = buyer.contact;
		const { title, price, delivery_method } = transaction;

		const
			seller = await User.findOne({ _id: transaction._seller }),
			auction = await Auction.findById(ObjectId(transaction._auction)),
			subject = 'Dokonano wpłaty za ' + title + '. Wyślij przedmiot',
			recipients = [{ email: seller.contact.email }],
			confirmed = transaction.confirmed;

		const mailer = new Mailer({ subject, recipients }, sendItemTemplate(name, phone, email, address, price, delivery_method, auction));
		await mailer.send();

		// TODO add rater

		if (confirmed) {
			req.session.message = "Płatność została przesłana";
		} else {
			req.session.message = "Płatność została przesłana i czeka na weryfikację";
		}

		console.log('transaction status: ' + (confirmed ? 'confirmed' : 'not confirmed'));
		res.redirect('/');
	});
}
