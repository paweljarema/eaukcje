const
  express = require('express'),
  seoRouter = express.Router(),

  mongoose = require('mongoose'),
  Admin = mongoose.model('admin'),
  Seo = mongoose.model('seo'),

  { ObjectId } = mongoose.Types,
  { CATEGORY_META_PROPS } = require('../client/src/constants/seo'),

  catIndexes = require('../cat_indexes.js');

seoRouter.get('/', async (req, res) => {
  let
    seo = await Seo.findOne({}),
    categoryProps = {},
    categories = await mongoose
      .model('category')
      .find({});


  const mapProps = (categories, parents = []) => {
    for (let cat of categories) {
      for (let prop of CATEGORY_META_PROPS) {
        let name = parents.concat([ cat.name, prop ]).join('.');

        if (cat[prop])
          categoryProps[ name ] = cat[ prop ];

        if (cat.subcategories)
          mapProps(cat.subcategories, parents.concat(cat.name));
      }
    }
  }

  mapProps(categories);

  categoryProps.title = seo.title;
  categoryProps.description = seo.description;

  res.send(categoryProps);
});

seoRouter.post('/', async (req, res) => {
  let
    { admin_id, title, description } = req.body,

    admin = await Admin.countDocuments({ _id: ObjectId(admin_id) }).limit(1),
    set = {},
    setMeta = {};

  if (!admin) {
    res.status(401).send(false);
    return;
  }

  if (title) set.title = title;
  if (description) set.description = description;

  Seo.findOneAndUpdate({}, { $set: set }, { upsert: true }).exec();

  for (let key in req.body) {
    if (['admin_id', 'title', 'description'].includes(key)) continue;

    let
      chunks = key.split('.'),
      mainCat = chunks[0],
      propName = chunks[chunks.length - 1],

      index;

    if (chunks.length === 2)
      index = propName;
    else
      index = catIndexes[ chunks.slice(0, -1).join('.') ] + '.' + propName;

    if (!setMeta[ mainCat ])
      setMeta[ mainCat ] = {};

    setMeta[ mainCat ][ index ] = req.body[ key ];
  }

  for (let mainCat in setMeta) {
    mongoose
      .model('category')
      .findOneAndUpdate(
        { name: mainCat },
        { $set: setMeta[ mainCat ] }
      ).exec();
  }

  res.send(true);
});

module.exports = seoRouter;
