const
  express = require('express'),
  violationRouter = express.Router(),

  mongoose = require('mongoose'),

  User = mongoose.model('user'),
  Auction = mongoose.model('auction'),
  Violation = mongoose.model('violation'),

  requireLogin = require('../middleware/requireLogin'),

  { ObjectId } = mongoose.Types;


violationRouter.post('/report', requireLogin, async (req, res) => {
  const
    { _user, _auction, reason, group } = req.body,

    user_id = ObjectId(_user),
    auction_id = ObjectId(_auction);

  let violation = await Violation.findOne({
    _user: user_id,
    _auction: auction_id
  });

  if (violation) {
    req.session.message = 'Już zgłosiłeś naruszenie w tej aukcji.';
    res.send(false);
    return;
  }

  violation = new Violation({
    date: Date.now(),

    _user: user_id,
    _auction: auction_id,

    reason: reason,
    group: group
  });

  const auction = await Auction.findById( auction_id );
  auction.violations.reported++;

  violation.save();
  auction.save();

  req.session.message = 'Naruszenie zostało zgłoszone i oczekuje na działanie administratora';
  res.send(true);
});


module.exports = violationRouter;
