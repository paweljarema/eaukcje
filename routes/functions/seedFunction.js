const
  CURRENT_PATH = '/routes/functions',
  ROOT_PATH = __dirname.replace(CURRENT_PATH, '');

require(`${ ROOT_PATH }/models/Admin`);
require(`${ ROOT_PATH }/models/Category`);
require(`${ ROOT_PATH }/models/Settings`);
require(`${ ROOT_PATH }/models/Seo`);

const
  fs = require('fs'),
  mongoose = require('mongoose'),
  Admin = mongoose.model('admin'),
  Category = mongoose.model('category'),
  Property = mongoose.model('property');

  bcrypt = require('bcryptjs'),

  { saveSettings } = require('../../functions/settings'),

  {
    getCatTree,
    parseCatData,
    parseSubcategories,
    parseProperties
  } = require('./categoryFunctions'),

  {
    createPositionalMapForCategories,
    CAT_INDEX_FILE,
    PROP_INDEX_FILE
  } = require('./indexMapFunctions');

async function seedCategoryData() {
  const
    categoryTree = getCatTree(),
    keys = Object.keys(categoryTree);

  await Category.deleteMany({});
  await Property.deleteMany({});

  for (let i = 0; i < keys.length; i++) {
    const
      key = keys[i],
      category = new Category({
        ...parseCatData(categoryTree[ key ]),
        subcategories: parseSubcategories( categoryTree[ key ] ),
        properties: parseProperties( categoryTree[ key ] )
      });

    await category.save();
  }

  console.log("Default category/prop set restored.");
}

function seedPaymentPackets() {
  let
    discountFinalDate = new Date('01.01.2020'),
    packetValidityTime = 30,

    auctionPackets = [
      { cost: 5, count: 1 },
      { cost: 20, count: 5 },
      { cost: 60, count: null, unlimited: true }
    ],

    promoPackets = [
      { name: 'Pakiet Mini', cost: 10, count: 1 },
      { name: 'Pakiet Medium', cost: 18, count: 4 },
      { name: 'Pakiet Max', cost: 25, count: 8 }
    ];

  saveSettings({
    mongoose,
    settings: {
      discountFinalDate,
      auctionPackets,
      promoPackets,
      packetValidityTime
    },
    res: { send: (msg) => console.log(msg) }
  });
}

function createIndexFiles() {
  createPositionalMapForCategories(
    getCatTree(),
    () => console.log('Category/property index files created.')
  );
}

async function seedAdmin() {
  const
    L = 'admin',
    P = 'admin1234',
    sr = 10,

    Admin = mongoose.model('admin');

  bcrypt.hash(P, sr, (err, hash) => {
      if (err) {
          console.log(err);
      } else {

          new Admin({
              login: L,
              password: hash,
              provision: 3
          })
          .save()
          .then(
              doc => { console.log('New Admin created sucessfully'); },
              err => { console.log('Error creating Admin', err); }
          );
      }
  });
}

async function createEmptySeo() {
  const Seo = mongoose.model('seo');
  new Seo({
    title: '',
    description: ''
  })
  .save()
  .then(
    doc => { console.log('Seo created.') },
    err => { console.log('Error creating seo.', err) }
  );
}

async function exists(model) {
  if (typeof model !== 'string')
    throw ('Exists function requires a model name passed in string format.');

  return Boolean(
    await mongoose
      .model(model)
      .countDocuments({})
      .limit(1)
    );
}

function fileExists(filename) {
  return fs.existsSync([ROOT_PATH, filename].join('/'));
}

async function seedAll() {
  if (!await exists('admin'))
    await seedAdmin();
  else
    console.log('Initializing admin account: done.');

  if (!await exists('settings'))
    seedPaymentPackets();
  else
    console.log('Initializing payment packets: done.')

  if (!await exists('category'))
    await seedCategoryData();
  else
    console.log('Initializing category data: done.')

  if (!fileExists(CAT_INDEX_FILE))
    createIndexFiles();
  else
    console.log('Initializing db helper/index files: done.');

  if (!await exists('seo'))
    await createEmptySeo();
  else
    console.log('Initializing seo: done.');
}

module.exports = seedAll;
