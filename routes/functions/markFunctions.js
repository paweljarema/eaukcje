const
  mongoose = require('mongoose'),

  Category = mongoose.model('category'),
  Property = mongoose.model('property'),

  { ObjectId } = mongoose.Types,


  catIndexes = require('../../cat_indexes'),
  propIndexes = require('../../prop_indexes'),

  ACTIVE_AUCTIONS_FIELD = '.activeAuctions';

function markCategories(array) {
  let { query, $set } = stripCatArrayForIncActiveAuctions(array, 1);
  updateOne(Category, query, $set);
}

function unmarkCategories(array) {
  let { query, $set } = stripCatArrayForIncActiveAuctions(array, -1);
  updateOne(Category, query, $set);
}

function markProperties(string) {
  let { queries, $sets } = parsePropPathsForIncActiveAuctions(string, 1);
  updateMany(Category, queries, $sets);
}

function unmarkProperties(string) {
  let { queries, $sets } = parsePropPathsForIncActiveAuctions(string, -1);
  updateMany(Category, queries, $sets);
}

function parsePropPathsForIncActiveAuctions(string, incValue) {
  if (!string || !string.split)
    return { queries: null, $sets: null };

  let
    queries = {},
    fieldQueries = {},

    paths = string.split(',');

  for (let path of paths) {
    let name = path.slice(0, path.indexOf('.'));

    if (!queries[name])
      queries[name] = { name: name };

    if (!fieldQueries[name])
      fieldQueries[name] = {};

    let fieldQuery = propIndexes[ path ] + ACTIVE_AUCTIONS_FIELD;

    fieldQueries[name][ fieldQuery ] = incValue;
  }

  let $sets = {};

  for (let key in fieldQueries)
    $sets[ key ] = { $inc: fieldQueries[ key ] };

  return {
    queries,
    $sets
  };
}

function stripCatArrayForIncActiveAuctions(array, incValue) {
  if (!array || !array.length)
    return { query: null, $set: null };

  const
    [ main, ...rest ] = array;

  let
    query = { name: main },
    fieldQueries = { activeAuctions: incValue },
    fieldQuery;

  for (let i = 0; i < rest.length; i++) {
    fieldQuery = catIndexes[ array.slice(0, i + 2).join('.') ] + ACTIVE_AUCTIONS_FIELD;
    fieldQueries[ fieldQuery ] = incValue;
  }
  
  return {
    query: query,
    $set: { $inc: fieldQueries }
  };
}

function updateMany(model, queries, $sets, incValue) {
  if (queries)
    for (let key in queries)
      updateOne(model, queries[ key ], $sets[ key ]);
}

function updateOne(model, query, $set) {
  console.log(query, $set);
  if (query)
    model.updateOne(query, $set).exec();
}

module.exports = {
  markCategories,
  markProperties,
  unmarkCategories,
  unmarkProperties
};
