const
  CURRENT_PATH = '/routes/functions',
  ROOT_PATH = __dirname.replace(CURRENT_PATH, '');

require(`${ ROOT_PATH }/models/Category`);

const
  mongoose = require('mongoose'),
  Category = mongoose.model('category'),

  FIELDS_TO_WIPE = 'ogDescription metaDescription description'.split(' '),
  catIndexes = require('../../cat_indexes.js');


function createQuery(fields_to_wipe, index = '', set_to = '') {
  let results = {};

  for (let key of fields_to_wipe) {
    if (index)
      results[ [index, key].join('.') ] = set_to;
    else
      results[ key ] = set_to;
  }

  return results;
}

async function wipe() {
  if (!catIndexes)
    throw 'first, create category indexes';

  const
    categories = (await Category
      .find({})
      .select('name')
      .lean()
    ).map(cat => cat.name);

  for (let category of categories) {
    let
      preQuery = createQuery(FIELDS_TO_WIPE),
      indexes = Object
        .keys(catIndexes)
        .filter(index => index.startsWith(category));

    for (let index of indexes) {
      preQuery = { ...preQuery, ...createQuery(FIELDS_TO_WIPE, catIndexes[ index ]) };
      process.stdout.write('.');
    }

    //console.log(`\n\n\nWipe query for ${ category } :\n\n`, preQuery);
    await Category.updateMany({ name: category }, { $set: preQuery });
    console.log(`\n\n${ category } done.`);

  }

  console.log('\n\nall done.');
}

module.exports = wipe;
