async function globalDiscountActive(mongoose) {
  const
    Settings = mongoose.model('settings'),
    settings = await Settings
    .findOne({})
    .select('discountFinalDate');

  if (settings && settings.discountFinalDate)
    return (Date.now() <= new Date(settings.discountFinalDate).getTime());
  else
    return false;
}

module.exports = {
  globalDiscountActive
}
