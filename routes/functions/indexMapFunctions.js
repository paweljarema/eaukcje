const
  fs = require('fs'),

  CAT_INDEX_FILE = 'cat_indexes.js',
  PROP_INDEX_FILE = 'prop_indexes.js';

let
  map = {},
  propMap = {};

function createRootPropsFor(cat, dict) {
  if (cat.properties) {
    for (let i = 0; i < cat.properties.length; i++) {
      const
        prop = cat.properties[i],

        _id = [cat.name, prop.name].join('.'),
        _path = 'properties.' + i;

      dict[ _id ] = _path;
    }
  }
}

function createPositionalMapForCategories(categories, callback) {
  let maincats = Object.keys(categories);

  for (let key of maincats) {
    const cat = categories[key];
    createRootPropsFor(cat, propMap);
    createMapFor(cat.subcategories, cat.name, '', map, propMap);
  }

  const makeModule = (json) => 'module.exports=' + json + ';';

  // JSON.stringify(propMap, null, 2)
  fs.writeFile(PROP_INDEX_FILE, makeModule(JSON.stringify(propMap)), (err) => {
    if (err) throw err;
    fs.writeFile(CAT_INDEX_FILE, makeModule(JSON.stringify(map)), (err) => {
      if (err) throw err;
      if (callback) callback();
    });
  });
}

function createMapFor(cats, id, path, dict, propDict) {
  if (cats && cats.length) {
    for (let i = 0; i < cats.length; i++) {
      const
        cat = cats[i],

        _id = id ? [id, cat.name].join('.') : cat.name,
        _path_part = 'subcategories.' + i,
        _path = path ? [path, _path_part].join('.') : _path_part;

      dict[ _id ] = _path;

      // console.log(i, _id, _path);

      if (cat.properties) {
        for (let p = 0; p < cat.properties.length; p++) {
          const
            prop = cat.properties[p],

            _p_id = [_id, prop.name].join('.'),
            _p_path_part = 'properties.' + p,
            _p_path = [_path, _p_path_part].join('.');

          propDict[ _p_id ] = _p_path;

        }
      }

      if (cat.subcategories)
        createMapFor(cat.subcategories, _id, _path, dict, propDict);
    }
  }
}

module.exports = {
  createPositionalMapForCategories,
  createRootPropsFor,
  createMapFor,
  CAT_INDEX_FILE,
  PROP_INDEX_FILE
}
