function commonAdFilters(req) {
  return {
    'violations.hanged': { $ne: true },
    ended: { $ne: true }
  };
}

function timestampQuery(timestamp) {
  let millis = parseInt(timestamp);

  return {
    $or: [
      { created: millis },
      { 'date.start_date': millis }
    ]
  };
}

module.exports = {
  commonAdFilters,
  timestampQuery
};
