const
  mongoose = require('mongoose'),
  Category = mongoose.model('category'),
  Property = mongoose.model('property'),

  CLIENT_PATH = '../../client';

// CHANGE if changes :)
function parseCatData({ name, pos, level, ogDescription, metaTitle, metaDescription, description }) {
  return {
    name, pos, level, ogDescription, metaTitle, metaDescription, description
  };
}

function parseProperties(input) {
  if (input && input.properties) {
    let properties = [];
    for (let prop of input.properties)
      properties.push(
        new Property(prop)
      );

    return properties;

  } else return undefined;
}

function parseSubcategories(input) {
  let
    subcategories = [];

  if (input.subcategories && isArray(input.subcategories)) {
    let category;

    for (let index = 0; index < input.subcategories.length; index++) {

      subcategories.push(
        new Category({
          ...parseCatData(input.subcategories[ index ]),
          subcategories: parseSubcategories( input.subcategories[ index ] ),
          properties: parseProperties( input.subcategories[ index ] )
        })
      );
    }
  }

  if (subcategories.length)
    return subcategories;

  else return null;
}

function getCatTree() {
if (!mongoose)
  throw('getCatTree requires mongoose.');

  const parse = json => JSON.parse(json);

  let categories = {
    ...require(`${ CLIENT_PATH }/src/categories/Moda.json`),
    ...require(`${ CLIENT_PATH }/src/categories/Dom_i_ogród.json`),
    ...require(`${ CLIENT_PATH }/src/categories/Uroda.json`),
    ...require(`${ CLIENT_PATH }/src/categories/Kolekcje_i_sztuka.json`),
    ...require(`${ CLIENT_PATH }/src/categories/Kultura_i_rozrywka.json`),
    ...require(`${ CLIENT_PATH }/src/categories/Zdrowie.json`),
    ...require(`${ CLIENT_PATH }/src/categories/Elektronika.json`),
    ...require(`${ CLIENT_PATH }/src/categories/Zwierzęta.json`),
    ...require(`${ CLIENT_PATH }/src/categories/Sprzątanie.json`),
    ...require(`${ CLIENT_PATH }/src/categories/Motoryzacja.json`),
    //...require(`${ CLIENT_PATH }/src/categories/Sport_i_turystyka.json'),
    ...require(`${ CLIENT_PATH }/src/categories/Sport_i_rekreacja.json`),
    ...require(`${ CLIENT_PATH }/src/categories/Spożywcze.json`),
    ...require(`${ CLIENT_PATH }/src/categories/Dzieci_i_młodzież.json`),
    ...require(`${ CLIENT_PATH }/src/categories/Nieruchomości.json`),
    ...require(`${ CLIENT_PATH }/src/categories/Turystyka.json`),
  };

  let
    { samochodyOsobowe, dostawczeCiezarowe, motocykle } = require(`${ CLIENT_PATH }/src/categories/categories`);
    motoryzacja = categories.Motoryzacja;

  // delete: samochody z okazja.pl
  let samochody_index = motoryzacja.subcategories.findIndex(cat => cat.name === 'Samochody');
  if (samochody_index >= 0)
    motoryzacja.subcategories.splice(samochody_index, 1);

  function makeSubCat(name, props) {
    return {
      name: name,
      properties: props,

      activeAuctions: 0,
      ogDescription: '',
      metaTitle: '',
      metaDescription: '',
      description: ''
    };
  }

  motoryzacja.subcategories = [
    makeSubCat('Samochody osobowe', samochodyOsobowe),
    makeSubCat('Dostawcze i ciężarowe', dostawczeCiezarowe),
    makeSubCat('Motocykle', motocykle)
  ].concat(motoryzacja.subcategories);

  categories.Motoryzacja = motoryzacja;

  return categories;
}

function isArray(arr) {
  return Array.isArray(arr);
}

module.exports = {
  getCatTree,
  parseCatData,
  parseSubcategories,
  parseProperties
}
