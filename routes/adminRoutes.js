const mongoose = require('mongoose');
const { ObjectId } = mongoose.Types;
const Admin = mongoose.model('admin');
const User = mongoose.model('user');
const Invoice = mongoose.model('invoice');
const Auction = mongoose.model('auction');
const Violation = mongoose.model('violation');
const Rate = mongoose.model('rate');
const Category = mongoose.model('category');
// const Subcategory = mongoose.model('subcategory');
// const SubSubCategory = mongoose.model('sub_subcategory');
const Property = mongoose.model('property');
const Mailer = require('../services/Mailer');
const Symfonia = require('../services/Symfonia');
const userDeletedTemplate = require('../services/emailTemplates/userDeletedTemplate');
const mailUserTemplate = require('../services/emailTemplates/mailUserTemplate');
const bulkMailTemplate = require('../services/emailTemplates/bulkMailTemplate');
const mailAuctionTemplate = require('../services/emailTemplates/mailAuctionTemplate');
const auctionDeletedTemplate = require('../services/emailTemplates/auctionDeletedTemplate');
const contactEaukcjeTemplate = require('../services/emailTemplates/contactEaukcjeTemplate');
const business = require('../services/emailTemplates/business');
const requireLogin = require('../middleware/requireLogin');

const multer = require('multer');
const upload = multer();

const util = require('util');

const { invoicePdfName, invoiceSymfoniaName } = require('../functions/invoice');
const { getFinancialReport, getInvoicesForBatchExport } = require('../functions/report');
const { dateRangeMonthsToMillis } = require('../functions/date');
const { markCategories, markProperties, unmarkCategories, unmarkProperties } = require('./functions/markFunctions');
const { removeInvalidChars } = require('../client/src/functions/links');

const DOCUMENT_DOCTYPES = ['user', 'auction', 'invoice', 'violation'];

module.exports = app => {
	app.post('/api/contact_eaukcje', [requireLogin, upload.any()], async(req, res) => {
		const
			to 	 = 'kontakt@eaukcje.pl',
			from = req.user.contact.email,
			{ message } = req.body,
			mailer = new Mailer(
				{ subject: 'Pomoc eaukcje.pl', recipients: [{ email: to }] },
				contactEaukcjeTemplate(from, message)
			);

			mailer.send();
			req.session.message = 'Wysłano wiadomość. Odpowiemy niebawem';

			res.send(true);
	});

	app.post('/api/resolve_violation', async (req, res) => {
		const { auction_id, admin_id, page, per_page } = req.body;

		if (!await adminExists(admin_id)) {
			res.send(await paginateViolations(page, per_page));
			return;
		}

		let _id = ObjectId(auction_id);

		Violation
			.updateMany(
				{ _auction: _id },
				{ $set: { resolved: true }}
			)
			.exec();

		let auction = await Auction.findById(_id);

		auction.violations.hanged = false;
		auction.violations.hangedTo = null;

		auction.save();

		markCategories(auction.categories);
		markProperties(auction.property_paths);

		req.session.message = 'Oznaczono naruszenie jako rozwiązane.';
		res.send(await paginateViolations(page, per_page));
	});

	app.post('/api/hang_auction', async (req, res) => {
		const { auction_id, admin_id, page, per_page } = req.body;

		if (!await adminExists(admin_id)) {
			res.send(await paginateViolations(page, per_page));
			return;
		}

		let auction = await Auction
			.findById(ObjectId(auction_id));

		if (!auction.violations)
			auction.violations = {};

		auction.violations.hanged = true;
		auction.violations.hangedTo = Date.now() + (1000 * 60 * 60 * 24 * 3);

		unmarkCategories(auction.categories);
		unmarkProperties(auction.property_paths);

		auction.save();

		req.session.message = 'Aukcja została czasowo zawieszona';
		res.send(await paginateViolations(page, per_page));
	});

	app.post('/api/delete_auction', async (req, res) => {
		const { auction_id, email, title, reason, admin_id, doctype, page, per_page } = req.body;

		let auctions;

		if (!await adminExists(admin_id)) {
			res.send(await paginateAuctions(page, per_page));
			return;
		}

		let id = ObjectId(auction_id);

		let auction = await Auction.findById(id);
		unmarkCategories(auction.categories);
		unmarkProperties(auction.property_paths);

		Violation.deleteMany({ _auction: id }).exec();
		auction.remove()
			.then(
				async doc => {
					req.session.message = 'Usunięto aukcję i przesłano wiadomość';
					const mailer = new Mailer(
						{
							subject: 'Wiadomość administracyjna z portalu ' + business.name,
							recipients: [{ email }]
						},
						auctionDeletedTemplate(title, reason)
					);

					await mailer.send();

					res.send(await paginateAuctions(page, per_page));
				},
				async err => {
					console.log(err);
					req.session.error = 'Nastąpił błąd. Spróbuj później';
					res.send(await paginateAuctions(page, per_page));
				}
			);
    });

	app.post('/api/mail_all_users', upload.any(), async (req, res) => {
		const { admin_id, subject, message } = req.body;

		if (!await adminExists(admin_id)) {
			res.send(false);
			return;
		}

		const users = await User.find({ 'agreements.corespondence': true });

		let recipients = [];
		for (let i = 0, l = users.length; i < l; i++) {
			recipients.push({ email: users[i].contact.email });
		}

		const mailer = new Mailer({ subject, recipients }, bulkMailTemplate(subject, message));
		await mailer.send();

		req.session.message = 'Wysłano wiadomość do bazy mailingowej';
		res.send(true);
	});

	app.post('/api/mail_user', upload.any(), async (req, res) => {
		const { admin_id, email, title, subject, message } = req.body;

		if ((!await adminExists(admin_id)) || !email || !message) {
			console.log(req.body);
			res.send(false);
			return;
		}

		let mailer;
		if (title) {
			mailer = new Mailer({ subject: ('E-mail w sprawie przedmiotu ' + title), recipients: [{ email }] }, mailAuctionTemplate(title, subject, message));
		} else {
			mailer = new Mailer({ subject, recipients: [{ email }] }, mailUserTemplate(subject, message));
		}

		const response = await mailer.send();

		req.session.message = 'Wysłano wiadomość';
		res.send(true);
	});

	app.post('/api/delete_user', async (req, res) => {
		const { user_id, reason, admin_id, doctype, page, per_page } = req.body;

		if (!await adminExists(admin_id)) {
			res.send(await paginateUsers(page, per_page));
			return;
		}

		const _user = ObjectId(user_id);
		const user = await User.findOne({ _id: _user });
		const mailer = new Mailer(
			{
				subject: 'Wiadomość administracyjna z portalu ' + business.name,
				recipients: [{ email: user.contact.email }]
			},
			userDeletedTemplate(reason)
		);

		await mailer.send();
		User.deleteOne({ _id: _user })
			.then(
				async doc => {

					const auctions = await Auction.find({ _user: _user }).select('_id categories property_paths');
					for (let i = 0; i < auctions.length; i++) {
						let auction = auctions[i];

						unmarkCategories(auction.categories);
						unmarkProperties(auction.property_paths);

						Violation.deleteMany({ _auction: auction._id }).exec();
						Auction.deleteOne({ _id: auction._id }).exec();

					}

					req.session.message = 'Usunięto konto użytkownika i przesłano wiadomość';
					res.send(await paginateUsers(page, per_page));
				},
				async err => {
					console.log(err);
					req.session.error = 'Nastąpił błąd. Spróbuj później';
					res.send(await paginateUsers(page, per_page)); }
			);
	});

	app.post('/api/fetch_documents', async (req, res) => {
		const { admin_id, doctype, page, per_page } = req.body;

		if (!await adminExists(admin_id)) {
			res.send(false);
			return;
		}

		switch (doctype) {
			case 'user':
				res.send(await paginateUsers(page, per_page));
				break;
			case 'auction':
				res.send(await paginateAuctions(page, per_page));
				break;
			case 'invoice':
				res.send(await paginateInvoices(page, per_page, req.body.from, req.body.to));
				break;
			case 'violation':
				res.send(await paginateViolations(page, per_page));
				break;
		}
	});

	app.post('/api/provision', async (req, res) => {
		const { admin_id, provision, premium7daysPrice, premiumForeverPrice } = req.body;

		if (!await adminExists(admin_id)) {
			res.send(false);
			return;
		}

		admin.provision = parseInt(provision);
		admin.premium7daysPrice = parseInt(premium7daysPrice);
		admin.premiumForeverPrice = parseInt(premiumForeverPrice);

		admin
			.save()
			.then(
				doc => {
					req.session.message = 'Zapisano wysokość prowizji';
					req.session.admin = doc;
					res.send(doc);
				},
				err => {
					console.log(err);
					req.error.message = 'Nastąpił błąd. Spróbuj później';
					res.send(admin);
				}
			);
	});

	app.get('/api/tech_break', async (req, res) => {
		const admin = await Admin.findOne({});
		if (admin) {
			res.send({
				techbreak: admin.techbreak || false,
				provision: admin.provision,
				premium7daysPrice: admin.premium7daysPrice,
				premiumForeverPrice: admin.premiumForeverPrice
			});

			return;
		} else {
			res.send({
				techbreak: false,
				provision: 5,
				premium7daysPrice: 6,
				premiumForeverPrice: 12
			});
		}
	});

	app.post('/api/tech_break', async (req, res) => {
		const { admin_id } = req.body;

		if (!await adminExists(admin_id)) {
			res.send(false);
			return;
		}

		admin.techbreak = admin.techbreak ? !admin.techbreak : true;
		admin
			.save()
			.then(
				doc => { res.send(admin); },
				err => { console.log(err); res.send(admin) }
			);

	});

	app.get('/api/symfonia_batch_export/:admin/:from/:to/:month', async (req, res) => {
		const { admin, from, to, month = '' } = req.params;

		if (!await adminExists(admin)) {
			res.send(false);
			return;
		}

		let
			invoices = await getInvoicesForBatchExport(mongoose, { from, to }),
			symfonia = new Symfonia(invoices);

		symfonia.sendFileViaExpress(res, 'symfonia_eksport_' + removeInvalidChars(month) + '.txt');

	});

	app.get('/api/faktura/:admin/:id/:see', async (req, res) => {
		let
			{ admin, id, see } = req.params;

		if (await adminExists(admin)) {
			let invoice = await Invoice
				.findById(ObjectId(id))
				.select('number pdf'),

				head = {
					'Content-Type': 'application/pdf',
					'Content-Disposition': `attachment; filename="${ invoicePdfName(invoice) }"`
				};

			if (Boolean(parseInt(see))) delete head['Content-Disposition'];

			res.writeHead(200, head);
			res.end(
				Buffer.from(invoice.pdf, 'base64')
			);

		} else {
			res.send({ 'status': '401 forbidden' });
		}
	});

	app.get('/api/symfonia/:admin/:id', async (req, res) => {
		let { admin, id, see } = req.params;

		if (await adminExists(admin)) {
			let
				invoice = await Invoice
					.findById(ObjectId(id))
					.select('-html -pdf')
			    .lean(),

				symfonia = new Symfonia([invoice]);

			symfonia.sendFileViaExpress(res, invoiceSymfoniaName(invoice));

		} else {

			res.send({ status: '401 forbidden' });
		}
	});

	app.post('/api/get_invoice_report', async (req, res) => {
		const { admin_id, from, to } = req.body;

		if (!await adminExists(admin_id)) {
			res.send(false);
			return;
		}

		res.send(await getFinancialReport(mongoose, { from, to }));
	});

	app.get('/api/ct', async (req, res) => {
		let
			CreditTransaction = mongoose.model('creditTransaction'),

			count = 0,
			invoices = await Invoice.find({}).lean();

		for (let invoice of invoices) {
			new CreditTransaction({
				date: new Date(invoice.created).getTime(),
				_user: invoice._user,
				_auction: null,
				promoCode: 0,
				qty: 1,
				vat: invoice.vat || 23,
				total: 21000 || invoice.total,
				done: true
			}).save();

			count++;
		}

		res.send({ 'credit transactions created': count })
	});
}

async function adminExists(id) {
	return Boolean(await Admin.countDocuments({ _id: ObjectId(id) }).limit(1));
}

async function fetchOpinions(user_id) {
	const opinions = await Rate.find({ _user: ObjectId(user_id) }, { rate: 1 });
	const count = opinions.length;
	const rate = count ? (opinions.map(opinion => opinion.rate).reduce((a, b) => a + b) / count).toFixed(2) : 0;

	return ({ count, rate });
}

async function paginate(model, { page, per_page, query = {}, sort = {} }) {
	 let docs = await model
		.find(query)
		.sort(sort)
		.skip(parseInt( (page - 1) * per_page))
		.limit(parseInt( per_page ))
		.lean();

	let count = Math.ceil(await model.countDocuments(query) / parseInt(per_page));

	return { docs, count };
}

async function paginateAuctions(page, per_page) {
	let { docs, count } = await paginate(Auction, { page, per_page, sort: { 'date.start_date': -1 } });

	for (let i = 0; i < docs.length; i++) {
		let
			doc = docs[i],
			user = await User.findOne(
				{ _id: doc._user },
				{ firstname: 1, lastname: 1, contact: 1 }
			);

		if (user) {
			doc.owner = `${user.firstname || ''} ${user.lastname || (!user.firstname ? 'Anonim' : '')}`;
			doc.email = user.contact.email;
		}
	}

	return ({ 'auction': docs, count });
}

async function paginateUsers(page, per_page) {
	let { docs, count } = await paginate(User, { page, per_page, sort: { joindate: -1 } });

	for (let i = 0; i < docs.length; i++) {
		const user = docs[i];
		user.opinion = await fetchOpinions(user._id);
	}

	return ({ 'user': docs, count });
}

async function paginateInvoices(page, per_page, from, to) {
	let
	 	{ dateFrom, dateTo, parsed } = dateRangeMonthsToMillis(from, to),
		{ docs, count } = await paginate(
			Invoice,
			{
				page,
				per_page,
				query: parsed ? { created: { $gte: dateFrom, $lte: dateTo }} : null,
				sort: { created: -1 }
			}
		);

	for (let i = 0; i < docs.length; i++)
		docs[i].user = await User.findById(docs[i]._user);

	return ({ 'invoices': docs, count });
}

async function paginateViolations(page, per_page) {
	let
		_page = parseInt(page),
		_per_page = parseInt(per_page);

	let
		violations = await Violation.aggregate([
			{
				$match: { resolved: { $ne: true }}
			},
			{
				$sort: { date: -1 },
			},
			{
				$group: {
					_id: '$_auction',

					users: { $push: '$_user' },
					groups: { $push: '$group' },
					reasons: { $push: '$reason' },
					dates: { $push: '$date' },

					resolved: { $push: '$resolved' }
				}
			},
			{
				$skip: ( _page - 1 ) * _per_page
			},
			{
				$limit: _per_page
			}
		]),

		count = (await Violation.distinct('_auction')).length;

	for (let i = 0; i < violations.length; i++) {
		let group = violations[ i ];

		group.auction = await Auction
			.findById(group._id)
			.populate('_user');
	}

	return ({ 'violation': violations, count: Math.ceil(count / _per_page) });
}

function isNotEmpty(object) {
  if (Object.prototype.toString.call(object) === '[object Array]') {
    return object.length > 0;
  }

  return Boolean(object);
}
