const mongoose = require('mongoose');
const Category = mongoose.model('category');

const CACHE = {
  categories: undefined,
  updated: 0,
  ttl: 24 * 60 * 60 * 1000
};

module.exports = app => {
    app.get('/api/categories', async (req, res) => {
      const time = Date.now();
      const cacheValid = !!CACHE.categories && (CACHE.updated + CACHE.ttl >= time);

      if (cacheValid) {
        res.send(CACHE.categories);
        console.log('Serving categories from cache.');

      } else {

        const categories = await Category.find({}, (err, categories) => {
          if (err) {
            console.log(err);

          } else {
            CACHE.categories = categories;
            CACHE.updated = time;
            console.log('Category cache updated.')
            res.send(categories);
          }
        });
      }
    });
}
