const mongoose = require('mongoose');
const { ObjectId } = mongoose.Types;
const util = require('util');
const fs = require('fs');
const shortid = require('shortid');

const Auction = mongoose.model('auction');
const User = mongoose.model('user');
const Category = mongoose.model('category');
// const Subcategory = mongoose.model('subcategory');
// const SubSubCategory = mongoose.model('sub_subcategory');
const
  Property = mongoose.model('property'),

  { saveSettings } = require('../functions/settings'),
  { isArray } = require('../client/src/functions/array'),

  { markCategories } = require('./functions/markFunctions');

            // const propertySchema = new Schema({
            // 	name: String,
            // 	type: String,
            // 	unit: String,
            // 	icon: String,
            // 	values: [String],
            // 	conditional_values: Schema.Types.Mixed
            // });
            //
            // const categorySchema = new Schema({
            //     name: String,
            //     icon: String,
            // 		 properties: [propertySchema],
            //     subcategories: [this]
            // });

const
    { appAuthSecret } = require('../config/keys'),
    md5 = require('md5'),
    axios = require('axios');

// kategoria, tytuł i cena, atrybuty, opis
function inspect(object, comment = '') {
  console.log('\n\n');
  console.log('----------  ' + comment.toUpperCase() + '  ----------');
  console.log('\n\n');
  console.log(util.inspect(object, false, null, true));
}

function random(min, max) {
  return parseInt( Math.random() * (max - min) + min );
}

module.exports = app => {
  app.get('/api/shortid', async (req, res) => {
    let auctions = (
      await Auction
        .find({})
        .select('_id')
      ).map(a => a._id);

    for (let i = 0, l = auctions.length; i < l; i++) {
      Auction.updateOne(
        { _id: auctions[i] },
        { $set: { _shortid: shortid.generate() }}
      ).exec();
    }

    res.send({ changed: auctions.length });
  });

  app.get('/api/set_created', async (req, res) => {
    let auctions = await Auction
      .find({})
      .sort({ 'date.start_date': 1 })
      .select('_id')
      .lean();

    for (let i = 0, l = auctions.length; i < l; i++) {
      let
        date = Date.now(),
        id = auctions[i]._id;

      Auction.findByIdAndUpdate(id, { $set: { created: date + i * 10 }}).exec();
    }

    res.send({
      'set created fields': true
    })
  });

    app.get('/api/inc_active', async (req, res) => {
      const auctions = await Auction
        .find({ ended: { $ne: true }, 'violations.hanged': { $ne: true }})
        .select('categories')
        .lean();

      for (let i = 0, l = auctions.length; i < l; i++) {
        let auction = auctions[i];
        markCategories(auction.categories);
      }

      res.send({
        '$inc activeCategories': true,
        'warning': 'should not run twice!'
      })
    });

    app.get('/api/seedauctions', async (req, res) => {
        const howMany = 200;

        const users = await User.find({}, { _id: 1}).lean();
        const user_ids = users.map(user => user._id);

        for (let i = 0; i < howMany; i++) {

            const category_keys = Object.keys(seedData);
            const main = category_keys[random(0, category_keys.length - 1)];
            const sub_categories = seedData[main];

            const sub = sub_categories[random(0, sub_categories.length - 1)];


            const _user = ObjectId(user_ids[random(0, user_ids.length)]);
            const title = titles[random(0, titles.length - 1)];
            const start_price = random(50, 2500);
            const attr_name = attributes[random(0, attributes.length - 1)];
            const attribute = { name: attr_name, value: values[attr_name][random(0, values[attr_name].length - 1)] };
            const quantity = random(1, 20);
            const shortdescription = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi.';

            const start_date = new Date().getTime();

            await new Auction({
                _user,
                title,
                shortdescription,
                price: { start_price, current_price: start_price },
                date: { start_date, duration: 10 },
                quantity: quantity,
                photos: [],
                attributes: [attribute],
                categories: { main, sub },
                verified: true

            }).save().then(() => console.log(title + ' created'), (err) => console.log('error', err));
        }

    });
}
