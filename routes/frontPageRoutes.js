const
  express = require('express'),
  frontPageRouter = express.Router(),

  mongoose = require('mongoose'),
  Auction = mongoose.model('auction'),

  { parseInts } = require('../functions/numbers'),

  AUCTION_LIMIT = 10;


frontPageRouter.get('/:category/:limit', async (req, res) => {
  const
    [ page, limit ] = parseInts( req.params.page, req.params.limit ),

    auctions = await Auction
      .find({
        ended: { $ne: true },
        'violations.hanged': { $ne: true },
        categories: req.params.category,
      })
      .select('title shortdescription price date created categories _shortid')
      .sort({ 'date.start_date': -1 })
      .limit(limit)
      .lean();

  res.send(auctions);
});

module.exports = frontPageRouter;
