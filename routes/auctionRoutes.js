const requireLogin          = require('../middleware/requireLogin');
const multer                = require('multer');
const upload                = multer();
const util                  = require('util');

const mongoose              = require('mongoose');
const { ObjectId }          = mongoose.Types;
const User                  = mongoose.model('user');
const Auction               = mongoose.model('auction');
const Violation             = mongoose.model('violation');
const Category              = mongoose.model('category');
const Rate                  = mongoose.model('rate');
const Like                  = mongoose.model('like');
const Views                 = mongoose.model('views');

const Imagemin              = require('imagemin');
const imageminPngquant      = require('imagemin-pngquant');
const imageminSvgo          = require('imagemin-svgo');
const imageminMozjpeg       = require('imagemin-mozjpeg');
const imageminGifsicle      = require('imagemin-gifsicle');

const Sharp = require('sharp');

const buyNowWonTemplate         = require('../services/emailTemplates/buyNowWonTemplate');
const buyNowItemSoldTemplate    = require('../services/emailTemplates/buyNowItemSoldTemplate');
const bidTemplate               = require('../services/emailTemplates/bidTemplate');
const itemSentTemplate          = require('../services/emailTemplates/itemSentTemplate');
const Mailer                    = require('../services/Mailer');

const helpers = require('../services/helpers/otherHelpers');
const { filterUnique } = require('../client/src/functions/array');
const { getItemsToSend, getItemsToRate, moveUserFromSendToRate, removeUserFromRateArray, makeRow, clearUserCache } = require('./sendItemAndRateFunctions');
const { markCategories, markProperties, unmarkCategories, unmarkProperties } = require('./functions/markFunctions');
const { timestampQuery } = require('./functions/filterFunctions');
const { globalDiscountActive } = require('./functions/adminFunctions');

// const util = require('util');

const currentUserId = (req) => {
  if (req.user)
    return ObjectId(req.user._id);
  else
    return null;
}

module.exports = app => {
    app.post('/auction/rate_buyer', requireLogin, async (req, res) => {
        const rate = req.body;

        rate._user = ObjectId(rate._user);
        rate._rater = ObjectId(req.user._id);

        await removeUserFromRateArray(req.user, ObjectId(rate._auction), rate._user, rate.buynow, rate.count);

        delete rate.buynow;
        delete rate._auction;
        delete rate.count;

        const newRate = new Rate(rate).save().then(
            doc => res.send(true),
            err => res.send(false)
        );
    });

    app.post('/auction/send_items', requireLogin, async (req, res) => {
        const
            { auction_id, auction_title, buynow, count, user_id, user_email } = req.body;
            data = {
                subject: 'Właśnie wysłano do Ciebie przedmiot: ' + auction_title,
                recipients: [{ email: user_email }]
            },
            auction = await Auction.findById(ObjectId(auction_id)),

            mailer = new Mailer(data, itemSentTemplate(auction));

        mailer.send();

        const
            _auction_id = ObjectId(auction_id),
            _user_id = ObjectId(user_id),
            auction = await Auction.findOne({ _id: _auction_id });

        helpers.sendChatMessageOnItemSend(_user_id, auction._user, auction, 0, count);
        await moveUserFromSendToRate(req.user, _auction_id, _user_id, buynow, count);

        res.send(true);
    });

    app.post('/auction/send_and_rate_rows', requireLogin, async (req, res) => {
        const
            rows = [],
            owner = req.user;

        let
            data, auction, buyers, i, j;

        console.log('request rows');

        data = await getItemsToSend(owner, false);
        for (let i = 0; i < data.length; i++) {
            console.log('auction item found');

            auction = data[i];
            buyers = auction.auctionpaid;

            for (let j = 0; j < buyers.length; j++) {
                console.log('making row for items to send auction');
                rows.push(await makeRow(
                    buyers[j],
                    auction,
                    false,
                    true,
                    false
                ));
            }
        }

        data = await getItemsToSend(owner, true);
        for (let i = 0; i < data.length; i++) {

            auction = data[i];
            buyers = auction.buynowpaid.filter(filterUnique);

            for (let j = 0; j < buyers.length; j++) {
                console.log('making row for items to send buynow');
                rows.push(await makeRow(
                    buyers[j],
                    auction,
                    true,
                    true,
                    false
                ));
            }
        }

        data = await getItemsToRate(owner, false);
        for (let i = 0; i < data.length; i++) {

            auction = data[i];
            buyers = auction.auctionpaid;

            for (let j = 0; j < buyers.length; j++) {
                console.log('making row for users to rate auction');
                rows.push(await makeRow(
                    buyers[j],
                    auction,
                    false,
                    false,
                    true
                ));
            }
        }

        data = await getItemsToRate(owner, true);
        for (let i = 0; i < data.length; i++) {

            auction = data[i],
            buyers = auction.buynowpaid.filter(filterUnique);

            for (let j = 0; j < buyers.length; j++) {
                console.log('making row for users to rate buynow');
                rows.push(await makeRow(
                    buyers[j],
                    auction,
                    true,
                    false,
                    true
                ));
            }
        }

        clearUserCache();

        console.log(rows);
        res.send(rows);
    });

    app.get('/auction/:id/photo', async (req, res) => {
        const { id }    = req.params,
                auction = await Auction.findOne({ _id: ObjectId(id) }, { photos: { $slice: 1 } }),
                photo   = auction.photos[0];

        if (!photo) {
            res.send(false);
            return;
        }

        const img       = Buffer.from(photo.data, 'base64');

        res.writeHead(200, {
            'Content-Type': photo.type || 'image/jpeg',
            'Content-Length': img.length
        });
        res.end(img);
    });

    app.get('/auction/:shortid/photos_via_shortid', async (req, res) => {
      const auction = await Auction
        .findOne({ _shortid: req.params.shortid })
        .select('photos')
        .lean();

      if (auction)
        res.send(auction.photos);
      else
        res.send(false);
    });

    // LEGACY
    // app.get('/auction/:timestamp/photos_by_timestamp', async (req, res) => {
    //   const { timestamp } = req.params;
    //   const auction = await Auction
    //     .findOne(timestampQuery(timestamp))
    //     .select('photos')
    //     .lean();
    //
    //   if (auction)
    //     res.send(auction.photos);
    //   else
    //     res.send(false);
    // });
    //
    //
    // app.get('/auction/:id/photos', async (req, res) => {
    //     const { id } = req.params;
    //     const auction = await Auction.findOne({ _id: ObjectId(id) }, { photos: 1 });
    //     res.send(auction.photos);
    // });
    // end of LEGACY

    app.post('/auction/rate', requireLogin, async (req, res) => {
        const rate = req.body;

        const auction = await Auction.findOne({ _id: ObjectId(rate._auction)});
        auction.rated = true;
        auction.raters = auction.raters.filter(id => String(id) !== String(req.user._id));

        rate._user = ObjectId(rate._user);
        rate._rater = ObjectId(req.user._id);

        delete rate._auction;
        const newRate = new Rate(rate).save().then(
            doc => {
                auction.save().then(
                    doc => {
                        req.session.message = 'Pomyślnie dodano opinię';
                        res.send({});
                    },
                    err => {
                        console.log(err);
                        req.session.error = 'Błąd przy zapisie stanu aukcji. Spróbuj później';
                        res.send({});
                    }
                );
            },
            err => {
                console.log(err);
                req.session.error = 'Nastąpił nieoczekiwany błąd. Spróbuj później';
                res.send({});
            }
        );
    });


    app.post('/auction/delete/:id', requireLogin, async (req, res) => {
        const id = req.params.id;

        Violation.deleteMany({ _auction: ObjectId(id) }).exec();
        const auction = await Auction.findOne({ _id: ObjectId(id) });
        unmarkCategories(auction.categories);
        unmarkProperties(auction.property_paths);

        await auction.remove().then(
            () => { req.session.message = 'Skasowano aukcję'; res.send({}); },
            err => { console.log(err); req.session.error = 'Nastąpił błąd'; res.send({}); }
        );
    });

    app.post('/auction/my_bids/:page/:per_page', requireLogin, async (req, res) => {
        const { page, per_page } = req.params,
                mode             = req.body.mode,
                user_id          = req.user._id;

        let query;
        if (mode === 'ended') {
            query = {
                $or: [
                    {
                        $and: [
                            { bids: { $elemMatch: { _user: user_id }}},
                            { ended: (mode === 'ended' ? true : ({ $ne: true })) }
                        ]
                    },
                    {
                        $or: [
                            { buynowpayees: user_id },
                            { payees: user_id },
                            { buynowpaid: user_id },
                            { raters: user_id }
                        ]
                    }
                ],

            };
        } else {
            query = {
                bids: { $elemMatch: { _user: user_id }},
                ended: (mode === 'ended' ? true : ({ $ne: true }))
            };
        }
        const projection = { _shortid: 1, _user: 1, title: 1, shortdescription: 1, price: 1, bids: 1, date: 1, ended: 1, rated: 1, payees: 1, buynowpayees: 1, raters: 1, created: 1, categories: 1 }; // photos: { $slice: 1 },
        const options = { skip: (+page - 1) * +per_page, limit: +per_page, sort: { 'date.start_date': 1 }};

        const auctions = await Auction.find(
            query,
            projection,
            options
        ).lean();


        const count = await Auction.countDocuments(query);
        auctions.push(count);

        if (count === 0) {
            res.send(false);
        } else {
            res.send(auctions);
        }
    });

    app.post('/auction/my_liked_bids/:page/:per_page', requireLogin, async (req, res) => {
        const { page, per_page } = req.params;

        const likes = await Like.find({ _user: ObjectId(req.user._id) })
            .lean();
        const idArray = likes.map(like => ObjectId(like._auction));

        const query = { _id: { $in: idArray }, ended: { $ne: true }};
        const projection = { _shortid: 1, _user: 1, title: 1, shortdescription: 1, price: 1, bids: 1, date: 1, ended: 1, rated: 1, payees: 1, buynowpayees: 1, raters: 1, created: 1, categories: 1 }; // photos: { $slice: 1 },
        const options = { skip: (+page - 1) * +per_page, limit: +per_page, sort: { 'date.start_date': 1 }};

        const auctions = await Auction.find(
            query,
            projection,
            options
        ).lean();

        const count = await Auction.countDocuments(query);
        auctions.push(count);

        if (count === 0) {
            res.send(false);
        } else {
            res.send(auctions);
        }
    });

    app.post('/auction/my_auctions/:page/:per_page', requireLogin, async (req, res) => {
        const { page, per_page } = req.params;
        const mode = req.body.mode;

        const query = { _user: req.user._id, ended: (mode === 'ended' ? true : ({ $ne: true })) };
        const projection = { _shortid: 1, title: 1, shortdescription: 1, price: 1, bids: 1, date: 1, ended: 1, created: 1, categories: 1, premium: 1 }; // photos: { $slice: 1 },
        const options = { skip: (+page - 1) * +per_page, limit: +per_page, sort: { 'date.start_date': 1 }};

        const auctions = await Auction.find(
            query,
            projection,
            options
        ).lean();

        const count = await Auction.countDocuments(query);
        auctions.push(count);

        if (count === 0) {
            res.send(false);
        } else {
            res.send(auctions);
        }
    });

    app.post('/auction/buy_now/:id', requireLogin, async (req, res) => {
        const user = req.user;
        const auction_id = req.params.id;

        const auction = await Auction.findById(ObjectId(auction_id));
        const owner = await User.findById(ObjectId(auction._user));

        auction.quantity = auction.quantity - 1;
        if (auction.buynowpayees) {
            auction.buynowpayees.push(user._id);
        } else {
            auction.buynowpayees = [user._id];
        }

        auction.bids = auction.bids.filter(bid => String(bid._user) !== String(user._id));

        if (auction.quantity === 0) {
            auction.ended = true;
        }

        await auction.save().then(
            async doc => {
                req.session.message = 'Kupiłeś ' + auction.title;
                res.send(await addBidders(doc));

                buyNowNotifications(user, owner, doc);
            },
            async err => {
                console.log(err);
                req.session.error = 'Nastąpił błąd. Spróbuj ponownie';
                res.send(await addBidderd(auction));
            }
        );
    });

    app.post('/auction/bid/:id', [requireLogin, upload.any()], async (req, res) => {
      const
        date = new Date().getTime(),
        price = parseInt(req.body.bid),
        auction_id = req.params.id,

        auction = await Auction.findById(ObjectId(auction_id)),
        bids = auction.bids;

      let
        returnMessage = 'Wziąłeś udział w licytacji.';

      if (price <= auction.price.start_price || price <= auction.price.current_price) {
        res.send(false);
        return;
      }

      if (bids.length) {
        let
          existingUserBids = bids.filter(bid => String(req.user._id) === String(bid._user)),
          highestBid = bids.reduce((bid1, bid2) => bid1.price >= bid2.price ? bid1 : bid2),
          highestBidIndex = bids.findIndex(bid => bid.price === highestBid.price),
          highestPrice = highestBid.price,

          newPrice = price,

          newBid = {
            date: date,
            _user: req.user._id,
            price: price
          };

        if (price > highestPrice) {
          newPrice = price > (highestPrice + 10) ? (highestPrice + 10) : price;
          win = true;

        } else if (price === highestPrice) {

          newPrice = highestPrice;
          newBid.price = highestPrice - 1;

        } else if (price < highestPrice) {

          newPrice = (price + 10) < highestPrice ? price + 10 : highestPrice;
        }

        if (existingUserBids.length) {
          let prevBidIndex = bids.findIndex(bid => String(req.user._id) === String(bid._user));
          auction.bids[prevBidIndex].date = newBid.date;
          auction.bids[prevBidIndex].price = newBid.price;
        } else {
          auction.bids.push(newBid);
        }

        auction.price.current_price = newPrice;

      } else {
        let
          newBid = {
            date: date,
            _user: req.user._id,
            price: price
          },

          newPrice = price > (auction.price.start_price + 10) ? auction.price.start_price + 10 : price;

        auction.price.current_price = newPrice;
        auction.bids = [newBid];
        returnMessage += ' Jesteś pierwszy.';
      }


      const bidder_ids = auction.bids.map(bid => ObjectId(bid._user));
      const bidders = await User.find({ _id: { $in: bidder_ids }}, { firstname: 1, lastname: 1 }).lean();

      auction.bids = auction.bids.sort((bid_1, bid_2) => {
          if (bid_1.price > bid_2.price) return -1;
          if (bid_1.price < bid_2.price) return 1;
          return 0;
      });

      let bidMessage = 'Podbij stawkę, aby wyjść na prowadzenie.';
      if (String(auction.bids[0]._user) === String(req.user._id)) {
          bidMessage = 'Gratulujemy, w chwili obecnej prowadzisz w licytacji!';
      }

      const mailer = new Mailer({
          subject: 'Wziąłeś udział w licytacji przedmiotu ' + auction.title,
          recipients: [{ email: req.user.contact.email }]
      }, bidTemplate(auction, bidMessage, price));
      mailer.send();

      await auction
          .save()
          .then(async doc => {
              req.session.message = bidMessage;
              doc = doc.toObject();

              let bidders_object = {};

              bidders.map(bidder => (bidders_object[bidder._id] = bidder ));
              doc.bidders = bidders_object;

              res.send(doc);
          },
          err => {
              req.session.error = 'Nastąpił błąd podczas licytacji. Spróbuj jeszcze raz';
              console.log(err);
              auction = auction.toObject();

              let bidders_object = {};
              bidders.map(bidder => (bidders_object[bidder._id] = bidder ));
              auction.bidders = bidders_object;

              res.send(auction);
          });
    });

    app.post('/auction/like/:id', requireLogin, async (req, res) => {
        const   id          = req.params.id,
                likeBool    = req.body.like,
                auction     = await Auction.findOne({ _id: ObjectId(id) }),
                likeObj     =  { _user: ObjectId(req.user._id), _auction: ObjectId(auction._id) };

        let     like        = await Like.findOne(likeObj);

        if (likeBool && !like) {
            auction.likes   = auction.likes ? auction.likes + 1 : 1;
            await new Like(likeObj).save();
        } else if (!likeBool && like) {
            auction.likes   = auction.likes - 1;
            await like.remove();
        }

        await auction.save();
    });

    app.get('/auction/edit/:id', requireLogin, async (req, res) => {
        const id = req.params.id;
        let auction = await Auction.findOne(
            { _id: ObjectId(id) },
            {}
        ).lean();
        const bidder_ids = auction.bids.map(bid => ObjectId(bid._user));
        const bidders = await User.find({ _id: { $in: bidder_ids }}, { firstname: 1, lastname: 1 }).lean();

        if (req.user) {
            const liked = await Like.findOne({ _user: req.user._id, _auction: auction._id });
            auction.liked = Boolean(liked);
        }

        let bidders_object = {};
        bidders.map(bidder => (bidders_object[bidder._id] = bidder ));
        auction.bidders = bidders_object;

        res.send(auction);
    });

    app.get('/auction/get_via_shortid/:shortid', async (req, res) => {
      let auction = await Auction
        .findOne({ _shortid: req.params.shortid })
        .select('-photos')
        .lean();

      if (auction) {
        auction.liked = await checkIfLiked(req.user, auction);
        auction.bidders = await getBidders(auction);

        res.send(auction);
      } else {
        res.send({ error: true });
      }
    });

    app.post('/auction/front_page_auctions/:mode', async (req, res) => {
        const
            { mode } = req.params,
            { itemCount, itemsPerFetch } = req.body;

        let
            query = {
              ended: { $ne: true },
              'violations.hanged': { $ne: true }
            };

        switch(mode) {
            case 'promoted':
                query['premium.isPremium'] = true;
                break;
            case 'new':
                break;
        }

        const auctions = await Auction.find(
            query,
            { _shortid: 1, title: 1, shortdescription: 1, price: 1, date: 1, created: 1, categories: 1 },
            {
                sort: { 'date.start_date': -1 },
                skip: parseInt(itemCount),
                limit: parseInt(itemsPerFetch)
            }
        );

        res.send(auctions);
    });

    app.get('/auction/get_front_page_auctions', async (req, res) => {
        const
          user_id = currentUserId(req),

          popular = await rotatePremium(),

          newest = await Auction.find(
            { ended: { $ne: true }, 'violations.hanged': { $ne: true } },
            { _shortid: 1, title: 1, shortdescription: 1, price: 1, date: 1, created: 1, categories: 1 }, // photos: { $slice: 1 },
            { sort: { 'date.start_date': -1 }, limit: 12 }
          );

        res.send({ popular, newest });
    });

    app.post('/auction/filter', upload.any(), async (req, res) => {
        let { seller, title, sort, page, per_page } = req.body,
            max = 9999999,
            min = 1,
            showPromotedOnly = sort === 'promowane';

        if (per_page > 100) per_page = 100;
        if (per_page < 1) per_page = 1;

        const
            // category        = req.body.category || null,
            // subcategory     = req.body.subcategory || null,
            // subsubcategory  = req.body.subsubcategory || null,

            categories = req.body.categories;

         // console.log('CATEGORIES FILTER', categories);
         // console.log('BODY', req.body);
         // console.log(category, subcategory, subsubcategory);

        const
            keys                    = Object.keys(req.body),
            property_keys           = keys.filter((key) => key.startsWith('_')),
            property_$and           = [];

        for (let i = 0; i < property_keys.length; i++) {
            const
                key      = property_keys[i],
                name     = key.replace('_', ''),
                value    = req.body[key];

            if (!value && value !== 0) continue;

            if (typeof value === 'string') {

                property_$and.push({
                  'properties': {
                    $elemMatch: {
                      name: name,
                      value: value
                    }
                  }
                });

            } else if (typeof value === 'object') {

                const innerProps = value;

                if (innerProps['od'] || innerProps['do']) {
                    // range input
                    const
                        _od = parseInt(innerProps['od']) || 1,
                        _do = parseInt(innerProps['do']) || 9999999;

                    // nie używamy Ceny - stosujemy wartości min i max z kodu spadkowego
                    if (name === 'Cena') {
                        min = Math.min(_od, _do);
                        max = Math.max(_od, _do);
                    } else {
                        property_$and.push({
                          'int_properties':
                            {
                              $elemMatch: {
                                name: name
                                  .replace(/</g, '[')
                                  .replace(/>/g, ']'),

                                value: { $gte: Math.min(_od, _do), $lte: Math.max(_od, _do) }
                              }
                            }
                        });
                    }

                } else {
                    // multiple input
                    const
                        propNames = Object.keys(innerProps);

                    if (propNames.indexOf('wszystkie') !== -1) {
                        continue;
                    } else {
                        property_$and.push({ 'properties': { $elemMatch: { name , value: { $in: propNames }}}});
                    }
                }
            }
        }

        let user = seller ? await findUserByNames((!seller || seller === '*' ? '.*' : seller)) : null;

        title    = !title || title === '*' ? '.*' : title;
        page     = parseInt(page) || 1;
        per_page = parseInt(per_page) || 10;


        switch(sort) {
            case 'promowane':
            case 'najnowsze':
                sort = { 'premium.forever': -1, 'premium.isPremium': -1, 'date.start_date': -1 };
                //sort = [[ 'date.start_date', -1 ]];
                break;
            case 'alfabetycznie':
                sort = { 'premium.forever': -1, 'premium.isPremium': -1, 'title': 1 };
                //sort = [[ 'title', 1 ]];
                break;
            case 'najtańsze':
                sort = { 'premium.forever': -1, 'premium.isPremium': -1, 'price.current_price': 1 };
                //sort = [[ 'price.start_price', 1 ]];
                break;
            case 'najdroższe':
                sort = { 'premium.forever': -1, 'premium.isPremium': -1, 'price.current_price': -1 };
                //sort = [[ 'price.start_price', -1 ]];
                break;
            case 'najpopularniejsze':
                sort = { 'premium.forever': -1, 'premium.isPremium': -1, 'likes': -1 };
                //sort = [[ 'likes', -1 ]];
                break;
            case 'kończące się':
                sort = { 'premium.forever': -1, 'premium.isPremium': -1, 'date.duration': 1, 'date.start_date': 1 };
                //sort = [[ 'date.duration', 1 ], [ 'date.start_date', 1 ]];
                break;
            default:
                sort = { 'premium.forever': -1, 'premium.isPremium': -1 };
                //sort = [];
        }

        // sort.unshift([ 'premium.isPremium', 1 ]);
        // sort.unshift([ 'premium.forever', 1 ]);

        const mongo_query = {
            title: {
                $regex: title, $options: 'i'
            },
            'price.current_price': { $lte: max, $gte: min },
            'violations.hanged': { $ne: true },
            ended: { $ne: true }

        };

        if (showPromotedOnly) {
          mongo_query['premium.isPremium'] = true;
        }

        if (seller) {
          mongo_query._user = user && user._id || 'brak-wyników';
        }

        // if (category) mongo_query['categories.main'] = category;
        // if (subcategory) mongo_query['categories.sub'] = subcategory;
        // if (subsubcategory) mongo_query['categories.subsub'] = subsubcategory;
        if (categories) {
          mongo_query['categories'] = { $all: categories };
        }

        // properties
        if (property_$and.length > 0) {
            mongo_query['$and'] = property_$and;
        }

        const projection = { _shortid: 1, _user: 1, title: 1, categories: 1, shortdescription: 1, price: 1, date: 1, created: 1, premium: 1 }; // photos:{ $slice: 1 }
        const options = { skip: (page-1) * per_page, limit: per_page };

        if (sort) {
            options.sort = sort;
        }
        console.log(sort);
        console.log('FILTER QUERY:')
        console.log(util.inspect(mongo_query, false, null, true));

        const count = await Auction.countDocuments(mongo_query);
        const auctions = await Auction.find(mongo_query, projection, options).lean();

        await checkIfLiked(auctions, req);
        auctions.push(Math.ceil(count / per_page));

        res.send(auctions);
    });

    app.get('/auction/get_all/:page/:per_page', async (req, res) => {
        const page = parseInt(req.params.page);
        const per_page = parseInt(req.params.per_page) || 10;

        const count = await Auction.countDocuments({});
        const auctions = await Auction.find(
            { _user: { $ne: currentUserId(req) } },
            { _shortid: 1, title: 1, shortdescription: 1, price: 1, photos:{ $slice: 1 }, created: 1, categories: 1 },
            { skip: (page-1) * per_page, limit: per_page}
        ).lean();

        await checkIfLiked(auctions, req);
        auctions.push(count);

        res.send(auctions);
    });

    app.get('/auction/search/:category/:query/:page/:per_page', async (req, res) => {
        const category = req.params.category === 'Kategorie' ? '.*' : req.params.category;
        const query = req.params.query === '*' ? '.*' : req.params.query;
        const page = parseInt(req.params.page);
        const per_page = parseInt(req.params.per_page);

        let mongo_query;
        if (category === 'Szukaj Sprzedawcy') {
            mongo_query = {
                ended: { $ne: true }
            };

            const user = await findUserByNames(query);
            if (user) {
                mongo_query._user = user._id;
            }
        } else {
            mongo_query = {
                _user: { $ne: currentUserId(req) },
                title: { $regex: query, $options: 'i' },
                $or: [{ 'categories.main': { $regex: category, $options: 'i' } }, { 'categories.sub': { $regex: category, $options: 'i'} }],
                ended: { $ne: true }
            };
        }

        const projection = { _shortid: 1, title: 1, shortdescription: 1, price: 1, date: 1, created: 1, categories: 1, photos:{ $slice: 1 } };
        const options = { skip: (page-1) * per_page, limit: per_page };

        const count = await Auction.countDocuments(mongo_query);
        const auctions = await Auction.find(mongo_query, projection, options).lean();

        await checkIfLiked(auctions, req);
        auctions.push(count);

        res.send(auctions);
    });

    app.get('/auction/advanced_search/:category/:query/:min/:max/:state/:sort/:page/:per_page', async (req, res) => {
        const page = parseInt(req.params.page);
        const per_page = parseInt(req.params.per_page);

        const category = req.params.category === 'Kategorie' ? '.*' : req.params.category;
        const query = req.params.query === '*' ? '.*' : req.params.query;
        const min = Number(req.params.min) || 1;
        const max = Number(req.params.max) || 999999;

        let state = null;
        switch(req.params.state) {
            case 'nowe':
                state = 'nowy';
                break;
            case 'uzywane':
                state = 'używany';
                break;
            default:
                state = null;
        }

        let sort = null;
        switch(req.params.sort) {
            case 'alfabetycznie':
                sort = [ 'title', 1];
                break;
            case 'tanie':
                sort = [ 'price.start_price', 1 ];
                break;
            case 'drogie':
                sort = [ 'price.start_price', -1];
                break
            default:
                sort = null;
        }

        const mongo_query = {
            _user: { $ne: currentUserId(req) },
            title: {
                $regex: query, $options: 'i'
            },
            'price.start_price': { $lte: max, $gte: min },   // change to current_price
            $or: [
                { 'categories.main': { $regex: category, $options: 'i' } },
                { 'categories.sub': { $regex: category, $options: 'i'} }
            ],
            ended: { $ne: true }
        };

        if (state) {
            mongo_query['attributes'] = { $elemMatch: { name: 'Stan', value: state } };
        }

        const projection = { _shortid: 1, title: 1, shortdescription: 1, price: 1, date: 1, photos:{ $slice: 1 }, created: 1, categories: 1 };
        const options = { skip: (page-1) * per_page, limit: per_page };

        if (sort) {
            options.sort = { [sort[0]]: sort[1] };
        }

        const count = await Auction.countDocuments(mongo_query);
        const auctions = await Auction.find(mongo_query, projection,options).lean();

        await checkIfLiked(auctions, req);
        auctions.push(count);

        res.send(auctions);
    });

    app.get('/auction/count/:category', async (req, res) => {
        const category = req.params.category;

        const mains = await Category.find({}).lean();
        const main_names = mains.map(c => c.name);

        const is_main = category === 'Kategorie' || main_names.indexOf(category) !== -1;
        const is_user = category === 'Szukaj Sprzedawcy';

        if (is_main || is_user) {
            let result = [];
            let items = 0;

            if (is_user) {
                return([]);
                await main_names.map(async c => {
                    const user = await findUserByNames(category);
                    let query = { 'categories.main': c };
                    Auction
                        .count(query)
                        .then(count => {
                            result.push({ name: c, count });
                            items++;
                            if (items === main_names.length) {
                                res.send(result);
                            }
                        })
                });
            } else {
                await main_names.map(c => {
                    Auction
                        .count({ 'categories.main': c })
                        .then(count => {
                            result.push({ name: c, count });
                            items++;
                            if (items === main_names.length) {
                                res.send(result);
                                return;
                            }
                        },
                        (err) => console.log('error'));
                });
            }


        } else {
            const parent = mains.filter(c => c.subcategories.filter(sub => sub.name.indexOf(category) !== -1).length)[0];
            const subs = parent.subcategories.map(c => c.name);

            let result = [];
            let items = 0;

            await subs.map(c => {
                Auction
                    .countDocuments({ 'categories.sub': c })
                    .then(count => {
                        result.push({ name: c, count }) ;
                        items++;
                        if (items === subs.length) {
                            res.send(result);
                        }
                    },
                    (err) => console.log('error'));
            });
        }
    });

    app.get('/auction/get_all', async (req, res) => {
        const auctions = await Auction.find(
            { _user: { $ne: currentUserId(req) }, ended: { $ne: true } },
            { _shortid: 1, title: 1, shortdescription: 1, price: 1, photos: { $slice: 1 }, created: 1, categories: 1 }
        ).limit(10);
        res.send(auctions);
    });

    app.post('/auction/create_or_update', [requireLogin, upload.any()], async (req, res) => {
        const data = req.body;
        const update = data.auction_id ? true : false;
        const postAgain = data.post_again ? true : false;

        if (postAgain) {
          const user = await User.findById(req.user._id);

          if (user.packets.auctions) {
            user.packets.auctions = user.packets.auctions - 1;
            user.save();

          } else {
            if (await globalDiscountActive(mongoose)) {
              // OK
            } else {
              req.session.message = 'Brak wystarczającej ilości aukcji w pakiecie';
              res.send({ redirect: '/wykup-pakiety' });
              return;
            }
          }
        }

        const
            keys = Object.keys(data),
            properties = [],
            int_properties = [],
            deliveries = [],
            categories = parseCategories(data);

        keys
          .filter(key => key.startsWith('property_'))
          .filter(key => Boolean(data[key]))
          .map(key => {
            const
                name  = key.
                  replace('property_', '')
                  .replace(/</g, '[')
                  .replace(/>/g, ']'),

                value = data[key];

            // oznakowane wartości liczbowe
            if (name.startsWith('%')) {
                int_properties.push({ name: name.slice(1), value: parseInt(value) });
            } else {
                properties.push({ name, value: value.split(', ') });
            }
        });

        const
          property_paths = keys
            .filter(key => key.startsWith('path_for_property'))
            .map(key => data[key] )
            .join(',');

        keys.filter(key => key.startsWith('delivery_')).map(key => {
            const
                arr = key.split('_'),
                name = arr[1],
                price = parseInt(arr[2]);

            if (name && price >= 0) {
                deliveries.push({ name, price });
            }
        });

        let auction = new Auction({
            _user: ObjectId(req.user._id),
            title: data.title,
            shortdescription: data.shortdescription,
            description: data.description,
            price: {
                start_price: data.start_price || 1,
                min_price: data.min_price,
                buy_now_price: data.buy_now_price,
                current_price: data.start_price || 1,
                hide_min_price: data.hide_min_price === 'on'
            },
            date: {
                start_date: Date.now(),
                duration: data.duration
            },
            likes: 0,
            quantity: data.quantity,
            photos: [],
            //attributes,
            categories: categories,
            // categories: {
            //     main: data.category,
            //     sub: data.subcategory,
            //     subsub: data.subsubcategory
            // },
            bids: [],
            ended: false,
            verified: false,
            properties,
            int_properties,
            deliveries,

            property_paths,
            created: Date.now()

        });

        console.log(auction);

        if (update) {
            auction._id                 = ObjectId(data.auction_id);
            const oldAuction            = await Auction.findOne({ _id: ObjectId(data.auction_id) });

            auction.price.start_price   = data.start_price || oldAuction.price.start_price || 1;
            auction.price.current_price = data.start_price || oldAuction.price.current_price;

            if (postAgain) {
              auction.date.start_date = Date.now();
              markProperties(property_paths);
              markCategories(auction.categories);

            } else {
              auction.date.start_date = data.start_date || Date.now();
              auction.premium         = oldAuction.premium;
              auction.bids            = oldAuction.bids;
              auction.payees          = oldAuction.payees;
              auction.buynowpayees    = oldAuction.buynowpayees;
              auction.raters          = oldAuction.raters;
              auction.created         = oldAuction.created;
              auction.violations      = oldAuction.violations;

              if (oldAuction.categories.join('') !== auction.categories.join('')) {
                unmarkCategories(oldAuction.categories);
                markCategories(auction.categories);
              }

              if (oldAuction.property_paths !== property_paths) {
                unmarkProperties(oldAuction.property_paths);
                markProperties(property_paths);
              }
            }

            delete auction._id;

            Auction.findOneAndUpdate({ _id: ObjectId(data.auction_id) }, auction, function(err, doc) {
                if (err) {
                    console.log('error', err);
                    req.session.error = 'Edycja aukcji nie powiodła się';
                    res.send(false);
                    return;
                }

                if (doc) {
                    req.session.message = postAgain ?  'Aukcja wystawiona ponownie' : 'Pomyślnie dokonano edycji aukcji';
                    res.send(doc);
                }
            });
        } else {
            await auction
                .save()
                .then(
                    async savedAuction => {
                        req.session.message = 'Pomyślnie dodano aukcję';

                        const user = await User.findById(req.user._id);
                        if (user.packets.auctions)
                          user.packets.auctions = user.packets.auctions - 1;

                        user.save();
                        markCategories(savedAuction.categories);
                        markProperties(savedAuction.property_paths);

                        res.send(savedAuction);
                    },
                    (err) => { console.log(err); req.session.error = 'Utworzenie aukcji nie powiodło się'; res.send(false); return;}
                );
        }
    });

    app.post('/auction/post_photos', [requireLogin, upload.any()], async (req, res) => {
        const _id   = req.body._id || null,
              files = req.files;

        const auction = (
            _id
            ?
            await Auction.findOne({ _id: ObjectId(_id) })
            :
            await Auction.findOne({ _user: req.user._id }, null, { sort: { 'date.start_date' : -1 } })
        );

        if (auction) {
            savePhotos(auction, files);
            res.send(true);
        } else {
            res.send(false);
        }
    });
};

async function savePhotos(auction, files) {
    const promises = await files.map((file, index) => ({
        order: index,
        type: file.mimetype,
        data: Sharp(file.buffer).resize(1024).toBuffer() // withMetadata()
    }));

    let progress = 0;

    promises.map(promise => {
        const
            type  = promise.type,
            order = promise.order;

        promise.data.then(buffer => {
            Imagemin.buffer(buffer, {
                plugins: [
                    imageminMozjpeg({ quality: 80 }),
                    imageminPngquant({ quality: 80 }),
                    imageminSvgo(),
                    imageminGifsicle()
                ]
            }).then(optimisedBuffer => {
                progress++;

                auction.photos[order] = { type: type, data: optimisedBuffer.toString('base64') };

                if (progress === files.length) {
                    auction.save();
                }
            });
        });
    });
}

async function getBidders(auction) {
  const ids = auction.bids.map(bid => ObjectId(bid._user));
  const bidders = await User
    .find({ _id: { $in: ids }})
    .select('firstname lastname')
    .lean();

  let aggregated = {};
  bidders
    .map(bidder => ( aggregated[ bidder._id ] = bidder ));

  return aggregated;
}

async function checkIfLiked(user, auction) {
  let liked = false;
  if (user && auction)
    liked = await Like.findOne({ _user: user._id, _auction: auction._id });

  return liked;
}

async function findUserByNames(query) {
    let names = query.split(' ');
    let name_array = [names[0] ? new RegExp(names[0], 'i') : null, names[1] ? new RegExp(names[1], 'i') : null];
    return await User.findOne({ $or: [ {firstname: { $in: name_array }}, {lastname: { $in: name_array }} ] }, { _id: 1 });
}

async function checkIfLiked(auctions, req) {
    if (!req.user) {
        return;
    }

    let
        ids         = await Like
            .find({ _user: ObjectId(req.user._id) })
            .lean();
        ids         = ids.map(like => String(like._auction));

    if (ids && ids.length) {
        return auctions.map(auction => {
            if (ids.indexOf(String(auction._id)) !== -1)
                auction.liked = true;
            return auction;
        });
    }
}

function sortByViews(a, b) {
    if (a.views > b.views) return 1;
    if (a.views < b.views) return -1;
    return 0;
}

async function rotatePremium() {
    let
        premiums = await Auction.find(
            { ended: { $ne: true }, 'premium.isPremium': true },
            { title: 1, shortdescription: 1, price: 1, date: 1, created: 1, categories: 1, _shortid: 1 }, //photos: { $slice: 1 },
        )
        .sort({ 'date.start_date': 1 })
        .limit(100)
        .lean();

    for (let i = 0; i < premiums.length; i++) {
        const
            auction = premiums[i],
            views = await Views.findOne({ _auction: auction._id });

        auction.views = views ? views.views : 0;
    }

    return premiums.sort(sortByViews).slice(0, 9);
}

async function addBidders(auction) {
    let auction_object = auction.toObject(),
        bidders_object = {};
    const bidder_ids = auction.bids.map(bid => ObjectId(bid._user));
    const bidders = await User.find({ _id: { $in: bidder_ids }}, { firstname: 1, lastname: 1 }).lean();
    bidders.map(bidder => (bidders_object[bidder._id] = bidder ));
    auction_object.bidders = bidders_object;
    return auction_object;
}

async function buyNowNotifications(user, owner, auction) {
    let subject, recipients, mailer;
    let res;

    let error = (e) => { throw ('Error in auctionRoutes.buyNowNoticications - ' + e) };

    if (!user) error('no buyer');
    if (!user._id) error('buyer has no _id');
    if (!owner) error('no owner');
    if (!owner._id) error('owner has no _id');
    if (!auction) error('no auction');

    const price = auction.price.buy_now_price;
    const username = `${user.firstname || ''} ${user.lastname || (!user.firstname ? 'Anonim' : '')}`;

    if (!price) error('no price');

    subject = 'Kupiłeś przedmiot ' + auction.title;
    recipients = [{ email: user.contact.email }];
    mailer = new Mailer({ subject, recipients }, buyNowWonTemplate(auction, price));
    res = await mailer.send();

    subject = `${username} kupi od Ciebie ${auction.title} w opcji Kup Teraz`;
    recipients = [{ email: owner.contact.email }];
    mailer = new Mailer({ subject, recipients }, buyNowItemSoldTemplate(auction, username, price));
    res = await mailer.send();

    helpers.sendChatMessagesOnAuctionEnd(user._id, owner._id, auction, price);
}

function parseCategories(data) {
  const PICKER_TEXT = 'Wybierz...';

  let categories = [];

  if (data.category && data.category !== PICKER_TEXT) {
    categories.push(data.category);
    Object
      .keys(data)
      .filter(key => key.startsWith('subcategory_'))
      .forEach(key => {
        let catname = data[ key ];
        if (catname !== PICKER_TEXT)
          categories.push(catname);
      });
  }

  return categories;
}

module.exports.savePhotos = savePhotos;
