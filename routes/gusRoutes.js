const
  express = require('express'),
  gusRouter = express.Router(),
  requireLogin = require('../middleware/requireLogin'),

  mongoose = require('mongoose'),
  User = mongoose.model('user'),

  { getFullReport } = require('../services/workers/GusTask'),
  { composeThread } = require('../services/workers/Worker'),

  keys = require('../config/keys');

gusRouter.post('/report', requireLogin, async (req, res) => {
  let
    input = {
      key: keys.gus,
      nip: req.body.nip
    },

    exists = await User
      .countDocuments({ _id: { $ne: req.user._id }, 'firm.nip': input.nip })
      .limit(1);

  console.log(input);

  composeThread(
    getFullReport,
    input,
    result => {
      result.exists = Boolean(exists);
      res.send(result);
    },
    progress => console.log(progress)
  );
});

module.exports = gusRouter;
