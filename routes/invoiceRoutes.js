const
		multer			= require('multer'),
		upload			= multer(),
		mongoose		= require('mongoose'),
		{ ObjectId }= mongoose.Types,
		User				= mongoose.model('user'),
		//Transaction	= mongoose.model('transaction'),
		Invoice 		= mongoose.model('invoice'),
		requireLogin= require('../middleware/requireLogin'),
		business		= require('../services/emailTemplates/business'),
		Mailer			= require('../services/Mailer'),
		invoiceTemplate	= require('../services/emailTemplates/invoiceTemplate'),

		{ invoicePdfName } = require('../functions/invoice');

module.exports = app => {
	// app.post('/invoices/send', requireLogin, async (req, res) => {
	// 	const
	// 		{ id, vat }		= req.body,
	// 		transaction 	= await Transaction.findOne({ _id: ObjectId(id) }),
	// 		seller 				= req.user,
	// 		buyer 				= await User.findOne({ _id: ObjectId(transaction._buyer) }),
	// 		subject				= `Faktura z ${business.name} za ${transaction.title}`,
	// 		recipients		= [{ email: buyer.contact.invoice_email || buyer.contact.email }, { email: seller.contact.invoice_email || seller.contact.email }],
	// 		mailer 				= new Mailer({ subject, recipients }, invoiceTemplate(seller, buyer, transaction, vat));
	//
	// 		await mailer.send();
	// 		req.session.message = 'Wysłano fakturę. Kopia została przesłana również do Ciebie';
	//
	// 		transaction.sent = true;
	// 		transaction.vat = Boolean(vat);
	// 		await transaction.save();
	//
	// 		res.send(true);
	// });

	app.get('/faktura/:id', requireLogin, async (req, res) => {
		let
			_id = ObjectId(req.params.id),
			_user = req.user._id,

			invoice = await Invoice
				.findOne({ _id, _user })
				.select('number pdf');

		if (invoice && invoice.pdf) {
			res.writeHead(200, {
				'Content-Type': 'application/pdf; charset=windows-1250',
				'Content-Disposition': `attachment; filename="${ invoicePdfName(invoice) }"`
			});
			res.end(
				Buffer.from(invoice.pdf, 'base64')
			);
		} else {
			req.session.error = 'Błąd. Faktura nie istnieje.';
			res.redirect('/');
		}
	});

	app.post('/invoices/fetch_invoices', requireLogin, async (req, res) => {
		const
			{ page, per_page } 	= req.body,

			query = { _user: req.user._id },
			projection = {},
			options = {
				sort: { created: -1 },
				limit: +per_page,
				skip: ((+page - 1) * +per_page)
			},
			invoices 	= await Invoice
				.find(query, projection, options)
				.lean();

		// for (let i = 0; i < transactions.length; i++) {
		// 	const
		// 		transaction 	= transactions[i],
		// 		buyer 			= await User.findOne({ _id: ObjectId(transaction._buyer) });
		//
		// 	transaction.buyer 	= buyer;
		// }

		const count = await Invoice.countDocuments(query);
		invoices.push(Math.ceil(count / +per_page));

		res.send(invoices);
	});
}
