const
  mongoose = require('mongoose'),
  { Schema } = mongoose,

  settingsSchema = new Schema({
    discountFinalDate: date(),
    auctionPackets: [ auctionPacket() ],
    promoPackets: [ promoPacket() ],
    packetValidityTime: number(30)
  });

mongoose.model('settings', settingsSchema);


function number(defaultValue) {
  return { type: Number, default: defaultValue || 0 };
}

function date(defaultValue) {
  return { type: Date, default: defaultValue || Date.now };
}

function boolean(defaultValue) {
  return { type: Boolean, default: defaultValue || false };
}

function auctionPacket() {
  return {
    cost: number(),
    count: number(),
    unlimited: boolean(),
  };
}

function promoPacket() {
  return {
    name: String,
    cost: number(),
    count: number()
  };
}
