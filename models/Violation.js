const
  mongoose = require('mongoose'),
  { Schema } = mongoose,
  { ObjectId } = Schema.Types,
  { VIOLATIONS } = require('../client/src/constants/violations'),

  violationSchema = new Schema({
    date: { type: Number, default: Date.now },

    _user: ObjectId,
    _auction: ObjectId,

    reason: String,

    group: { type: String, enum: VIOLATIONS },

    resolved: { type: Boolean, default: false }
  });

mongoose.model('violation', violationSchema);
