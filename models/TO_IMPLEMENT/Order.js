const mongoose = require('mongoose'),
      { Schema } = mongoose,
      { ObjectId } = Schema.Types,

      addressSchema = require('./Address'),

      orderStatuses = {
        ORDER_NEW: 'new',
        ORDER_PAID: 'paid',
        ORDER_REJECTED: 'rejected',
        ORDER_FAILED: 'failed',
        ORDER_CANCELED: 'canceled'
      };

const orderSchema = new Schema({
  _user: { type: ObjectId, ref: 'user' },
  _invoice: { type: ObjectId, ref: 'invoice' },
  ip: String,
  total: Number,
  title: String,
  status: {
    type: String,
    enum: Object.keys(orderStatuses),
    default: orderStatuses.ORDER_NEW
  },
  buyer: {
    integerId: Number,
    fullname: String,
    firm_name: String,
    address: addressSchema,
    NIP: String
  },
  created: { type: Date, default: Date.now },
  updated: { type: Date, default: Date.now }
});

orderSchema.methods._updateStatus = function(status) {
  this.status = status;
  this.updated = new Date();
  return this.save();
}

orderSchema.methods.setPaid = function() {
  return this._updateStatus(orderStatuses.ORDER_PAID);
}

orderSchema.methods.setCanceled = function() {
  return this._updateStatus(orderStatuses.ORDER_CANCELED);
}

mongoose.model('order', orderSchema);
