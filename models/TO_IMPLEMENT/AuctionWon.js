const mongoose = require('mongoose'),
      { Schema } = mongoose,
      { ObjectId } = Schema.Types,

      user = { type: ObjectId, ref: 'user' },
      auct = { type: ObjectId, ref: 'auction' },
      bool = { type: Boolean, default: false },
      date = { type: Date, default: Date.now },

      auctionTypes = {
        BUY_NOW: 'buynow',
        BID: 'bid'
      };

const auctionWonSchema = new Schema({
  _buyer: user,
  _seller: user,
  _auction: auction,
  
  title: String,
  total: Number,
  qty: Number,
  type: { type: String, enum: Object.keys(auctionTypes) },

  paid: bool,
  sent: bool,
  sellerReviewed: bool,
  buyerReviewed: bool,

  resolved: bool,

  created: date
});

mongoose.model('auctionWon', auctionWonSchema);
