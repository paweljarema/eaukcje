const mongoose = require('mongoose'),
      { Schema } = mongoose,
      { ObjectId } = Schema.Types;

const bidSchema = new Schema({
  _buyer: { type: ObjectId, ref: 'user' },
  _auction: { type: ObjectId, ref: 'auction' },
  total: Number,

  created: { type: Date, default: Date.now }
});

mongoose.model('bid', bidSchema);
