const mongoose = require('mongoose');
const { Schema } = mongoose;

const propertySchema = new Schema({
	name: String,
	type: String,
	unit: String,
	icon: String,
	values: [String],
	conditional_values: Schema.Types.Mixed,

	activeAuctions: { type: Number, min: 0, default: 0 }
});

// const sub_subcategorySchema = new Schema({
//     name: String,
//     icon: String,
//     properties: [propertySchema]
// });
//
// const subcategorySchema = new Schema({
//     name: String,
//     icon: String,
//     properties: [propertySchema],
//     sub_subcategories: [sub_subcategorySchema],
// });

const categorySchema = new Schema({
    name: String,
    icon: String,

		pos: { type: Number, min: 0 },
		level: { type: Number, min: 0 },
		activeAuctions: { type: Number, min: 0, default: 0 },

		ogDescription: { type: String, default: '' },
		metaTitle: { type: String, default: '' },
		metaDescription: { type: String, default: '' },
		description: { type: String, default: '' },

		properties: [propertySchema],
    subcategories: [this]
});

mongoose.model('category', categorySchema);
mongoose.model('property', propertySchema);
// mongoose.model('subcategory', subcategorySchema);
// mongoose.model('sub_subcategory', sub_subcategorySchema);
