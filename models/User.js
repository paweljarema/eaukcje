const mongoose = require('mongoose');
const { Schema } = mongoose;
const { ObjectId } = Schema.Types;

const addressSchema = require('./Address');
const balanceSchema = require('./Balance');

require('./IntegerIdTracker');
const IntegerIdTracker = mongoose.model('integerIdTracker');

const sendRateSchema = new Schema({
    _auction: ObjectId,
    _user: ObjectId,
    buynow: Boolean,
    count: Number
});

const userSchema = new Schema({
    _integerId: Number,

    firstname: String,
    lastname: String,
    birthdate: Number,
    pesel: Number,
    joindate: Number,
    address: addressSchema,
    auth: {
        linkedinId: String,
        facebookId: String,
        googleId: String,
        twitterId: String
    },
    contact: {
        email: String,
        invoice_email: String,
        phone: String
    },
    balance: {
        balance: balanceSchema,
        credits: { type: Number, default: 5 },
        account_number: String
    },
    deliveries: [{ name: String, price: Number }],
    agreements: {
        rodo_1: Boolean,
        rodo_2: Boolean,
        corespondence: Boolean
    },
    security: {
        password: String,
        verified: Boolean,
        id: {
            firstname: String,
            lastname: String,
            id_number: String,
            pesel: String,
            agreement: Boolean
        }
    },
    firm: {
        firm_name: String,
        nip: String
    },
    packets: {
        auctions: { type: Number, default: 0 },
        auctions_unlimited: Boolean,
        unlimited_date: Number,
        promos: { type: Number, default: 0 }
    },
    toSend: [sendRateSchema],
    toRate: [sendRateSchema]
});

userSchema
  .virtual('fullname')
  .get(function () { return `${ this.firstname || '' } ${ this.lastname || '' }`.trim() || 'Anonim'; });

userSchema
  .pre('save', function (next) {
    if (!this._integerId) {
      let doc = this;

      IntegerIdTracker.findOneAndUpdate(
        { model: 'user' },
        { $inc: { count: 1 }},
        { upsert: true, new: true },
        function (error, tracker) {
          if (error) return next(error);

          doc._integerId = tracker.count;
          next();
        }
      );
    } else next();
  });

mongoose.model('user', userSchema);
