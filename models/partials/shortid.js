const shortid = require('shortid');

module.exports = { type: String, default: shortid.generate };
