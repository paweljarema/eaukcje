const mongoose = require('mongoose');
const { Schema } = mongoose;
const { ObjectId } = mongoose.Types;

const seoSchema = new Schema({
	title: String,
  description: String
});

mongoose.model('seo', seoSchema);
