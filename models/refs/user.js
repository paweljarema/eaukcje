const
  mongoose = require('mongoose'),
  { ObjectId } = mongoose.Schema.Types;
  
module.export = { type: ObjectId, ref: 'user' };
