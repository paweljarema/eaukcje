const
  mongoose = require('mongoose'),
  { Schema } = mongoose,

  userRef = require('./refs/user'),

  addressSchema = require('./Address'),
  invoiceSchema = new Schema({
    _user: userRef,
    lp: { type: Number, default: 1 },
    number: String,

    html: String, // just html
    pdf: String, // base64 buffer

    vat: { type: Number, default: 0 },
    total: { type: Number, default: 0 },

    description: String,

    firstname: String,
    lastname: String,
    firm: {
      firm_name: String,
      nip: String
    },
    address: addressSchema,


    sent: { type: Boolean, default: false },
    created: { type: Date, default: Date.now }
  });

mongoose.model('invoice', invoiceSchema);
