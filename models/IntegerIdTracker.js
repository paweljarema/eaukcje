const mongoose = require('mongoose'),
      { Schema } = mongoose;

const integerIdTrackerSchema = new Schema({
  model: { type: String, required: true },
  count: { type: Number, default: 0 }
});

mongoose.model('integerIdTracker', integerIdTrackerSchema);
