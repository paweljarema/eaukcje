const
  html = require('./html'),
  { nicePrice } = require('../functions/price');

// business { fullname, firm_name, NIP, address, accountNumber, dueDate}
// buyer
// product [{ name, qty, unit, price, vat }]
// 19/07/31/LP
module.exports = function({ seller = {}, buyer = {}, products = [], invoiceNumber }) {
  let
    date = formatDate(new Date()),
    { total, totalNet, totalVat, productTable } = parseProducts(products);

  return html(
    `
      html {                          /* this is a hack, remove it when it's no longer needed */
        zoom: 0.55 !important;
      }

      @page
      {
          size:  auto;
          margin: 0;
      }

      @media print {
        html, body, .html-pdf {
          width: auto;
          height: auto;
        }
      }

      .html-pdf a {
        color: black;
        text-decoration: none;
      }

      .html-pdf {
        display: block;
        font-family: sans-serif;
        line-height: 1.3;
      }

      .html-pdf table {

      }

      .html-pdf .empty,
      .html-pdf .noborder {
        border: none;
      }

      .html-pdf .products th,
      .html-pdf .products td {
        padding: 5px;
      }

      .html-pdf .title {
        width: 100%;
        text-align: left;
        background: white;
        border-bottom: 1px solid #999;
      }

      .html-pdf .title h1,
      .html-pdf .title h2,
      .html-pdf .title h3 {
        color: black;
      }

      .html-pdf .title h2 {
        margin-bottom: 0;
      }

      .html-pdf .heading h5 {
        margin: 0;
      }

      .html-pdf .invoice-info {
        width: 100%;
      }

      .html-pdf .invoice-info th {
        text-align: left;
      }
      .html-pdf .invoice-info th,
      .html-pdf .invoice-info td {
        line-height: 1;
        padding: 2px 5px 2px 0 !important;
      }

      .html-pdf .invoice-info {
        white-space: pre-line;
      }

      .html-pdf .invoice-info tbody td {
        line-height: 1.35;
      }

      .html-pdf .invoice-info .seller,
      .html-pdf .invoice-info .buyer {

      }

      .products {
        border-collapse: collapse;
      }

      .products th, .products td {
        border: 1px solid #999;
      }

      .html-pdf .products {
        width: 100%;
        font-size: 13px;
        text-align: center;
        border-spacing: 0px;
        margin: 6px 30px auto 0;
      }

      .html-pdf .products th {
        padding: 10px 5px;
      }

      .html-pdf .products th {
        background: #eee;
      }

      .html-pdf table .name {
        text-align: left;
      }

      .html-pdf table .price {
        text-align: right;
      }

      .html-pdf .final-info {
        margin-top: auto;
      }

      .html-pdf h5 {
        font-size: 16px;
      }

      .html-pdf b {
        font-size: 15px;
      }
    `,
    `
      <table class="title">
        <tbody>
          <tr>
            <td><h2>Faktura VAT</h2></td>
          </tr>
          <tr>
            <td><h3>Nr. ${ invoiceNumber }</h3></td>
          </tr>
        </tbody>
      </table>

      <table class="invoice-info">
        <thead>
          <tr>
            <td><b>Sprzedawca:</b></td>
            <td align="right"><b>Nabywca:</b></td>
          </tr>
        </thead>

        <tbody>
          <tr>
            <td valign="top">${ aggregate(
                seller.fullname,
                seller.firm_name.toUpperCase(),
                seller.address,
                seller.NIP ? 'NIP ' + seller.NIP : seller.pesel ? 'Pesel ' + seller.pesel : ''
              )}
            </td>

            <td align="right" valign="top">${ aggregate(
                buyer.fullname,
                buyer.firm_name,
                buyer.address,
                buyer.NIP ? 'NIP ' + buyer.NIP : buyer.pesel ? 'Pesel ' + buyer.pesel : ''
              )}
            </td>
          </tr>
        </tbody>
      </table>
      <br/>
      <br/>
      <br/>

      <table class="info">
        <tbody>
          <tr>
            <td><b>Data wystawienia:</b> ${ date }</td>
          </tr>
          <tr>
            <td><b>Data sprzedaży:</b> ${ date }</td>
          </tr>
          <tr>
            <td><b>Termin płatności:</b> Zapłacono</td>
          </tr>
          <tr>
            <td><b>Forma płatności:</b> Przelew</td>
          </tr>
        </tbody>
      </table>
      <br/>
      <br/>
      <br/>

      <table class="heading">
        <tbody>
          <td><h5>Podsumowanie zamówienia</h5></td>
        </tbody>
      </table>

      <table class="products">
        <thead>
          <tr>
            <th>Lp.</th>
            <th class="name">Nazwa usługi</th>
            <th>Ilość</th>
            <th>Jedn.</th>
            <th>Cena netto</th>
            <th>VAT</th>
            <th>Wartość netto</th>
            <th class="price">Wartość VAT</th>
            <th class="price">Wartość brutto</th>
          </tr>
        </thead>
        <tbody>
          ${ productTable }
          <tr>
            <td class="empty noborder" colspan="4"></td>
            <td class="total price" colspan="2" align="right"><b>Razem:</b></td>
            <td class="price" align="right">${ nicePrice( totalNet ) }</td>
            <td class="price" align="right">${ nicePrice( totalVat ) }</td>
            <td class="price" align="right">${ nicePrice( total ) }</td>
          </tr>
        </tbody>
      </table>
      <br/>
      <br />

      <div class="final-info">
        <div>
          <div><b>Zapłacono:</b> ${ nicePrice( total ) } (${ numberToWords( total ) })</div>
        <div>
      </div>
    `
  );
}

function leadingZero(num) {
  if (num < 10) return '0' + num;
  return num;
}

function formatDate(date) {
  return `${ leadingZero( date.getDate() ) }-${ leadingZero( date.getMonth() + 1 ) }-${ date.getFullYear() }`;
}

function calculateVat({ price, vat }) {
  if (vat)
    return price * ( vat / 100 );
  else
    return 0;
}

function parseProducts(products = []) {
  let
    total = 0,
    totalVat = 0,
    totalNet = 0,
    productTable = products.map((product, i) => {
      let
        { name, qty, unit, price, vat } = product,
        calcVat = calculateVat({ price, vat });

      total += price + calcVat;
      totalNet += price;
      totalVat += calcVat;

      return `
        <tr>
          <td>${ i + 1 }</td>
          <td class="name">${ name }</td>
          <td>${ qty }</td>
          <td>${ unit || 'szt.' }</td>
          <td>${ nicePrice( price ) }</td>
          <td>${ vat }%</td>
          <td class="price" align="right">${ nicePrice( price ) }</td>
          <td class="price" align="right">${ nicePrice( calcVat ) }</td>
          <td class="price" align="right">${ nicePrice( price + calcVat ) }</td>
        </tr>
      `;
    }).join('');

  return { total, totalNet, totalVat, productTable };
}

function numberToWords(num) {
  if (isNaN(num) || num === 0) return 'zero złotych';

  let
    number = Number(num),

    units = ['', ' jeden', ' dwa', ' trzy', ' cztery', ' pięć', ' sześć', ' siedem', ' osiem', ' dziewięć'],
    teens = ['', ' jedenaście', ' dwanaście', ' trzynaście', ' czternaście', ' piętnaście', ' szesnaście', ' siedemnaście', ' osiemnaście', ' dziewiętnaście'],
    decimals = ['', ' dziesięć', ' dwadzieścia', ' trzydzieści', ' czterdzieści', ' pięćdziesiąt', ' sześćdziesiąt', ' siedemdziesiąt', ' osiemdziesiąt', ' dziewięćdziesiąt'],
    hundreds = ['', ' sto', ' dwieście', ' trzysta', ' czterysta', ' pięćset', ' sześćset', ' siedemset', ' osiemset', ' dziewięćset'],
    plurals = [
      ['', '', ''],
      [' tysiąc', ' tysiące', ' tysięcy'],
      [' milion', ' miliony', ' milionów'],
      [' miliard', ' miliardy', ' miliardów'],
      [' bilion', ' biliony', ' bilionów']
    ],
    currencies = [' złoty', ' złote', ' złotych'];

  let
    plural = 0,
    result = '',
    currency;

  while (number > 0) {
    let
      hundred = Math.floor((number % 1000) / 100),
      teen = 0,
      decimal = Math.floor((number % 100) / 10),
      unit = Math.floor(number % 10);

    if (decimal === 1 && unit > 0) {
      teen = unit;
      decimal = 0;
      unit = 0;
    }

    let form = 2;
    if (unit === 1 && hundred + decimal + teen === 0)
      form = 0;
    if (unit === 2 || unit === 3 || unit === 4)
      form = 1;
    if (hundred + decimal + teen + unit > 0)
      result = hundreds[hundred] + decimals[decimal] + teens[teen] + units[unit] + plurals[plural][form] + result;

    if (!currency) {
      currency = currencies[form];
      result += currency;
    }

    plural++;
    number = Math.floor(number / 1000);
  }

  return result.trim() + gr(num);
}

function gr(number) {
  let remainder = number.toFixed(2).split('.')[1] || 0;
  return `, ${ ( remainder == 0 ? '0' : Number(remainder) ) }/100`;
}

function aggregate() {
  let strings = [...arguments];

  if (strings.length) {
    let result = strings
        .map(str => String(str))
        .filter(str => Boolean(str))
        .join('\n')
        .trim();

    return result;

  } else return '';
}
