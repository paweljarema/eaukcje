module.exports = (style = '', body = '', smallscale = false) => {
  const today = new Date();

  return `
    <!doctype html>
    <html>
      <head>
        <meta charset="utf-8">
        <style>
          ${
            smallscale ? (
              `html {                          /* this is a hack, remove it when it's no longer needed */
                zoom: 0.55 !important;
              }`
            ) : (
              `@page
              {
                  size:  A4;
                  margin: 2cm;
              }

              @media print {
                html, body, .html-pdf {
                  width: 210mm;
                  height: 297mm;
                }
              }`
            )
          }

          .html-pdf,
          .html-pdf .signatures {
            display: flex;
          }

          .html-pdf {
            flex-direction: column;

            max-width: 800px;

            ${ smallscale ? '' : 'padding: 0 0 30px 0' }
            margin: 0px auto;
          }

          .html-pdf > div {

          }

          .html-pdf h1,
          .html-pdf h2,
          .html-pdf h3 {
            font-family: sans-serif;
          }

          .html-pdf .title,
          .html-pdf h1.title,
          .html-pdf h2.title,
          .html-pdf h3.title {
            text-transform: uppercase;
            text-align: center;

            padding: 0;
            margin-bottom: 10px;

            color: white;
            background: #00b5ad;
          }

          .html-pdf .inline > div,
          .html-pdf .inline > figure {
            display: inline-block;
          }

          .html-pdf .inline > div {
            max-width: 435px;
          }

          .html-pdf figure img {
            width: 100%;
          }

          .html-pdf figure {
            max-width: 220px;
            vertical-align: top;
          }

          .html-pdf .input {
            display: inline-block;
            word-break: break-word;
          }

          .html-pdf .signatures {
            justify-content: space-between;
            margin-top: auto;
            padding: 0 16%;
          }

          .html-pdf .signatures > p {
            position: relative;
          }

          .html-pdf figure figcaption,
          .html-pdf .signatures .hint {
            text-align: center;
          }

          .html-pdf figure figcaption {
            font-size: 13px;
          }

          .html-pdf .signatures .hint {
            display: block;
            position: absolute;
            font-size: 11px;
            left: 50%;
            width: 100%;
            transform: translateX(-50%);
          }
          ${ style }
        </style>
      </head>
      <body>
        <div class="html-pdf">
          ${ body }
        </div>
      </body>
    </html>
  `;
};

module.exports.IMG_DIR = '/assets/';
module.exports.DOT_LINE = '..................................';
module.exports.LONG = '..................................................................................................................................................................................................................................................................................................................................................................................';
module.exports.LONGER = '....................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................';
module.exports.SIGNATURE = '..............................................';
module.exports.LINE_WORD = '....................................................................................................................................................................';
