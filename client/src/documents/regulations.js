const REGULATION_BID_EXCERPT = "„Przystępując do Licytacji, Licytant deklaruje cenę maksymalną Towaru, którą w przypadku wygranej zobowiązuje się zapłacić…” DW : Artykuł 17. ustawy o prawach konsumenta obliguje do wyraźnego sposobu oznaczania, że konsument ma do czynienia z zamówieniem z obowiązkiem zapłaty. Jak można się domyśleć, dotyczy to umów zawieranych drogą elektroniczną. Kontynuacja oznacza zgodę i świadome przyjęcię odpowiedzialności za regulację należności wobec wystawiającego.";

module.exports = {
  REGULATION_BID_EXCERPT
};
