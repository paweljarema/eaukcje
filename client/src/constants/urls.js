const
  HOME_PATH = '/',
  BUY_PACKETS_PATH = '/wykup-pakiety',
  NEW_AUCTION_PATH = '/konto/aukcje/dodaj',
  LOGIN_PATH = '/konto/zaloguj',
  PROFILE_PATH = '/konto/ustawienia',
  PROMOTED_AUCTIONS_PATH = '/promowane',
  NEW_AUCTIONS_PATH = '/najnowsze',
  AUCTIONS_PATH = '/aukcje',
  MY_AUCTIONS_PATH = '/moje-aukcje',
  PAGE_404_PATH = '/404',
  MY_AUCTIONS_ENDED_PATH = MY_AUCTIONS_PATH + '/zakonczone',
  MY_AUCTIONS_POST_AGAIN_PATH = MY_AUCTIONS_PATH + '/wystaw-ponownie/:title/:id',
  ALL_CATEGORIES_PATH = '/wszystkie-kategorie';

module.exports = {
  HOME_PATH,
  BUY_PACKETS_PATH,
  NEW_AUCTION_PATH,
  LOGIN_PATH,
  PROFILE_PATH,
  PAGE_404_PATH,
  PROMOTED_AUCTIONS_PATH,
  NEW_AUCTIONS_PATH,
  AUCTIONS_PATH,
  MY_AUCTIONS_PATH,
  MY_AUCTIONS_ENDED_PATH,
  MY_AUCTIONS_POST_AGAIN_PATH,
  ALL_CATEGORIES_PATH
};
