const OPTIONS = {
  type: 'pdf',
  format: 'A4',
  orientation: 'portrait',
  // zoomFactor: 0.55,
  border: {
    top: '2in',
    right: '1in',
    bottom: '2in',
    left: '1in'
  }
};

module.exports.OPTIONS = OPTIONS;
