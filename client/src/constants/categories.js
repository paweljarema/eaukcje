const
	leftoutCategories = ['Rolnictwo', 'Muzyka i edukacja', 'Ślub i wesele'],
	selectedCategories = ['Motoryzacja', 'Nieruchomości', 'Dom i ogród', 'Elektronika', 'Moda', 'Zwierzęta', 'Dzieci i młodzież', 'Sport i rekreacja', 'Turystyka'],
	newCategories = ['Uroda', 'Kultura i rozrywka', 'Zdrowie', 'Kolekcje i sztuka', 'Spożywcze'], // 'AGD i RTV'
	frontPageCategories = selectedCategories.concat(newCategories);

module.exports.frontPageCategories = frontPageCategories;
