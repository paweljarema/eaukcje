const
  TRUTH_ISSUE = 'Ogłoszenie zawiera nieprawdziwe informacje',
  PAYMENT_ISSUE = 'Problem z płatnościami',
  SEND_ISSUE = 'Problem z wysyłką towaru',
  CHEAT_ISSUE = 'Sprzedawca oszukuje',
  OTHER_ISSUE = 'Inne',

  VIOLATIONS = [
    TRUTH_ISSUE,
    PAYMENT_ISSUE,
    SEND_ISSUE,
    CHEAT_ISSUE,
    OTHER_ISSUE
  ];

module.exports = {
  VIOLATIONS,
  TRUTH_ISSUE,
  PAYMENT_ISSUE,
  SEND_ISSUE,
  CHEAT_ISSUE,
  OTHER_ISSUE
};
