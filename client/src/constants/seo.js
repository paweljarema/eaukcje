const
  CATEGORY_META_PROPS = [ 'metaTitle', 'description', 'metaDescription', 'ogDescription' ],
  CATEGORY_META_LABELS = [ 'Meta tytuł', 'Opis', 'Meta opis', 'Opis Open Graph' ];

module.exports = {
  CATEGORY_META_PROPS,
  CATEGORY_META_LABELS
};
