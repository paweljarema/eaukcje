const
  in_prod = process.env.NODE_ENV === 'production',
  prot = in_prod ? 'https://' : 'http://',
  host = in_prod ? 'eaukcje.pl' : 'localhost:3000',

  business = {
    firm: 'Polmarket',
    NIP: '9731058131',
    address: 'ul. Poznańska 20B 65-128 Zielona Góra',
    name: 'eaukcje.pl',
    phone: '+48 530 606 040',
    copyright: 'Copyright © 2018. Wszelkie prawa zastrzeżone',
    domain: host,
    host: prot + host,
    //https://drive.google.com/file/d/1ATFGGA43ZHT--pE-wiiYfTYQ84xD5Yh2/view?usp=sharing
	  logo: prot + 'drive.google.com/uc?export=view&id=1ATFGGA43ZHT--pE-wiiYfTYQ84xD5Yh2',
	  //https://drive.google.com/file/d/1CPkfZqKtmuTCULLF3xHWHUq4-GtCPBYu/view?usp=sharing
	  image: prot + 'drive.google.com/uc?export=view&id=1CPkfZqKtmuTCULLF3xHWHUq4-GtCPBYu',
    invoice: {
      name: 'EAUKCJE',
  		firm: 'POLMARKET.PL SP. Z O.O. SP. K.',
  		address: 'ul. Poznańska 20B\n65-137 Zielona Góra',
  		NIP: '9731059426'
  	}
  };

module.exports = business;
