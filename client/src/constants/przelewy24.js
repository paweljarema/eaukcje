const
  BUY_AUCTION_PACKET_ID = 'auctionPackets',
  BUY_PROMO_PACKET_ID = 'promoPackets';

module.exports = {
  BUY_AUCTION_PACKET_ID,
  BUY_PROMO_PACKET_ID
};
