function concatClasses() {
  return [...arguments]
    .filter(a => Boolean(a))
    .join(' ');
}

module.exports = {
  concatClasses
}
