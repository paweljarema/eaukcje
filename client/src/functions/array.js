function isArray(object) {
  return Object.prototype.toString.call(object) === '[object Array]';
}

function arraysAreEqual(a1, a2) {
  if (a1 === a2) return true;
  if (a1 == null || a2 == null) return false;
  if (a1.length !== a2.length) return false;

  for (let i = 0; i < a1.length; i++)
    if (a1[i] !== a2[i]) return false;

  return true;
}

function arraysAreDifferent(a1, a2) {
  return !arraysAreEqual(a1, a2);
}

function arrayIsEmpty(object) {
  if ( !isArray(object) ) return true;
  return ( object.length === 0 );
}

function arrayIsNotEmpty(object) {
  return !arrayIsEmpty(object);
}

function filterUnique(val, i, arr) {
	return arr.indexOf(val) === i;
}

module.exports = {
  isArray,
  arrayIsEmpty,
  arrayIsNotEmpty,
  arraysAreEqual,
  arraysAreDifferent,
  filterUnique
};
