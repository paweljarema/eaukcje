const {
  BUY_AUCTION_PACKET_ID,
  BUY_PROMO_PACKET_ID
} = require('../constants/przelewy24');

function isNoTeen(count) {
  return count < 20;
}

function pluralForm({ count, declinationArray }) {
  if (count === 1) return declinationArray[0]
  if (count >= 2 && count <= 4 && isNoTeen(count)) return declinationArray[1];
  return declinationArray[2];
}

function getDeclinationFor(id) {
  switch (id) {
    case BUY_AUCTION_PACKET_ID: return ['aukcja', 'aukcje', 'aukcji'];
    case BUY_PROMO_PACKET_ID: return ['wyróżnienie', 'wyróżnienia', 'wyróżnień'];
  }
}

function bigWordsIn(string) {
  const MAX_LENGTH = 6;

  return string
    .split(' ')
    .some(word => word.length > MAX_LENGTH);
}

function longTitle(string) {
  const MAX_LENGTH = 30;

  return string.length > MAX_LENGTH;
}

module.exports = {
  pluralForm,
  getDeclinationFor,
  bigWordsIn
}
