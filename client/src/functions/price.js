function nicePrice(price) {
  let nice = price;

  if (!price) nice = 0;
  if (parseInt(price) !== Number(price))
    nice = price.toFixed(2);

  return nice + ' zł';
}

function validatePrice(price) {
  if (!price) price = 0;
  const test_price = Number(price);

  if (isNaN(test_price)) return false;
  if (test_price < 0 || test_price > 9999999) return false;
  if (parseInt(test_price * 100) / 100 !== test_price) return false;
  if (!(/^[\d,\.]+$/.test(price))) return false;

  return true;
}

module.exports = {
  nicePrice,
  validatePrice
};
