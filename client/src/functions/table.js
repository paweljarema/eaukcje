function applyToTable(func) {
	const e = document.querySelector('table');
	if (e) func(e);
}

function fadeTableIn() {
	applyToTable((e) => e.style.opactiy = 1);
}

function fadeTableOut() {
	applyToTable((e) => e.style.opacity = 0.8);
}

const loadStyle = (e) => e.style.opacity = 0.8;
const loadedStyle = (e) => e.style.opacity = 1;

module.exports = {
  applyToTable,
	fadeTableIn,
	fadeTableOut,
	loadStyle,
	loadedStyle
};
