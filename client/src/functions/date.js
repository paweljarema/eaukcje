const
  MONTHS = ['Stycznia', 'Lutego', 'Marca', 'Kwietnia', 'Maja', 'Czerwca', 'Lipca', 'Sierpnia', 'Września', 'Października', 'Listopada', 'Grudnia'],
  MONTH = ['Styczeń', 'Luty', 'Marzec', 'Kwiecień', 'Maj', 'Czerwiec', 'Lipiec', 'Sierpień', 'Wrzesień', 'Październik', 'Listopad', 'Grudzień'],
  MONTH_SHORT = ['Sty', 'Lut', 'Mar', 'Kwi', 'Maj', 'Cze', 'Lip', 'Sie', 'Wrz', 'Paź', 'Lis', 'Gru'],

  { padLeft } = require('./number');

function deconstructDate(timestamp) {
  let
    date = new Date(timestamp),
    year = date.getFullYear(),
    month = date.getMonth(),
    day = date.getDate(),

    millis = date.getTime() - new Date(year, month, day).getTime();

  return {
    year: year,
    month: padLeft(month + 1, 2),
    day: padLeft(day, 2),
    millis: millis
  };
}

function numberDate(timestamp) {
  let date = new Date(timestamp);

  return `${ zero(date.getDate()) } ${ zero(date.getMonth() + 1) } ${ date.getFullYear() }`;
}

function friendlyDate(timestamp) {
  let date = new Date(timestamp);

  return `${ date.getDate() } ${ MONTHS[ date.getMonth() ] } ${ date.getFullYear() }`;
}

function monthPl(index) {
  return MONTH[index];
}

function datetimeFromMillis(millis) {
  let date = new Date(millis);

  return `${ date.getDate() } ${ MONTH_SHORT[ date.getMonth() ] } ${ date.getHours() }:${ zero(date.getMinutes()) }`;
}

function zero(number) {
  if (number < 10) return '0' + number;
  else return number;
}

module.exports = {
  numberDate,
  friendlyDate,
  monthPl,
  datetimeFromMillis,
  deconstructDate
};
