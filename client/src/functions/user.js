const ANON = 'Anonim';

function username(user) {
  if (!user) return ANON;

  let { firstname, lastname } = user;

  return `${ firstname || '' } ${ lastname || '' }`.trim() || ANON;
}

function userfirmname(user) {
  let firm_name;

  if (user.firm)
    firm_name = user.firm.firm_name;

  return firm_name || username(user);
}

function usergender(user) {
  const
    EXCEPTIONS = [ 'Kuba', 'Jarema', 'Zawisza' ],
    MALE = 'male',
    FEMALE = 'female';

  let { firstname = 'Anonim' } = user;

  if (firstname.endsWith('a') && !EXCEPTIONS.includes(firstname))
    return FEMALE;
  else
    return MALE;
}

function userinitials(name) {
  if (name === ANON) return name;

  return name
    .split(' ')
    .map(n => n.slice(0, 1) + '.')
    .join(' ');
}

function usercity(user) {
  let city;

  if (user.address)
    city =  user.address.city;

  return city || '';
}

function stripuserstreet(user) {
  let street, house, apartment;

  if (user.address) {
    let chunks = user.address.street.split(/\s+|\//g);

    street = chunks[0];
    house = chunks[1];
    apartment = chunks[2];
  }

  return {
    street: street || '',
    house: house || '',
    apartment: apartment || ''
  };
}

function userstreet(user) {
  let { street } = stripuserstreet(user);
  return street;
}

function userhouse(user) {
  let { house } = stripuserstreet(user);
  return house;
}

function userapartment(user) {
  let { apartment } = stripuserstreet(user);
  return apartment;
}

function userpostcode(user) {
  let postcode;

  if (user.address)
    postcode = user.address.postal;

  return postcode || '';
}

function usernip(user) {
  let nip;

  if (user.firm)
    nip = user.firm.nip;

  return nip || '';
}



module.exports = {
  username,
  userfirmname,
  usergender,
  userinitials,
  usercity,
  userstreet,
  userhouse,
  userapartment,
  userpostcode,
  usernip
};
