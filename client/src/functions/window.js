function getVisualWindowWidth(window) {
	if (!window) {
		alert('error obtaining visual window width on this device.');
		return 0;
	}

	let
		width = window.innerWidth,
		visualWidth = (window.visualViewport || {}).width;

  // let ratio = window.devicePixelRatio || 1;
  // if (ratio !== 1) width *= ratio;

	return visualWidth || width;
}

export {
  getVisualWindowWidth
};
