const
  { AUCTIONS_PATH: _AUCTIONS_PATH,
    PROMOTED_AUCTIONS_PATH,
    NEW_AUCTIONS_PATH } = require('../constants/urls'),

  AUCTIONS_PATH = _AUCTIONS_PATH.slice(1),
  AUCTIONS_PATH_REGEX = `(${ _AUCTIONS_PATH }|${ PROMOTED_AUCTIONS_PATH }|${ NEW_AUCTIONS_PATH })`,
  AUCTION_PATH_PARAMS = '/:title/:shortid', // ':year/:month/:day/:title/:millis',
  { deconstructDate } = require('./date'),

  business = require('../constants/business');

function auctionPhotoLink(auction) {
  return `${ business.host }/auction/${ auction._id }/photo`;
}

function makeCatLink(array) {
  let chunks = array.map(name => removeInvalidChars(name));
  return [business.host, AUCTIONS_PATH].concat(chunks).join('/');
}

function makeCatLinkRelative(array) {
  let chunks = array.map(name => removeInvalidChars(name));
  return ['', AUCTIONS_PATH].concat(chunks).join('/');
}

function catPathToArray(path, categories, array = []) {
  if (!categories || !path) return array;

  const
    cats = path.replace(AUCTIONS_PATH_REGEX, AUCTIONS_PATH).split('/').filter(v => v && v !== AUCTIONS_PATH),
    cat = cats[0],
    index = categories.findIndex(
      c => removeInvalidChars(c.name) === cat
    );

  if (cat && index >= 0) {

    array.push(categories[ index ].name);

    return catPathToArray(
      cats
        .slice(1)
        .join('/'),
      categories[ index ].subcategories,
      array
    );
  } else return array;

}

function matchParamsAreDifferent(set1, set2) {
  return (
    set1.millis !== set2.millis ||
    set1.day !== set2.day ||
    set1.month !== set2.month ||
    set1.year !== set2.year
  );
}

function auctionTimestampFromMatchParams({ year, month, day, millis }) {
  return new Date(
      parseInt(year),
      parseInt(month) - 1,
      parseInt(day)
    ).getTime() + parseInt(millis);
}

function auctionPathFromAuction(auction) {
  const
    title = removeInvalidChars(auction.title),
    categoryPath = (auction.categories || [])
      .map(cat => removeInvalidChars(cat))
      .join('/');

  return [ AUCTIONS_PATH, categoryPath, AUCTION_PATH_PARAMS ]
    .join('/')
    .replace(':title', title)
    .replace(':shortid', auction._shortid);
}

function invoiceLink({ _id }) {
  return `/faktura/` + _id;
}

function invoiceLinkAdmin(admin, doc, see) {
  return `/api/faktura/${ admin._id }/${ doc._id }/${ see ? 1 : 0 }`;
}

function invoiceSymfonyExportLink(admin, doc) {
  return `/api/symfonia/${ admin._id }/${ doc._id }`;
}

function invoiceSymfonyBatchExportLink(admin, from, to, month) {
  return `/api/symfonia_batch_export/${ admin._id }/${ from }/${ to }/${ month }`;
}

function auctionEditLink(auction) {
  return `edytuj-aukcje/${ removeInvalidChars(auction.title) }/${ auction._id }`;
}

function friendlyAuctionLink(auction, start = '/') {
  return `${ start }${ auctionPathFromAuction(auction).replace(/\/+/g, '/') }`;
  // return auctionLink(auction, start);
}

function removeInvalidChars(str) {
  const invalid = 'żźćńęąśłó'.split('');
  const replace = 'zzcneaslo'.split('');

  if (str) {
    let result = str.toLowerCase();

    for (let i = 0, l = invalid.length; i < l; i++)
      result = result.replace( new RegExp(invalid[i]), replace[i] );

    return result
      .replace(/[\s\/#\%\\]+/g, '-')
      .replace(/-+/g, '-');

  } else return '';
}

function paramPath(path, params) {
  let result = path;
  for (let key in params)
    result = result.replace(`:${ key }`, removeInvalidChars(params[ key ]))

  return result;
}

function catDataFromPath(path, categories) {
  function getAllNamesAndLast(path = [], normalizedPath = [], objects, object) {
    if (path === null) {
      // top level, no path
      let _active_count = 0;
      for (let cat of categories) {
        _active_count += cat.activeAuctions || 0;
        //console.log(cat.activeAuctions);
      }
      return { _active_count };
    }

    if (!path.length) {
      // all done
      let
        _categories = normalizedPath,
        _active_category = object,
        _active_count = object.activeAuctions || 0;

      return { _categories, _active_category, _active_count };
    }

    let
      catName = path.shift(),
      traverse = (objects || categories);

    for (let i = 0, l = traverse.length; i < l; i++) {
      if (removeInvalidChars(traverse[i].name) === catName) {

        object = traverse[i];
        normalizedPath.push(object.name);

        return getAllNamesAndLast(path, normalizedPath, object.subcategories, object);
      }
    }

    return '';
  }

  path = path.replace(AUCTIONS_PATH_REGEX, '');
  let chunks = path ? path.slice(1).split('/') : null;

  return getAllNamesAndLast(chunks);
}

module.exports = {
  AUCTIONS_PATH,
  AUCTION_PATH_PARAMS,
  invoiceLink,
  invoiceLinkAdmin,
  friendlyAuctionLink,
  auctionEditLink,
  removeInvalidChars,
  auctionTimestampFromMatchParams,
  matchParamsAreDifferent,
  catPathToArray,
  makeCatLink,
  auctionPhotoLink,
  invoiceSymfonyExportLink,
  invoiceSymfonyBatchExportLink,
  makeCatLinkRelative,
  paramPath,
  catDataFromPath
};
