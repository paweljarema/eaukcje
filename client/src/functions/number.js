function nicePrice(price) {
  return price.toFixed(2) + ' zł';
}

function discardVat(total, vat) {
  return total - (total * (vat/100));
}

function discardVatClosure(vat) {
  return (total) => discardVat(total, vat);
}

function sumUp(store, fieldname, apply) {
  return store.reduce((a, b) => {
    if (apply)
      return a += apply(b[fieldname]);
    else
      return a += b[fieldname];
  }, 0);
}

function valueOrZero(value) {
  if (isNaN(value)) return 0;
  return Number(value);
}

function padLeft(text, length, char = '0') {
  let diff = length - String(text).length;

  if (diff > 0)
    return char.repeat(diff) + String(text);
  else
    return String(text);
}

module.exports = {
  nicePrice,
  discardVat,
  discardVatClosure,
  sumUp,
  valueOrZero,
  padLeft
};
