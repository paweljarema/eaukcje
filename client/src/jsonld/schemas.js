const
  business = require('../constants/business'),

  {
    ldScript,
    schemaDate,
    schemaItemCondition,
    schemaItemAvailability,
    schemaSellerType,
    schemaSellerName
  } = require('./functions'),

  { friendlyAuctionLink } = require('../functions/links');

const
  CONTEXT = { "@context": "https://schema.org" },
  PRODUCT = { "@type": "Product" },
  OFFER = { "@type": "Offer" },
  AGGREGATE_RATING = { "@type": "AggregateRating" },
  SEARCH_ACTION = { "@type": "SearchAction" };

const
  PLN = 'PLN';

function siteLd() {
  return ldScript({
    ...CONTEXT,
    "@type": "WebPage",
    "url": business.host,
    "name": business.name,
    "potentialAction": {
      ...SEARCH_ACTION,
      "target": `${ business.host }/aukcje/szukaj/Kategorie/{search_term_string}`,
      "query-input": "required name=search_term_string"
    }
  });
}

function auctionLd(auction, owner = {}) {
  if (!auction) throw 'auction required as argument to auctionLD()';

  let { _id, title, shortdescription, price } = auction;
  let { firstname, lastname, rating } = owner;

  return ldScript({
    ...CONTEXT,
    ...PRODUCT,
    "name": title,
    "image": [
      business.host + `/auction/${ _id }/photo`
    ],
    "description": shortdescription,
    "aggregateRating": {
      ...AGGREGATE_RATING,
      "ratingValue": String(rating || 5),
      "reviewCount": String(auction.raters.length || 1)
    },
    "offers": {
      ...OFFER,
      "url": business.host + friendlyAuctionLink(auction),
      "priceCurrency": PLN,
      "price": String(price.current_price || price.start_price),
      "priceValidUntil": schemaDate(new Date()),
      "itemCondition": schemaItemCondition(auction),
      "availability": schemaItemAvailability(auction),
      "seller": {
        "@type": schemaSellerType(owner),
        "name": schemaSellerName(owner)
      }
    }
  });
}

// for tests only, comment out afterwards
function thisWasRendered(component) {
  component.wasRendered = ( component.wasRendered || 0 ) + 1;
  console.log(component.constructor.name, component.wasRendered);
}

export {
  siteLd,
  auctionLd,

  thisWasRendered
};
