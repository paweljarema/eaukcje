const
  IN_STOCK = "https://schema.org/InStock",
  OUT_OF_STOCK = "https://schema.org/OutOfStock",
  CONDITION_NEW = "https://schema.org/NewCondition",
  CONDITION_USED = "https://schema.org/UsedCondition",

  ORGANIZATION = 'Organization',
  PERSON = 'Person',

  ANONIM = 'Anonim',
  STAN = 'Stan',
  STAN_NOWY = "Nowy",
  STAN_UZYWANY = "Używany";

// function ldScript(object) {
//   if (!object) return null;
//
//   console.log(Helmet);
//
//   return (
//     <Helmet>
//       <script type="application/ld+json">
//         { JSON.stringify(object) }
//       </script>
//     </Helmet>
//   );
// }

function ldScript(object) {
  let
    script = document.head.querySelector("[type='application/ld+json']"),
    source = JSON.stringify(object);

  if (script) {
    script.innerHTML = source;
  } else {
    script = document.createElement("script");
    script.type = 'application/ld+json';
    script.innerHTML = source;

    if (document.head)
      document.head.insertBefore(script, document.head.firstChild.nextSibling);
      //document.head.appendChild(script);
  }
}

// <script
//     type="application/ld+json"
//     dangerouslySetInnerHTML={{ __html: JSON.stringify(data) }}
//   />;

function schemaSellerName(user) {
  let { firm } = user || {};

  if (firm && firm.firm_name)
    return firm.firm_name;

  if (user) {
    let { firstname = '', lastname = '' } = user;
    return `${ firstname } ${ lastname }`.trim() || ANONIM;
  } else return ANONIM;

}

function schemaSellerType(user = {}) {
  let { firm } = user;
  if (firm && firm.firm_name) return ORGANIZATION;
  else return PERSON;
}

function schemaItemAvailability(auction) {
  if (auction.ended)
    return OUT_OF_STOCK;
  else
    return IN_STOCK;
}

function schemaItemCondition(auction) {
  let state = auction.properties.find(property => property.name === STAN);
  if (state) {
    switch (state.value) {
      case STAN_NOWY:
        return CONDITION_NEW;
      case STAN_UZYWANY:
        return CONDITION_USED;
    }
  };

  return null;
}

function schemaDate(date) {
  return date.toISOString();
}

export {
  ldScript,
  schemaDate,
  schemaItemCondition,
  schemaItemAvailability,
  schemaSellerType,
  schemaSellerName
};
