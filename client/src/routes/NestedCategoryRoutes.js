import React from 'react';
import AuctionListSearch from '../components/auctions/AuctionListSearch';

import { Route, Switch } from 'react-router-dom';
import { AuctionDetails } from '../components/Auctions';
import { removeInvalidChars, AUCTION_PATH_PARAMS } from '../functions/links';

function nestedRoute(path, props, i, Component) {
  return (
    <Route
      key={ '___' + path }
      path={ path }
      render={ __props => (
        <Component { ...__props }
          searchProps={ props.searchProps }
          auctionProps={ props.auctionProps }
          categories={ props.categories[i].subcategories }
        />
      )}
    />
  );
}

function nestedCategoryRoute(path, props, i) {
  return nestedRoute(path, props, i, CategoryRoutes);
}

function nestedAuctionRoute(path, props, i) {
  return nestedRoute(path, props, i, AuctionRoutes);
}

function searchRoute(path, props, i) {
  const { searchProps } = props;
  return (
    <Route
      exact
      key={ '__' + path }
      path={ path }
      render={ props => (
        <AuctionListSearch { ...props } { ...searchProps } />
      )}
    />
  );
}

function auctionRoute(path, props, i) {
  const
    url = `${ path }${ AUCTION_PATH_PARAMS }`,
    { auctionProps } = props;

  return (
    <Route
      exact
      key={ '_' + path }
      path={ url }
      render={
        props => <AuctionDetails { ...props } { ...auctionProps } />
      }
    />
  );
}

function ChildOrder({ children, sort }) {
  function childSortFunc(child1, child2) {
    if (child1.props.path.length > child2.props.path.length) return -1;
    if (child1.props.path.length < child2.props.path.length) return 1;
    return 0;
  }

  if (sort) {
    const sortedChildren = React.Children
      .toArray(children)
      .sort(childSortFunc);

    return <>{ sortedChildren }</>;
  } else return <>{ children }</>;
}

function traverseCategories(props, putFunctions, sort = false) {
  const
    currentPath = props.match.path,
    categories = props.categories
      .map(cat => removeInvalidChars(cat.name)),

    { onCurrentPath, onSubcategories, onNoSubcategories } = putFunctions;

  if (!currentPath) throw 'No match prop, No current path.';

  return (
    <ChildOrder sort={ sort }>
      { onCurrentPath && onCurrentPath(currentPath, props) }
      {
        categories.map((cat, i) => {
          const
            path = `${ currentPath }/${ cat }`,
            hasSubcategories = Boolean(props.categories[i].subcategories);

          if (hasSubcategories && onSubcategories)
            return onSubcategories(path, props, i);
          else if (onNoSubcategories)
            return onNoSubcategories(path, props, i);
        })
      }
    </ChildOrder>
  );
}

function CategoryRoutes(props) {
  return traverseCategories(props, {
    onCurrentPath: searchRoute,
    onSubcategories: nestedCategoryRoute,
    onNoSubcategories: searchRoute
  });
}

function AuctionRoutes(props) {
  return traverseCategories(props, {
    onCurrentPath: auctionRoute,
    onSubcategories: nestedAuctionRoute,
    onNoSubcategories: auctionRoute
  });
}

export default function NestedCategoryRoutes(props) {
  return (
    <>
      <CategoryRoutes { ...props } />
      <AuctionRoutes { ...props } />
    </>
  )
}
