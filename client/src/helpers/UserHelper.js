const year_in_millis = 1000 * 60 * 60 * 24 * 365;

function is18(momentDate) {
	if (momentDate)
		return ((Date.now() - momentDate.valueOf()) / year_in_millis) >= 18;
	else
		return false;
}

function userIs18(user) {
	return ((Date.now() - (user.birthdate || 0)) / year_in_millis) >= 18;
}

export const UserHelper = {
	is18,
	userIs18
};
