const
  FILTER_BUTTON_PRESSED = 'filter_button_pressed',
  PARAMS_IN_BREADCRUMBS = 'params_in_breadcrumbs',
  PROPERTY_PICKER_HINT_EVENT = 'property_picker_hint';

export {
  FILTER_BUTTON_PRESSED,
  PARAMS_IN_BREADCRUMBS,
  PROPERTY_PICKER_HINT_EVENT
};

export default class Observer {
  constructor() {
    if (Observer.exists) {
      return Observer.instance;
    }

    Observer.instance = this;
    Observer.exists = true;

    this.events = {};
    this.id = 0;
  }

  watchEvent(event, action) {
    action.id = this.id;

    if (this.events[event])
      this.events[event].push(action);
    else
      this.events[event] = [action];

    return this.id++;
  }

  unregister(event, id) {
    let actions = this.events[event];

    if (actions) {
      for (let i = 0; i < actions.length; i++)
        if (actions[i].id === id) actions.splice(i, 1);
    }
  }

  trigger(event, optionalParams) {
    let actions = this.events[event];

    if (actions)
      for (let act of actions)
        act(optionalParams);
  }

  clearEvent(event) {
    if (this.events[event])
      this.events[event] = null;
  }

  clear() {
    // pointless
    // this.events = {};
  }
}
