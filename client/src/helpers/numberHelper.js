function restrictInputToNumber(value = '') {
  const string = String(value).replace(/[\D-e]/g, '');
  return isNaN(string) ? '' : string;
}

module.exports = {
  restrictInputToNumber
}
