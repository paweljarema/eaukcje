class RedirectHelper {
  constructor(url) {
    if (url) this._url = url;

    if (RedirectHelper.exists)
      return RedirectHelper.instance;

    RedirectHelper.exists = true;
    RedirectHelper.instance = this;

    return this;
  }

  getUrl() {
    let url = this._url;
    this._url = null;

    return url;
  }
}

export default RedirectHelper;
