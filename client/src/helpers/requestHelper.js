const axios = require('axios');

class Request {
  constructor({ method, url, options, callback, group_id }) {
    this._group_id = group_id;
    this._method = method;
    this._url = url;
    this._options = options;
    this._callback = callback;
  }

  params() {
    return {
      method: this._method,
      url: this._url,
      data: this._options
    };
  }

  callback(data, completionId) {
    this._callback(data, completionId);
  }

  groupId() {
    return this._group_id;
  }
}

class HttpRequestQueue {
  constructor() {
    if (HttpRequestQueue.exists)
      return HttpRequestQueue.instance;

    this._queue = [];
    this._requestInProgress = false;
    this._completed = 0;

    HttpRequestQueue.instance = this;
    HttpRequestQueue.exists = true;

    return this;
  }

  push(request) {
    if (this._requestInProgress)
      this._queue.push(request);
    else
      this.realize(request);
  }

  next() {
    this._requestInProgress = false;

    if (this._queue.length)
      this.realize(this._queue.shift());
  }

  realize(request) {
    this._requestInProgress = true;

    axios(request.params())
      .then(({ data }) => {
        let completionId = data.length ? this._completed++ : null;

        request.callback(data, completionId); // change if too speciffic
        this.next();
      });
  }

  terminate(group_id) {
    this._queue = this._queue
      .filter(request => request.groupId() !== group_id);

    this._requestInProgress = false;
  }
}

function getRequest({ url, callback, group_id }) {
  const queue = new HttpRequestQueue();

  queue.push(new Request({
    group_id: group_id,
    method: 'GET',
    url: url,
    callback: callback
  }));
}

function terminateRequestsFor(group_id) {
  const queue = new HttpRequestQueue();

  queue.terminate(group_id);
}

export {
  HttpRequestQueue,
  getRequest,
  terminateRequestsFor
};
