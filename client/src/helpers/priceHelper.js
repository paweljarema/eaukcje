const PriceHelper = {
	write: (value) => {
		let formated;

		if (isInteger(value))
			formated = String(value);
		else
			formated = String(value.toFixed(2));

		return formated.replace(/\./, ',');
	},
	read: (value) => {
		return Number(String(value).replace(/,/, '.'));
	}
};

function isInteger(value) {
	return parseInt(value) === Number(value);
}

export default PriceHelper;
