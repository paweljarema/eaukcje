import React, { Component } from 'react';
import Progress from './Progress';

import { Redirect, withRouter } from 'react-router-dom';
import { HOME_PATH, NEW_AUCTION_PATH } from '../constants/urls';
import { valueOrZero } from '../functions/number';
import { nicePrice } from '../functions/price';
import { pluralForm, getDeclinationFor } from '../functions/words';
import { hardRedirect } from './auctions/functions'
import {
  BUY_AUCTION_PACKET_ID,
  BUY_PROMO_PACKET_ID
} from '../constants/przelewy24';

import { connect } from 'react-redux';
import * as przelewy24Actions from '../actions/przelewy24Actions';

import axios from 'axios';

import './BuyPackets.css';
import './BuyPacketsMobile.css';

const BuyButton = ({ text, totalCost, callback, disabled }) => {
  return (
    <div
      className={"p24-buy-button button clickable" + (disabled ? ' disabled' : '')}
      onClick={ callback }>

      {
        text || `Zapłać ${ disabled ? '' : nicePrice(totalCost) }`
      }
    </div>
  );
}

// z <span className="przelewy24-signature">Przelewy<span>24</span></span>

const Packet = ({ id, index, selected, packet, promotion, callback }) => {
  let
    declinationArray = getDeclinationFor(id),

    { unlimited, name, count, cost } = packet,

    active = index === selected,
    className = `packet${ promotion ? ' promotion' : '' }${ unlimited ? ' unlimited' : '' }${ active ? ' active' : '' }`;

  return (
    <div className={ className }>
      <div className="packet-inner">
        {
          name && <div className="packet-name">{ name }</div>
        }

        <div className="packet-description">
          <div className="packet-count">
            {
              unlimited
              ?
              <div className="medium red">
                bez limitu
                <br />
                aukcji
              </div>
              :
              <>
                <div className="big red">
                  {
                    count
                  }
                </div>

                <div className="small">
                  {
                    pluralForm({ count, declinationArray })
                  }
                </div>
              </>
            }
          </div>
          <div className="packet-cost">
            {
              nicePrice(cost)
            }
          </div>
          <div
            className="packet-action"
            onClick={ () => callback(id, packet, index) }>

            {
              active ? 'Odznacz' : 'Wybierz'
            }

          </div>
        </div>
      </div>

      {
        unlimited && '* do końca roku za darmo'
      }
    </div>
  );
}

const ListPackets = (props) => {
  let { id, title, items } = props;

  return (
    <div>
      <h2>{ title }</h2>
      <br />
      <div className="packet-purchase-container">
        {
          items.map((item, i) => (
            <Packet
              key={ `${ id }_${ i }` }
              packet={ item }
              index={ i }
              { ...props }
            />
          ))
        }
      </div>
    </div>
  );
}

export default connect(null, przelewy24Actions)(
  class extends Component {
    constructor(props) {
      super(props);

      this.state = {
        settings: null,
        loading: true,

        buyPackets: false,
        buyPromos: false
      };

      this.select = this.select.bind(this);
      this.buy = this.buy.bind(this);
      this.cancel = this.cancel.bind(this);
    }

    componentDidMount() {
      axios
        .get('/settings')
        .then(({ data }) => {
          this.setState({ settings: data, loading: false });
        });
    }

    select(id, packet, index) {
      let stateKey, stateDataKey;

      switch(id) {
        case BUY_AUCTION_PACKET_ID:
            stateKey = 'buyPackets';
          break;

        case BUY_PROMO_PACKET_ID:
            stateKey = 'buyPromos';
          break;
      };

      stateDataKey = stateKey + 'Data';

      if (this.state[stateKey] && this.state[stateDataKey].index === index)
        this.setState({ [stateKey]: false, [stateDataKey]: undefined });
      else
        this.setState({ [stateKey]: true, [stateDataKey]: { ...packet, index }})
    }

    buy() {
      let
        { auction_id, auction_title, auction_url } = this.props,
        { buyPackets, buyPacketsData,
          buyPromos, buyPromosData } = this.state;

      if (!buyPackets && !buyPromos) {
        alert('Wybierz jedną z opcji');
        return;
      }

      if (auction_id && !buyPromos) {
        alert('Aby wypromować aukcję, wykup pakiet promocji');
        return;
      }

      let price = countCost(this.state, promotionActive(this.state.settings));

      if (price === 0) {
        this.redirect = auction_url || NEW_AUCTION_PATH;
        this.setState({ cancelled: true });
        return;
      }

      this.props.aggregatedOrder({
        price: price,
        buy_auctions: buyPackets,
        buy_promos: buyPromos,
        promote_auction: Boolean(auction_id),

        auction_id: auction_id,
        auction_title: auction_title,
        auction_url: auction_url,

        buy_auctions_data: buyPacketsData,
        buy_promos_data: buyPromosData
      });
    }

    cancel() {
      this.redirect = this.props.auction_url || HOME_PATH;
      if (window.location.href.indexOf(this.redirect) !== -1) this.redirect = HOME_PATH;

      this.setState({ canceled: true });
    }

    render() {
      const
        { user, promoteOnly } = this.props,
        { settings, loading, buyPackets, buyPromos } = this.state;

      if (!user) return <Redirect to={ HOME_PATH } />;
      if (this.redirect) hardRedirect(startWithSlash(this.redirect)); //return <Redirect to={ startWithSlash(this.redirect) } />;
      if (loading) return <Progress />;

      const
        promotion = promotionActive(settings),
        totalCost = countCost(this.state, promotion);

      return (
        <div className="packet-purchase-choices">
          {
            !promoteOnly && (
              <ListPackets
                id={ BUY_AUCTION_PACKET_ID }
                items={ settings.auctionPackets }
                title="Dodaj ogłoszenie"
                promotion={ promotion }
                selected={ buyPackets ? this.state.buyPacketsData.index : null }
                callback={ this.select }
              />
            )
          }

          <ListPackets
            id={ BUY_PROMO_PACKET_ID }
            items={ settings.promoPackets }
            title="Promuj ogłoszenie"
            selected={ buyPromos ? this.state.buyPromosData.index : null }
            callback={ this.select }
          />

          <div className="buttons">
            <div
              className="button clickable"
              onClick={ this.cancel }>
              Nie, dziękuję
            </div>

            <BuyButton
              disabled={ totalCost === 0 }
              callback={ this.buy }
              totalCost={ totalCost }
            />
          </div>
        </div>
      );
    }
  }
);

class PromoteFromCredits extends Component {
  cancel() {
    let url = this.props.auction_url ? startWithSlash(this.props.auction_url) : HOME_PATH;

    //this.props.history.push(url);
    hardRedirect(url);
  }

  promote() {
    let { history, auction_id, auction_url } = this.props;
    axios
      .post('/przelewy24/justPromote', {
        _auction: auction_id,
        auction_url: startWithSlash(auction_url)
      })
      //.then(({ data }) => history.push(data));
      .then(({ data }) => hardRedirect(data))
  }

  render() {
    const
      { user, auction_title } = this.props,
      { packets } = user || {},
      { promos } = packets || {};

    if (!user) return null;

    return (
      <div className="packet-purchase-choices">
        <div className="promote-from-credits">
          <h2>Czy chcesz wypromować: "{ auction_title }"</h2>
          <h3>Możesz wypromować jeszcze: { promos } szt.</h3>
        </div>

        <div className="buttons">
          <div
            className="button clickable"
            onClick={ this.cancel.bind(this) }>
            Nie, dziękuję
          </div>

          <BuyButton
            text="Wypromuj"
            callback={ this.promote.bind(this) }
          />
        </div>
      </div>
    );
  }
}

PromoteFromCredits = withRouter(PromoteFromCredits);
export { PromoteFromCredits, promotionActive };

function startWithSlash(string) {
  if (string.startsWith('/')) return string;
  else return '/' + string;
}

function countCost(state, promotion) {
  let { buyPacketsData = {}, buyPromosData = {}} = state;
  return ( promotion ? 0 : valueOrZero( buyPacketsData.cost )) + valueOrZero( buyPromosData.cost );
}

function promotionActive({ discountFinalDate }) {
  return Date.now() <= new Date(discountFinalDate).getTime();
}
