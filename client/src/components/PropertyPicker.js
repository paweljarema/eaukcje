import React, { Component } from 'react';
import { isNotEmpty } from './CategoryPicker';
import { changeBrackets } from './auctions/functions';
import Observer, { PROPERTY_PICKER_HINT_EVENT } from '../helpers/eventHelper';
import './Categories.css';

const PROPERTY_PATH = 'path_for_property';

class Input extends Component {
	constructor(props) {
		super(props);

		this.state = { hints: [], value: '' };

		this.setValue  = this.setValue.bind(this);
		this.showHints = this.showHints.bind(this);
		// this.filterHints = this.filterHints.bind(this);
		this.hideHints = this.hideHints.bind(this);
		this.stopInput = this.stopInput.bind(this);
		this.onChange = this.onChange.bind(this);
		this.checkAll = this.checkAll.bind(this);
		this.callback = this.callback.bind(this);
		this.setValueFromProps = this.setValueFromProps.bind(this);

		this.hintObserver = new Observer();
		this.watch = this.hintObserver.watchEvent(PROPERTY_PICKER_HINT_EVENT, this.hideHints);
	}

	setValueFromProps() {
		if (this.props.update && this.props.propertyData) {
			let
				{ originalName, type } = this.props,
				value = '';

			if (type === 'Range') {
				let
					set = this.props.propertyData.int_properties,
					index = set.findIndex(prop => prop.name === originalName);

				if (index !== -1)
					value = this.props.propertyData.int_properties[index].value;

			} else {
				let
					set = this.props.propertyData.properties,
					index = set.findIndex(prop => prop.name === originalName);

				if (index !== -1) {
					value = this.props.propertyData.properties[index].value;
					if (Array.isArray(value)) value = value.join(', ');
				}
			}

			this.setState({ value });
		}
	}

	componentDidMount() {
		window.addEventListener('click', this.hideHints);
		this.setValueFromProps();
	}

	componentWillUnmount() {
		window.removeEventListener('click', this.hideHints);
		this.hintObserver.unregister(PROPERTY_PICKER_HINT_EVENT, this.watch);
		this.hintObserver = null;
	}

	componentWillReceiveProps(props) {
		this.setValueFromProps();
	}

	checkAll(e) {
		if (e) e.stopPropagation();

		this.setState(({ value }) => ({
			value: !value ? this.props.values
				.filter(val => val)
				.sort()
				.join(', ') : ''
		}), this.callback);
	}

	setValue(newValue, isSet, e) {
		if (e) e.stopPropagation();

		let isRange = this.props.type === 'Range';
		if (isRange) {
			this.setState({ value: newValue });
			return;
		}

		if (this.inputRef) {
			let callback = this.props.callback || null;

			if (isSet) {
				this.setState(({ value }) => ({
					value: value
						.split(', ')
						.filter(val => val !== newValue)
						.join(', ')
				}), this.callback);
			} else {
				this.setState(({ value }) => ({
					value: [newValue]
						.concat(
							value
								.split(', ')
								.filter(val => val)
								.sort()
						)
						.join(', ')
				}), this.callback)
			}
		}
	}

	callback() {
		if (this.props.callback) {
			this.props.callback(this.props.name, this.state.value);
		}
	}

	showHints(e) {
		if (e) e.stopPropagation();

		if (this.state.hints.length)
			this.hideHints();
		else {
			this.hintObserver.trigger(PROPERTY_PICKER_HINT_EVENT);
			if (this.props.values)
				this.setState({ hints: this.props.values.filter(val => val) });
		}
	}

	// filterHints(e) {
	// 	if (e) e.stopPropagation();
	//
	// 	if (this.inputRef) {
	// 		const
	// 			value = this.inputRef.value,
	// 			regex = new RegExp(value, 'i');
	//
	// 		this.setState({ hints: this.props.values.filter(val => val && regex.test(val)) });
	// 	}
	// }

	stopInput(e) {
		e.preventDefault();


	}

	hideHints() {
		this.setState({ hints: [] });
	}

	getValue(initialPropData, propName) {
		if (isNotEmpty(initialPropData)) {
			const prop = initialPropData.filter(prop => prop.name === propName.slice(9).replace('%', ''))[0];
			return prop ? prop.value : null;
		}

		return null;
	};

	onChange(e) {
		let
			range = this.props.type === 'Range',

			{ name, value } = e.target;

		if (range) {
 		 	if (value < 0) value = 0;
 		 	if (String(value).includes('e')) value = 0;
 		 	if (value > 9999999) value = 9999999;
		}

		this.setState({ value });
	}

	render() {
		const
			{ _id, name, originalName, path, type, unit, placeholder, update, propertyData } = this.props,
			{ value, hints, open } = this.state;

		const
			symbol = unit ? unit : '',
			initialPropData = isNotEmpty(propertyData) ? propertyData.properties.concat(propertyData.int_properties) : null,

			range = type === 'Range';

		return (
			<div className="input-input">
				<span className="unit-span">
					<input
						name={ name }
						placeholder={ placeholder }
						ref={ (e) => this.inputRef = e }
						onClick={ this.showHints }
						defaultValue={ this.getValue(initialPropData, name) }
						autoComplete="off"

						type={ range ? 'number' : 'text' }
						onChange={ this.onChange }
						readOnly={ range ? false : 'readonly' }
						value={ value  }
					/>

					{
						symbol && <span className="unit">{ symbol }</span>
					}
				</span>
				{
					isNotEmpty(hints) && (<ul className="input-hints">
						{
							!range && (
								<li
									className="hint check-all"
									onClick={ this.checkAll }>

										-- { (value ? 'odznacz' : 'zaznacz') } wszystko --
								</li>
							)
						}

						{
							hints.map((hint, i) => {
								let isSet = String(this.state.value).includes(hint);

								return (
									<li
										key={ hint }
										className="hint"
										className={ "hint checkable" + `${ isSet ? ' set' : '' }`}
										onClick={ (e) => this.setValue(hint, isSet, e) }>

											{ hint + ' ' + symbol }

									</li>
								);
							})
						}
					</ul>)
				}
				{
					(Boolean(value) || value === 0) && <input type="hidden" name={ PROPERTY_PATH + '_' + name } value={ [path, originalName].join('.') } />
				}
			</div>
		);
	}
}

class Property extends Component {
	render() {
		const
			{ property, callback, marka, update, propertyData } = this.props,
			{ name, type, unit, icon, values, conditional_values, path } = property;

		if (!property) return null;

		const alias = type === 'Range' ? '%' + name : name;

		return (
			<span className="property">
				<label htmlFor={ 'property_' + name }>{ name }</label>
				<Input
					id={ 'property_' + name }
					originalName={ name }
					name={ 'property_' + changeBrackets(alias) }
					type={ type }
					unit={ unit }
					callback={ callback }
					values={ marka ? conditional_values[marka] : values }
					path={ path }
					update={ update }
					propertyData={ propertyData }
				/>
			</span>
		);
	}
}

class PropertyPicker extends Component {
	constructor(props) {
		super(props);

		this.state = { marka: '' };

		this.callback = this.callback.bind(this);
	}

	callback(name, value) {
		// keep track of conditional props
		if (name.endsWith('Marka')) this.setState({ marka: value });
	}

	render() {
		const { properties, callback, update, propertyData } = this.props;

		if (!properties) return null;

		const propProps = {
			update,
			propertyData
		};

		return (
			<div className="Properties">
				{
					properties.map((prop, index) => {
						if (prop.name === 'Cena')
							return null;

						if (prop.name === 'Marka')
							return <Property key={ "prop_" + index } property={ prop } callback={ this.callback } {...propProps } />;

						if (prop.name === 'Model')
							return <Property key={ "prop_" + index } property={ prop } marka={ this.state.marka } {...propProps } />;

						return <Property key={ "prop_" + index } property={ prop } {...propProps } />;
					})
				}
			</div>
		);
	}
}

export default PropertyPicker;
