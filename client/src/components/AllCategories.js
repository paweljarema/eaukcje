import React from 'react';
import { Link } from 'react-router-dom';
import { makeCatLinkRelative, removeInvalidChars } from '../functions/links';

import './AllCategories.css';

export default function({ categories, user }) {

  return (
    <div className="all-categories">
      <h1>Wszystkie kategorie</h1>
      {
        categoryLinks(categories)
      }
    </div>
  );
};

function categoryLinks(categories, index = []) {
  if (!categories) return null;

  return (
    <ul className="all-categories__list">
      {
        categories.map(cat => {
          const
            { name } = cat,

            isMain = !index.length,
            path = index.concat(name),
            link = makeCatLinkRelative(path);

          return (
            <li className="all-categories__item" key={ name }>
              {
                isMain && <HashLinkMarker name={ name } />
              }

              <Link className="all-categories__link" to={ link }>
                <Heading level={ 2 + index.length }>
                  { name }
                </Heading>
              </Link>
              { cat.subcategories && categoryLinks(cat.subcategories, path) }
            </li>
          );
        })
      }
    </ul>
  );
}

function Heading({ level, children }) {
  switch (level) {
    case 1:
      return <h1>{ children }</h1>;
    case 2:
      return <h2>{ children }</h2>;
    case 3:
      return <h3>{ children }</h3>;
    case 4:
      return <h4>{ children }</h4>;
    case 5:
      return <h5>{ children }</h5>;
  }

  return <h6>{ children }</h6>;
}

function HashLinkMarker({ name }) {
  const style = { position: 'absolute', marginTop: -100 };

  return <div id={ removeInvalidChars(name) } style={ style }></div>;
}
