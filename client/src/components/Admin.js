import React, { Component } from 'react';
import { Link, Switch, Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import * as adminActions from '../actions/adminActions';
import * as techBreakActions from '../actions/techBreakActions';
import * as categoryActions from '../actions/categoryActions';

import AdminLinks from './admin/Links';
import Categories from './admin/Categories';
import UserList from './admin/UserList';
import AuctionList from './admin/AuctionList';
import Provision from './admin/Provision';
import InvoiceSummary from './admin/InvoiceSummary';
import BulkMail from './admin/BulkMail';
import TechBreak from './admin/TechBreak';
import Settings from './admin/Settings';
import Violations from './admin/Violations';
import Seo from './admin/Seo';

import Modal from './Modal';
import Progress from './Progress';
import NoDocuments from './admin/Empty';

import { Pagination } from './Pagination';
import { applyToTable } from '../functions/table';

import './Admin.css';


class AdminPanel extends Component {
	constructor(props) {
		super(props);

		this.state = { login: '', password: '' };

		this.handleInput = this.handleInput.bind(this);
		this.authAdmin = this.authAdmin.bind(this);
	}

	componentDidMount() {
		this.props.authAdmin();
	}

	handleInput(e) {
		const { name, value } = e.target;
		this.setState({ [name]: value });
	}

	authAdmin() {
		this.props.authAdmin(this.state);
	}

	render() {
		const { admin, categories } = this.props;
		const { login, password } = this.state;


		return (
			<div className="Admin">
				{
					admin === false
					?
					<Modal
						title={<span><i className="material-icons">verified_user</i>Zaloguj się jako administrator</span>}
						actions={<button className="standard-button" onClick={this.authAdmin}><i className="material-icons">arrow_forward</i>Zaloguj</button>}
						open={true}
						close={ () => window.location.href='/' }
					>
						<form className="admin-login">
							<input name="login" type="text" placeholder="login" onChange={this.handleInput} />
							<input name="password" type="password" placeholder="hasło" onChange={this.handleInput} />
						</form>
					</Modal>
					:
					(
						<div className="AdminPanel">
							<Switch>
								<AdminLinks />
							</Switch>
							<div style={{ width: '100%', position: 'relative' }}>
								<Route path="/admin/seo" render={ props => <Seo { ...props } admin={ admin } categories={ categories } /> } />
								<Route path="/admin/prowizja" component={ Settings } />
								<Route path="/admin/faktury" render={ (props)  => <InvoiceSummary { ...props } admin={ admin } /> } />
								<Route path="/admin/kategorie" component={ null } />
								<Route path="/admin/uzytkownicy" component={ UserList } />
								<Route path="/admin/aukcje" component={ AuctionList } />
								<Route path="/admin/naruszenia" component={ Violations } />
								<Route path="/admin/baza-mailingowa" component={ BulkMail } />
							</div>
						</div>
					)
				}
			</div>
		);
	}
}

// Categories

function mapAdminStateToProps({ admin }) {
	return { admin };
}

function mapAdminAndTechBreakStateToProps({ admin, tech_break }) {
	return { admin, tech_break };
}

function mapAdminAndDocumentStateToProps({ admin, documents }) {
	return { admin, documents };
}

AdminPanel = connect(mapAdminStateToProps, adminActions)(AdminPanel);

export {
	AdminPanel,
	TechBreak,
	mapAdminStateToProps,
	mapAdminAndDocumentStateToProps,
	mapAdminAndTechBreakStateToProps
};
