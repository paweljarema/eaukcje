import React, { Component } from 'react';
import Carousel from './carousel/GenericCarousel';
import { withRouter } from 'react-router-dom';
import { frontPageCategories as categories } from '../constants/categories';
import { stripPolish } from '../helpers/charHelper';

import { AUCTIONS_PATH, removeInvalidChars } from '../functions/links';

const
	catResource = (cat) => '/assets/icons/new/' + stripPolish(cat.toLowerCase().replace(/\s+/g, '_')) + '.svg',
	assetNames = categories.map(cat => catResource(cat));

class CategoryLink extends Component {
	navigate() {
		const { to, categoryCallback } = this.props;

        new Promise((resolve, reject) => {
					let categories = [];
					if (to) categories.push(to);

     			resolve(
            // categoryCallback({ categories, time: new Date().getTime() })
          );
        }).then(this.props.history.push(['', AUCTIONS_PATH, removeInvalidChars(to)].join('/')));
	}

	render() {
		const { squeeze } = this.props;
		return (
			<div
				className="clickable"
				onClick={ this.navigate.bind(this) }
			>

				{ this.props.children }
			</div>
		);
	}
}

function FrontPageCategories({ onMobile, windowWidth, categoryCallback }) {
	function sizeForWindowWidth(w) {
		if (w <= 900) return 3;
		if (w <= 2000) return 7;
		return 20;
	}

	let
		size = sizeForWindowWidth(windowWidth),
		items = [];

	for (let i = 0, l = assetNames.length; i < l; i += size)
		items.push(
			assetNames
				.slice(i, i + size)
				.map((asset, j) => {
					const name = categories[i + j];

					return (
						<CategoryLink
							key={ 'cat_' + j }
							to={ name }
							categoryCallback={ categoryCallback }>

							<figure className="generic-carousel__presentation">
								<img className="generic-carousel__img" src={ asset } alt={ name } />
								<figcaption className="generic-carousel__caption">
									{ name }
								</figcaption>
							</figure>

						</CategoryLink>
					)
				})
		);

	return (
		<Carousel
			className="carousel__frontpage-categories"
			items={ items }
		/>
	);
}

CategoryLink = withRouter(CategoryLink);
export { catResource };
export default FrontPageCategories;
