import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import { HashLink } from 'react-router-hash-link';
import Logo from './Logo';

import { AUCTIONS_PATH, removeInvalidChars } from '../functions/links';
import { arrayIsNotEmpty } from '../functions/array';
import { isNotEmpty } from './auctions/functions';
import { ALL_CATEGORIES_PATH } from '../constants/urls';

const SHOW_MORE_TEXT = 'Zobacz wszystkie ...';

class CategoryLink extends Component {
    constructor(props) {
        super(props);
        this.navigate = this.navigate.bind(this);
    }

    navigate(category, subcategory) {
        const { categoryCallback } = this.props;
        let categories = [];

        if (category) categories.push(category);
        if (subcategory) categories.push(subcategory);

        new Promise((resolve, reject) => {
            resolve(
              // categoryCallback({ categories, time: new Date().getTime() })
            );
        }).then(this.props.navigate);
    }

    render() {
        const category = this.props.data;
        const subcategories = category.subcategories;
        const categoryLink = ['', AUCTIONS_PATH, removeInvalidChars(category.name)].join('/');

        // <a onClick={ () => this.navigate(category.name, null) }>
        //   <strong >{ category.name }</strong>
        // </a>

        // <a
        //   key={ subcategory.name }
        //   onClick={ () => this.navigate(category.name, subcategory.name) }
        // >

        return (
            <div className="categories">
                <Link to={ categoryLink }>
                  <strong >{ category.name }</strong>
                </Link>

                {
                    arrayIsNotEmpty(subcategories) && subcategories.slice(0, 5)
                      .map(subcategory => (
                        <Link key={ subcategory.name } to={ [categoryLink, removeInvalidChars(subcategory.name)].join('/') } >
                          { subcategory.name }
                        </Link>
                      ))
                }

                {
                  // subcategories.length > 5 && (
                    <div style={{ marginTop: 10 }}>
                      <HashLink
                        to={ ALL_CATEGORIES_PATH + '#' + removeInvalidChars(category.name) }
                        style={{ fontStyle: 'italic' }}
                      >
                        { SHOW_MORE_TEXT }

                      </HashLink>
                    </div>
                  // )
                }
            </div>
        );
    }
}

class CategoryLinks extends Component {
    constructor(props) {
        super(props);
        this.state = { sections: null };
    }

    navigate() {
        this.props.history.push('/aukcje');
        window.scroll({ top: 0, behavior: 'smooth' });
    }

    render() {
        const
            { categories, categoryCallback } = this.props;

        if (categories === null || categories === false)
            return null;

        let sections = this.state.sections;

        if (!sections) {
            sections = [];
            let
                index = 0,
                notEmpty = true;

            do {
                const
                    section = categories.slice(index, index + 6);
                    notEmpty = isNotEmpty(section);

                if (notEmpty) sections.push(section);
                index += 6;
            } while(notEmpty);

            this.setState({ sections });
        }



        return (
            <div className="category-links">
                {
                    sections.map((section, i) => (
                        <div key={ 'row_' + i } className="row">

                            {
                                section.map((cat, i) => (
                                    <div key={ cat.name } className="column">
                                        <CategoryLink data={ cat } categoryCallback={ categoryCallback } navigate={ this.navigate.bind(this) } />
                                    </div>
                                ))
                            }
                        </div>
                    ))
                }


            </div>
        )
    }
}

// Bez logotypu:
//
// <div className="row">
//   <div className="column">
//       <Logo />
//   </div>
// </div>

// {
//     section.length < 6 && (
//         <div className="column">
//             <Logo />
//         </div>
//     )
// }

class FooterLinks extends Component {
    render() {
        return (
            <div className="footer-links">
                <Link to="/regulamin">Regulamin</Link>
                <Link to="/kontakt">Kontakt</Link>
            </div>
        );
    }
}

class FooterBar extends Component {
    render() {
        return (
            <div className="footer-bar">
                <div>Copyright © 2019. Wszelkie prawa zastrzeżone.</div>
                <div>Właścicielem portalu jest Polmarket</div>
                <div>Powered by ADAWARDS</div>
            </div>
        );
    }
}

function mapCategoryStateToProps({ categories }) {
    return { categories };
}


CategoryLinks = connect(mapCategoryStateToProps)(withRouter(CategoryLinks));
export { CategoryLinks, FooterLinks, FooterBar };
