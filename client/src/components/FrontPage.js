import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import * as auctionActions from '../actions/auctionActions';

import FrontPageCategories from './FrontPageCategories';
import FrontPageInner from './FrontPageInner';
import AuctionTickers from './auctions/AuctionTickers';
import AuctionCarousel from './auctions/AuctionCarousel';
import Auction from './auctions/Auction';
import Progress from './Progress';

import { PROMOTED_AUCTIONS_PATH, NEW_AUCTIONS_PATH, AUCTIONS_PATH } from '../constants/urls';
import { isEmpty, isNotEmpty } from './auctions/functions';
import { siteLd } from '../jsonld/schemas';

import './FrontPage.css';

const modes = ['main', 'promoted', 'new'];

class FrontPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
          mode: 'main',
          initialized: false
        };

        this.toggleMode = this.toggleMode.bind(this);
    }

    componentDidMount() {
        this.props.fetchFrontPageAuctions()
          .then(() => this.setState({ initialized: true }));
    }

    shouldComponentUpdate(nextProps, nextState) {
      return nextState.initialized;
    }

    toggleMode(changeMode) {
        this.setState(({ mode }) => ({ mode: (mode === 'main' || mode !== changeMode ? changeMode : 'main') }));
    }

    render() {
        const
          { mode } = this.state,
          { categories, categoryCallback, onMobile, windowWidth, user } = this.props,

          inPromoted = mode === 'promoted',
          inNew = mode === 'new',
          empty = this.props.auctions && isEmpty(this.props.auctions.popular) && isEmpty(this.props.auctions.newest);

        return (
            <div className="FrontPage">
              { siteLd() }
              <div className="main-container">
                <h1 style={{ fontSize: 0, opacity: 0 }}>Najciekawsze aukcje na eaukcje.pl</h1>

                {
                    (!this.props.auctions || !this.props.auctions.popular) && <Progress />
                }
                {
                    this.props.auctions && <FrontPageCategories
                      categoryCallback={ categoryCallback }
                      onMobile={ onMobile }
                      windowWidth={ windowWidth }
                    />
                }
                {
                    empty && (
                        <div className="empty">
                            <p className="absolute-center">Nic tu jeszcze nie ma!<br/><a className="link clickable" href={ (user ? "/konto/aukcje/dodaj" : "/konto/zaloguj") }>Dodaj aukcję</a> i bądź pierwszy.</p>
                        </div>
                    )
                }
                {
                    this.props.auctions && isNotEmpty(this.props.auctions.popular) && (
                        <div className="most-popular">
                          <Link to={ PROMOTED_AUCTIONS_PATH }>
                            <h2 className="front-page__heading large-h clickable"><i className="material-icons">trending_up</i> Promowane</h2>
                          </Link>

                          {
                              inPromoted
                              ?
                              (
                                  <FrontPageInner mode={ mode } onMobile={ onMobile }/>
                              )
                              :
                              (
                                  <div>
                                      <div className="three-column">
                                          <div className="column">
                                              <Auction auction={this.props.auctions.popular[0]} />
                                           </div>
                                          <div className="column">
                                              <Auction auction={this.props.auctions.popular[1]} />
                                          </div>
                                          <div className="column">
                                              <Auction auction={this.props.auctions.popular[2]} />
                                          </div>
                                      </div>

                                      <div className="six-column">
                                          {
                                              this.props.auctions.popular.slice(3).map(
                                                (auction, index) => (
                                                  <div
                                                    key={auction.title + index}
                                                    className="column">

                                                      <Auction auction={auction} />
                                                  </div>
                                                )
                                              )
                                          }
                                      </div>
                                  </div>
                              )
                          }

                        </div>
                    )
                }
                {
                    this.props.auctions && isNotEmpty(this.props.auctions.newest) && (
                        <div className="new">
                          <Link to={ NEW_AUCTIONS_PATH }>
                            <h2 className="front-page__heading large-h clickable"><i className="material-icons">new_releases</i> Najnowsze</h2>
                          </Link>

                          {
                              inNew
                              ?
                              (
                                  <FrontPageInner mode={ mode } onMobile={ onMobile } />
                              )
                              :
                              (
                                  <AuctionCarousel auctions={ this.props.auctions.newest } windowWidth={ windowWidth } />
                              )
                          }

                        </div>
                    )
                }

              </div>

              <AuctionTickers categories={ categories } windowWidth={ windowWidth } />
            </div>
        )
    }
}

function mapAuctionsStateToProps({ auctions }) {
    return { auctions };
}

FrontPage = connect(mapAuctionsStateToProps, auctionActions)(FrontPage);
export default FrontPage;
