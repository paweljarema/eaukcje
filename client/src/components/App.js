import React, { Component } from 'react';
import { BrowserRouter, Route, Link, Switch, Redirect, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import * as userActions from '../actions/userActions';
import * as categoryActions from '../actions/categoryActions';
import * as flashActions from '../actions/flashActions';
import * as countActions from '../actions/statisticActions';
import * as techBreakActions from '../actions/techBreakActions';
import * as cookieActions from '../actions/cookieActions';
import * as settingsActions from '../actions/settingsActions';

import { Navi, MobileNavFragment, Breadcrumbs } from './Navigation';
import { AuctionList, MyAuctionList, FrontPage } from './Auctions';
import { RegistrationLanding, LoginLanding } from './Landing';
import { Settings, Delivery, ProfileLinks, MyOpinions, Invoices } from './Profile';
import { AdminPanel, TechBreak } from './Admin';
import { CategoryLinks, FooterLinks, FooterBar } from './Footer';
import { Page404 } from './Page404';
import { Helmet } from 'react-helmet';

import CreateUpdateAuction from './auctions/CreateUpdate';
import Contact from './Contact';
import Progress from './Progress';
import Ads from './Ads';
import AllCategories from './AllCategories';

import ProfileSendAndRateBuyer from './ProfileSendAndRateBuyer';
import BuyPackets from './BuyPackets';

import socketIOClient from 'socket.io-client';
import { getVisualWindowWidth } from '../functions/window';
import { thisWasRendered } from '../jsonld/schemas';

import NestedCategoryRoutes from '../routes/NestedCategoryRoutes';
import business from '../constants/business';

import {
  BUY_PACKETS_PATH,
  NEW_AUCTION_PATH,
  LOGIN_PATH,
  PROFILE_PATH,
  PROMOTED_AUCTIONS_PATH,
  NEW_AUCTIONS_PATH,
  AUCTIONS_PATH,
  MY_AUCTIONS_ENDED_PATH,
  MY_AUCTIONS_POST_AGAIN_PATH,
  ALL_CATEGORIES_PATH
} from '../constants/urls';

import './App.css';
import './Mobile.css';
import './Scrollbars.css';

class TopScroller extends Component {
   componentDidUpdate(prevProps, prevState) {
        if (this.props.location.pathname !== prevProps.location.pathname) {
            window.scrollTo(0,0);
        }
   }

   render() {
        return null;
   }
}

class CookieMessage extends Component {
    state = { initialized: false };

    componentDidMount() {
      this.props.fetchCookies()
        .then(() => this.setState({ initialized: true }));
    }

    render() {
        const { cookies } = this.props;

        if (this.state.initialized && !cookies) {
            return (
                <div className="cookie-message">
                    <div>
                        <img src="/assets/cookies.png" />
                    </div>
                    <p>
                        <h3>Ciasteczka</h3>
                        <p>Serwis używa ciasteczek do zbierania istotnych informacji o operacjach wykonywanych w serwisie. Włączenie obsługi ciasteczek ułatwia korzystanie z serwisu i jest wymagane. Wszystkie Państwa informacje są zaszyfrowane i zabezpieczone.</p>
                        <h3>Rodo</h3>
                        <p>25 maja 2018 roku zaczęło obowiązywać Rozporządzenie Parlamentu Europejskiego i Rady (UE) 2016/679 z dnia 27 kwietnia 2016 r. w sprawie ochrony osób fizycznych w związku z przetwarzaniem danych osobowych w sprawie swobodnego przepływu takich danych oraz uchylenia dyrektywy 95/46/WE (RODO). Uprzejmie informujemy, że administratorem Państwa danych osobowych jest firma Polmarket. Celem przetwarzania danych jest realizacja prawnych obowiązków administratora.</p>
                        <button className="standard-button" onClick={this.props.allowCookies}>Rozumiem i wyrażam zgodę</button>
                    </p>
                </div>
            );
        } else {
            return null;
        }

    }
}

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
        search_query: '',
        category_filter_data: {},
        endpoint: process.env.REACT_APP_CHAT_URL,
        socket: null,
        chatBox: null,
        windowWidth: window.innerWidth,

        seo: null,
        initialized: false
    };

    this.setQuery = this.setQuery.bind(this);
    this.callback = this.callback.bind(this);
    this.categoryFilterCallback = this.categoryFilterCallback.bind(this);
    this.onResize = this.onResize.bind(this);
  }

  onResize(e) {
    this.setState({ windowWidth: getVisualWindowWidth(window) });
  }

  setQuery(search_query) {
    this.setState({ search_query })
  }

  categoryFilterCallback(object) {
    this.setState({ category_filter_data: object });
  }

  componentWillReceiveProps(props) {
    if (props.flash) {
        setTimeout(this.props.clearMessage, 10000);
    }

    this.initializeSocket();
  }

  initializeSocket = (callback) => {
    if (this.props.user && !this.state.socket) {
      const { endpoint } = this.state;
      const socket = socketIOClient(endpoint);
      socket.on("handshake", data => {
          socket.emit("handshake", String(this.props.user._id));
      });

      this.setState({ socket }, callback);
    } else {
      if (callback) callback();
    }
  }

  asyncInitializeSocket = () => new Promise((resolve, reject) => {
    this.initializeSocket(resolve);
  })

  componentDidMount() {
    fetch('/seo')
      .then(res => res.json())
      .then(json => this.setState(
        { seo: json },
        () => {
          this.props.fetchCategories()
            .then(this.props.fetchSettings)
            .then(this.props.fetchTechBreak)
            .then(this.props.fetchUser)
            .then(this.asyncInitializeSocket)
            .then(this.props.fetchMessage)
            .then(() => this.setState({ initialized: true }))
        }
      ));

    window.addEventListener('resize', this.onResize);
  }

  shouldComponentUpdate(nextProps, nextState) {
    return nextState.initialized;
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.onResize);
  }

  callback(chatBox) {
    this.setState({ chatBox });
  }

  render() {
    const
        { socket, chatBox, category_filter_data, search_query, seo, initialized } = this.state,
        { user, flash, tech_break, categories, settings } = this.props;

    const
        message = flash !== null && flash !== false ? <div className={ "flash-message " + flash.type }>{ flash.message }</div> : null,
        windowWidth = this.state.windowWidth,
        onMobile = windowWidth <= 1350,

        appLoaded = Boolean(initialized); // add more conditions

    // thisWasRendered(this);
    // console.log(categories);

    const
      navProps = {
        socket: socket,
        callback: this.callback,
        query: search_query,
        setQuery: this.setQuery,
        categoryCallback: this.categoryFilterCallback,
        categoryData: category_filter_data,
        onMobile: onMobile
      },

      searchProps = {
        user: user,
        query: search_query,
        setQuery: this.setQuery,
        categories: categories,
        categoryData: category_filter_data,
        categoryCallback: this.categoryFilterCallback,
        windowWidth: windowWidth
      },

      auctionProps = {
        socket: socket,
      };

    if (appLoaded) {
       if (tech_break.techbreak === false) return (
          <div className="App">
            {
                message
            }
            {
              seo && (
                <Helmet>
                  <title>{ seo.title || 'SET_IN_ADMIN' }</title>
                  <meta name="description" content={ seo.description || 'SET_IN_ADMIN' } />

                  <meta property="og:type" content="website" />
                  <meta property="og:title" content={ seo.title || business.name } />
									<meta property="og:description" content={ seo.description || 'SET_IN_ADMIN' } />
									<meta property="og:url" content={ business.host } />
                  <meta property="og:image" content={ `${ business.host }/assets/eaukcje.jpg` } />
                  <meta property="og:image:width" content="1417" />
                  <meta property="og:image:height" content="1417" />

                  <link rel="canonical" href={ business.host } />
                </Helmet>
              )
            }
            <BrowserRouter>
                <div>
                    <header className="App-header">
                        <Navi { ...navProps } />
                        <Breadcrumbs />
                    </header>

                    <div>
                      <Route exact path="/" render={ props => <FrontPage {...props} categories={ categories } categoryCallback={ this.categoryFilterCallback } categoryData={ category_filter_data } windowWidth={ windowWidth } onMobile={ onMobile } user={ user } /> } />
                    </div>
                    <div className="main-container">
                      <TopScroller />
                      <CookieMessage />

                      <Route
                        path={ `(${ AUCTIONS_PATH }|${ PROMOTED_AUCTIONS_PATH }|${ NEW_AUCTIONS_PATH })` }
                        render={ props => (
                          <NestedCategoryRoutes
                            { ...props }
                            searchProps={ searchProps }
                            auctionProps={ auctionProps }
                            categories={ categories }
                          />
                        )}
                      />
                      <Route exact path="/edytuj-aukcje" render={ props => <Redirect to="/" /> } />
                      <Route path="/konto/zarejestruj" component={ RegistrationLanding } />
                      <Route path={ LOGIN_PATH } component={ LoginLanding } />
                      <Route path="/admin" render={ props => <AdminPanel { ...props } categories={ categories } /> } />
                      <Route path="/(pomoc|kontakt)" component={ Contact } />
                      <Route path={ BUY_PACKETS_PATH } render={ props => <BuyPackets { ...props } user={ user } /> } />
                      <Route path={ ALL_CATEGORIES_PATH } render={ props => <AllCategories {...props} categories={ categories } user={ user } /> } />
                      {
                        user && <>
                          <Route exact path="/moje-aukcje" render={ (props) => <MyAuctionList {...props} mode='current_auctions' settings={ settings } /> } />
                          <Route exact path={ MY_AUCTIONS_ENDED_PATH } render={ (props) => <MyAuctionList {...props} mode='ended_auctions' /> } />
                          <Route exact path="/moje-licytacje/" render={ (props) => <MyAuctionList {...props} mode='current_bids' /> } />
                          <Route exact path="/moje-licytacje/zakonczone" render={ (props) => <MyAuctionList {...props} mode='ended_bids' /> } />
                          <Route exact path="/polubione-aukcje" render={ (props) => <MyAuctionList {...props} mode='liked' /> } />
                          <Route path={ PROFILE_PATH } component={ Settings } />
                          <Route path="/konto/opinie" component={ MyOpinions } />
                          <Route path="/konto/faktury" component={ Invoices } />
                          <Route path={ NEW_AUCTION_PATH } render={ props => <CreateUpdateAuction { ...props } settings={ settings } /> } />
                          <Route path="/edytuj-aukcje/:title/:id" render={ (props) => <CreateUpdateAuction {...props} update={true} settings={ settings } /> } />
                          <Route path={ MY_AUCTIONS_POST_AGAIN_PATH } render={ (props) => <CreateUpdateAuction {...props} update={true} postAgain={true} settings={ settings } /> } />
                          <Route path="/konto/aukcje/dostawa" component={ Delivery } />
                          <Route path="/konto/wyslij-i-ocen" render={ (props) => <ProfileSendAndRateBuyer { ...props } user={ user } /> } />
                          <Route path="/konto/moje-faktury" render={ (props) => <Invoices { ...props } user={ user } /> } />
                        </>
                      }

                      <Route exact path="/404" component={ Page404 } />
                    </div>

                    <div className="Chat">{ chatBox }</div>

                    {
                        true && <Ads windowWidth={ windowWidth } />
                    }

                    <footer>
                        <CategoryLinks categoryCallback={ this.categoryFilterCallback } />
                        <FooterLinks />
                        <FooterBar />
                    </footer>

                </div>
            </BrowserRouter>
          </div>
        );
        else if (appLoaded && tech_break.techbreak === true) return (
          <div className="App">
              {
                  message
              }
              <BrowserRouter>
                  <div className="main-container">
                      <Route exact path="/" component={ TechBreak } />
                      <Route path="/admin" component={ AdminPanel } />
                  </div>
              </BrowserRouter>

          </div>
        );
    } else {
        return <Progress />;
    }
  }
}

function mapUserAndFlashStatesToProps({ user, flash }) {
    return { user, flash };
}

function mapUserFlashTechBreakAndCookiesStatesToProps({ user, flash, tech_break, categories, settings }) {
    return { user, flash, tech_break, categories, settings };
}

function mapCookiesToProps({ cookies }) {
  return { cookies };
}

function mapFlashToProps({ flash }) {
    return { flash };
}

function aggregateProps({ user, categories, flash }) {
    return { user, categories, flash };
}

function mapAuctionCountStateToProps({ auction_count }) {
    return { auction_count };
}

TopScroller = withRouter(TopScroller);
CookieMessage = connect(mapCookiesToProps, cookieActions)(CookieMessage);

export default connect(mapUserFlashTechBreakAndCookiesStatesToProps, { ...userActions, ...categoryActions, ...flashActions, ...techBreakActions, ...settingsActions })(App);
