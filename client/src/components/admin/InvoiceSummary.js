import React from 'react';
import InvoiceReport from './InvoiceReport';
import Invoices from './Invoices';
import MonthPicker from './MonthPicker';
import BatchSymfonyExport from './BatchSymfonyExport';

export default class extends React.Component {
  constructor(props) {
    super(props);

    let month = new Date().getMonth();

    this.state = {
      from: month,
      to: month
    };
  }

  onChange(e) {
    const { value } = e.target;

    this.setState({ from: value, to: value });
  }

  render() {
    const props = { ...this.props, ...this.state };

    return (
      <>
        <MonthPicker onChange={ this.onChange.bind(this) } />
        <BatchSymfonyExport { ...props } />
        <InvoiceReport { ...props } />
        <Invoices { ...props } />
      </>
    )
  }
};
