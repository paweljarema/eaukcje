import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

import { connect } from 'react-redux';
import { mapAdminAndTechBreakStateToProps } from '../Admin';
import * as adminActions from '../../actions/adminActions';
import * as techBreakActions from '../../actions/techBreakActions';

export default connect(mapAdminAndTechBreakStateToProps, {...adminActions, ...techBreakActions})(
  class extends Component {
  	constructor(props) {
  		super(props);
  		this.toggleTechBreak = this.toggleTechBreak.bind(this);
  		this.logoutAdmin = this.logoutAdmin.bind(this);
  	}

  	toggleTechBreak() {
  		const admin_id = this.props.admin._id;
  		this.props.techBreak({ admin_id });
  	}

  	logoutAdmin() {
  		this.props.logoutAdmin();
  	}

  	render() {
  		const { tech_break } = this.props;

  		return (
  			<div className="links">
  				<h2><i className="material-icons">verified_user</i>Administracja</h2>
          <NavLink to="/admin/seo">Seo</NavLink>
  				<NavLink to="/admin/prowizja" activeClassName="active">Opłaty</NavLink>
          <NavLink to="/admin/faktury">Faktury</NavLink>

  				<NavLink to="/admin/uzytkownicy" activeClassName="active">Użytkownicy</NavLink>
  				<NavLink to="/admin/aukcje">Aukcje</NavLink>
          <NavLink to="/admin/naruszenia">Naruszenia</NavLink>
  				<NavLink to="/admin/baza-mailingowa">Baza mailingowa</NavLink>

          <a onClick={this.logoutAdmin}>Wyloguj</a>

  				{
  					tech_break && <button className="standard-button" onClick={this.toggleTechBreak}>{( tech_break.techbreak ? <i className="material-icons">power_off</i> : <i className="material-icons">power</i>)}{( tech_break.techbreak ? 'Odwołaj przerwę techniczną' : 'Zarządź przerwę techniczną')}</button>
  				}
  			</div>
  		);
  	}
  }
);

// <NavLink to="/admin/kategorie" activeClassName="active">Kategorie</NavLink>
