import React from 'react';

export default () => (
	<div className="absolute-center">
		<p className="transparent"><i className="material-icons" style={{marginRight: 10}}>filter_none</i>Na razie nic tu nie ma</p>
	</div>
);
