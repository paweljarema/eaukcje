import React from 'react';
import { monthPl } from '../../functions/date';
import { invoiceSymfonyBatchExportLink } from '../../functions/links';

export default function({ admin, from, to }) {
  if (!admin) return null;

  let
    monthNow = new Date().getMonth(),
    monthName = monthPl(monthNow),

    tooEarly = to >= monthNow;

  return (
    <a
      style={{ float: 'right', marginTop: 17 }}
      href={ invoiceSymfonyBatchExportLink(admin, from, to, monthName) }
      onClick={ (e) => isTooEarly(e, tooEarly) }>

      <div className="symfony-batch-export-button">
        <i style={{ marginRight: 6 }} className="material-icons">save_alt</i>
        Eksportuj { monthName } do Symfony
      </div>
    </a>
  );
};

function isTooEarly(e, tooEarly) {
  if (tooEarly) {
    let confirm = window.confirm('Okres rozliczeniowy tego miesiąca nie zakończył się. Czy mimo to chcesz eksportować faktury ?');

    if (!confirm && e) e.preventDefault()
  }
}
