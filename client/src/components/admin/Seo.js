import React from 'react';
import { Redirect } from 'react-router-dom';
import Progress from '../Progress';
import NoDocuments from './Empty';

import { CATEGORY_META_PROPS, CATEGORY_META_LABELS } from '../../constants/seo';

import axios from 'axios';

import './Seo.css';

//
// change here to add more fields
//

const
  SEO_URL = '/seo',
  FIELDS = {
    title: {
      type: 'text',
      label: 'Tytuł strony',
      length: { max: 60, min: 10 }
    },

    description: {
      type: 'textarea',
      label: 'Opis strony',
      length: { max: 160, min: 30 }
    }
  };

//
// end of change here
//

class EditCategorySeo extends React.Component {
  state = { isOpen: false };

  open() {
    this.setState(({ isOpen }) => ({ isOpen: !isOpen }));
  }

  render() {
    const
      { i, category, parents, dataSource, callback } = this.props,
      { isOpen } = this.state,

      hasChildren = Boolean(category.subcategories),

      activeClass = isOpen ? ' active' : '';

    return (
      <div className={ `edit-category-seo${ activeClass }` }>
        <h2 className={ `clickable${ activeClass }` } onClick={ this.open.bind(this) }>
          { category.name }
          <i className="material-icons">
            { ( isOpen ? 'expand_more' : 'chevron_right' ) }
          </i>
        </h2>
        <span>Ilość aktywnych aukcji: { category.activeAuctions }</span>
        {
          isOpen && (
            <div className="inner">
              {
                CATEGORY_META_PROPS.map((prop, i) => {
                  let name = parents.concat([ category.name, prop ]).join('.');
                  return (
                    <div key={ name } className="seo-input">
                      <label for={ name }>{ CATEGORY_META_LABELS[ i ] } ({ prop })</label>
                      <textarea id={ name } name={ name } onChange={ callback } value={ dataSource[name] || '' } />
                    </div>
                  )
                })
              }

              {
                hasChildren && (
                  <>
                    <CategoryIterator
                      categories={ category.subcategories }
                      parents={ parents.concat(category.name) }
                      callback={ callback }
                      dataSource={ dataSource }
                    />
                  </>
                )
              }
            </div>
          )
        }

      </div>
    );
  }
}

function CategoryIterator({ categories, parents, dataSource, callback }) {
  return (
    <div className="editable-category-list">
      {
        categories.map((cat, i ) => {
          return (
            <EditCategorySeo
              i={ i }
              key={ cat.name }
              category={ cat }
              parents={ parents }
              callback={ callback }
              dataSource={ dataSource }
            />

          );
        })
      }
    </div>
  );
}

export default class extends React.Component {
  constructor(props) {
    super(props);

    this.state = { loading: true };

    this.load = this.load.bind(this);
    this.save = this.save.bind(this);
    this.onChange = this.onChange.bind(this);
    this.pressSave = this.pressSave.bind(this);
    this.handleMetaInput = this.handleMetaInput.bind(this);
  }

  componentDidMount() {
    this.load();
  }

  onChange(e) {
    let { name, value } = e.target;
    this.setState({ [name]: value });
  }

  load() {
    axios
      .get(SEO_URL)
      .then(({ data }) => {
        this.setState({ ...consumeData(data), loading: false });
      });
  }

  save() {
    axios
      .post(SEO_URL, {
        ...consumeData(this.state),
        admin_id: this.props.admin._id
      })
      .then(({ data }) => {
        if (data) alert('Zapisano dane.');
      });
  }

  pressSave() {
    if (allIsValid())
      this.save();
    else
      alert('Przed zapisem wprowadź poprawne informacje');
  }

  handleMetaInput(e) {
    const
      MAX = 640,
      { name, value = '' } = e.target;

    this.setState({ [name]: value ? value.slice(0, MAX) : undefined })
  }

  render() {
    const
      { admin, categories } = this.props,
      { loading } = this.state,

      data = consumeData(this.state);

    if (!admin) return <Redirect to="/admin" />;
    if (loading) return <Progress />;

    return (
      <div className="AdminSeo">
        <h3>Zasadnicze</h3>
        {
          renderInputsForFields({
            data: data,
            onchange: this.onChange
          })
        }

        <h3>Seo kategorii</h3>

        <CategoryIterator
          categories={ categories }
          parents={ [] }
          callback={ this.handleMetaInput }
          dataSource={ this.state }
        />

        <div className="button-flex-row">
          <div className="seo-save button clickable" onClick={ this.pressSave }>
            Zapisz
          </div>
        </div>
      </div>
    )
  }
};

function consumeData(data) {
  const EXCLUDE = ['loading'];

  let result = {};
  for (let key in data) {
      if (EXCLUDE.includes(key))
        continue;

      if (data[ key ] || data[ key ] !== 0)
        result[ key ] = data[ key ];
  }

  return result;
}

// function consumeData(data) {
//   let result = {};
//   for (let key in FIELDS)
//     if (data[ key ])
//       result[ key ] = data[ key ];
//
//   return result;
// }

function renderInputsForFields({ data, onchange }) {
  let result = [];

  for (let key in FIELDS)
    result.push(
      componentFor({
        fieldname: key,
        field: FIELDS[ key ],
        value: data[ key ] || '',
        onchange: onchange
      })
    );

  return result;
}

function componentFor({ fieldname, field, value, onchange }) {
  let
    props = {
      id: fieldname,
      name: fieldname,
      type: field.type,
      value: value,
      onChange: onchange
    },

    label = (
      <label htmlFor={ fieldname }>
        { field.label } ({ fieldname })
      </label>
    ),

    validationmessage = validationMessageFor({ fieldname, value }),

    input;


  switch(field.type) {

    case 'textarea':
      input = <textarea { ...props } />
      break;

    default:
      input = <input { ...props } />;
      break;
  }

  return (
    <div className={ "seo-input " + fieldname } key={ fieldname }>
      { label }
      { input }
      { validationmessage }
    </div>
  )
}

function validationMessageFor({ fieldname, value }) {
  let
    field = FIELDS[ fieldname ],
    message;

  if (field.length) {
    let
      currentLength = value.length,
      { max, min } = field.length;


    if (max && currentLength > max) message = `Maksymalnie ${ max } znaków.`;
    if (min && currentLength < min) message = `Przynajmniej ${ min } znaków.`;
  }

  if (message)
    return (
      <div className='seo-validation invalid'>
        { message }
      </div>
    )
  else
    return null;
}

function allIsValid() {
  if (document.querySelector('.invalid'))
    return false;
  else
    return true;
}
