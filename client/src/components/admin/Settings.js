import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import { connect } from 'react-redux';
import * as adminActions from '../../actions/adminActions';
import { mapAdminStateToProps } from '../Admin';

import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';

import './Settings.css';

import axios from 'axios';


moment.updateLocale('pl', {
   months: ['Styczeń', 'Luty', 'Marzec', 'Kwiecień', 'Maj', 'Czerwiec', 'Lipiec', 'Sierpień', 'Wrzesień', 'Październik', 'Listopad', 'Grudzień'],
   monthsShort: ['Sty', 'Lut', 'Mar', 'Kwi', 'Maj', 'Cze', 'Lip', 'Sie', 'Wrz', 'Paź', 'Lis', 'Gru'],
   weekdays: ['Poniedziałek', 'Wtorek', 'Środa', 'Czwartek', 'Piątek', 'Sobota', 'Niedziela'],
   weekdaysShort: ['Pon', 'Wto', 'Śro', 'Czw', 'Pią', 'Sob', 'Nie'],
   weekdaysMin: ['Pn', 'Wt', 'Śr', 'Cz', 'Pi', 'So', 'Ni']
});

class Packet extends React.Component {
  render() {
    const
      { id, index, packet, onChange } = this.props,
      fields = Object.keys(packet);

    return (
      <div className="packet">
        {
          fields
            .filter( key => !['_id'].includes(key) )
            .map((key, i) => {
              let
                fieldname = `${ id }_${ index }_${ key }`,
                fieldvalue = packet[ key ];


              return inputFor(fieldvalue, fieldname, onChange);
            })
        }
      </div>
    );
  }
}

class ListPackets extends React.Component {
  render() {
    const { title, packets } = this.props;

    return (
      <div className="admin-packet-cost">
        <h2>{ title }</h2>

        <div className="admin-packet-cost-wrapper">
          {
            packets.map((packet, i) => (
              <Packet
                key={ packet._id }
                index={ i }
                packet={ packet }
                { ...this.props }
              />
            ))
          }
        </div>
      </div>
    );
  }
}

export default connect(mapAdminStateToProps, adminActions)(
  class extends Component {
    constructor(props) {
      super(props);

      this.state = { loading: true };
      this.fetchSettings = this.fetchSettings.bind(this);
      this.onChange = this.onChange.bind(this);
      this.saveSettings = this.saveSettings.bind(this);
      this.handleDate = this.handleDate.bind(this);
    }

    componentDidMount() {
      this.fetchSettings();
    }

    fetchSettings() {
      axios
        .get('/settings')
        .then(({ data }) => this.setState({ settings: data, loading: false }));
    }

    onChange(e) {
      let
        { name, type, value, checked } = e.target,
        { settings } = this.state,

        chunks = name.split('_');

      switch(type) {
        case 'text': value = String(value); break;
        case 'number': value = Number(value); break;
        case 'checkbox': value = Boolean(checked); break;
      }

      settings[ chunks[0] ][ chunks[1] ][ chunks[2] ] = value;

      // if (type === 'checkbox') {
      //   if (value === true)
      //     settings[ chunks[0] ][ chunks[1] ].count = null;
      //   else
      //     settings[ chunks[0] ][ chunks[1] ].count = 60;
      // }

      this.setState({ settings });
    }

    saveSettings() {
      let { settings } = this.state;

      axios
        .post('/settings', settings)
        .then(({ data }) => alert(data));
    }

    handleDate(date) {
      let settings = this.state.settings;
      settings.discountFinalDate = date;

      this.setState({ settings });
    }

    render() {
      const
        { admin } = this.props,
        { settings, loading } = this.state;

      if (!admin) return <Redirect to={ admin } />;
      if (loading) return <div>Trwa wczytywanie treści...</div>;

      return (
        <div className="Settings">
          <ListPackets
            id="auctionPackets"
            title="Opłaty za dodawanie aukcji"
            onChange={ this.onChange }
            packets={ settings.auctionPackets } />

          <ListPackets
            id="promoPackets"
            title="Opłaty za promowanie aukcji"
            onChange={ this.onChange }
            packets={ settings.promoPackets } />

          <h2>Darmowe zamieszczanie ogłoszeń możliwe do:</h2>
          <DatePicker
            dateFormat="DD / MM / YYYY"
            locale="pl"
            selected={ moment(settings.discountFinalDate) }
            onChange={ this.handleDate }
            dropdownMode="select"
            showYearDropdown
          />
          <br/><br/>

          <div className="buttons">
            <div
              className="button clickable"
              onClick={ this.saveSettings }>
              Zapisz
            </div>
          </div>
        </div>
      );
    }
  }
);

function inputFor(fieldvalue, fieldname, onChange) {
  let
    type = typeof fieldvalue,
    props = {
      key: fieldname,
      name: fieldname,
      type: typeFor(type),
      onChange: onChange,
      [ (type === 'boolean' ? 'checked' : 'value') ]: fieldvalue,
    };

  return (
    <div>
      <label for={ fieldname }>{ labelFor(fieldname) }</label>
      <input { ...props } />
    </div>
  );
}

function typeFor(type) {
  if (type === 'number') return 'number';
  if (type === 'string') return 'text';
  if (type === 'boolean') return 'checkbox';
}

function labelFor(fieldname) {
  if (fieldname.endsWith('cost')) return 'koszt [PLN]';
  if (fieldname.endsWith('count')) return 'ilość';
  if (fieldname.endsWith('unlimited')) return 'bez limitu';
  if (fieldname.endsWith('name')) return 'nazwa';
}
