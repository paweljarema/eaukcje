import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import { connect } from 'react-redux';
import * as adminActions from '../../actions/adminActions';
import { mapAdminAndDocumentStateToProps } from '../Admin';

import Modal from '../Modal';
import Progress from '../Progress';
import NoDocuments from './Empty';
import AuctionEndHelper from '../../helpers/auctionEndHelper';

import { Pagination } from '../Pagination';
import { applyToTable } from '../../functions/table';
import { friendlyAuctionLink } from '../../functions/links';

export default connect(mapAdminAndDocumentStateToProps, adminActions)(
  class extends Component {
  	constructor(props) {
  		super(props);
  		this.state = { page: 1, pages: 1, per_page: 5, mailbox: false };

  		this.paginateTo = this.paginateTo.bind(this);
  		this.openMailbox = this.openMailbox.bind(this);
  		this.sendMessage = this.sendMessage.bind(this);
      this.deleteAuction = this.deleteAuction.bind(this);
  	}

  	componentWillReceiveProps(props) {
  		if (props.documents) {
  			const { documents } = props;

  			if (typeof documents.count === 'number') {
  				applyToTable((e) => e.style.opacity = 1);
  				this.setState({ pages: documents.count });
  			}
  		}
  	}

  	componentDidMount() {
  		this.paginateTo(1);
  	}

  	paginateTo(page) {
  		const { admin } = this.props;
  		const { pages, per_page } = this.state;

  		if (admin) {
  			applyToTable((e) => e.style.opacity = 0.8);
  			this.setState({ page }, () => this.props.paginateDocuments({ admin_id: admin._id, doctype: 'auction', page, per_page }));
  		}
  	}

  	openMailbox(email, title) {
  		this.setState({ mailbox: { email, title} });
  	}

  	sendMessage() {
  		const { title, email } = this.state.mailbox;
  		const formData = new FormData(this.messageFormRef);
      formData.append('admin_id', this.props.admin._id);
  		formData.append('email', email);
  		formData.append('title', title);

  		this.setState({ mailbox: false }, () => this.props.mailUser(formData));
  	}

  	deleteAuction({ id, title, email }) {
  		const confirm = window.confirm(`Czy napewno chcesz usunąć aukcję ${title}?`);
  		if (confirm) {
  			const prompt = window.prompt('Proszę podać powód usunięcia');
  			const documents = this.props.documents['auction'];

  			const { admin } = this.props;
  			const { per_page } = this.state;

  			let page = this.state.page;
  			if (documents.length === 1) {
  				page = (page - 1) || 1;
  			}

  			this.props.deleteAuctionAndPaginate({ auction_id: id, email, title, reason: prompt, admin_id: admin._id, doctype: 'auction', page, per_page });
  		}
  	}


  	render() {
  		const documents = this.props.documents && this.props.documents['auction'];
  		const { page, pages, mailbox } = this.state;
  		const { admin } = this.props;

  		if (!admin)
  			return <Redirect to="/admin" />;

  		if (documents && documents.length) {
  			return (
  				<div className="auction-list">
  					{
  						mailbox && (
  							<Modal
  								title={<span><i className="material-icons">mail_outline</i>Wyślij wiadomość w temacie auckji {mailbox.title}</span>}
  								actions={<button className="standard-button" onClick={this.sendMessage}>Wyślij</button>}
  								open={mailbox}
  								close={() => this.setState({mailbox:false})}
  							>
  								<form ref={ (e) => this.messageFormRef = e }>
  									<input name="subject" type="text" placeholder="wpisz tytuł" />
  									<textarea name="message" placeholder="wpisz wiadomość"></textarea>
  								</form>

  							</Modal>
  						)
  					}
  					<Pagination page={page} pages={pages} clickHandler={this.paginateTo} />
  					<table>
  						<thead>
  							<tr>
  								<th>Sprzedawca</th>
  								<th>Przedmiot</th>
  								<th>Opis</th>
  								<th>Cena</th>
  								<th>Licytujący</th>
  								<th>Polubienia</th>
  								<th className="icon"><i className="material-icons">alarm</i></th>
  								<th>Akcje</th>
  							</tr>
  						</thead>
  						<tbody>
  							{
  								documents.map((doc, index) => (
  									<tr key={"auction_"+ index}>
  										<td>{doc.owner}</td>
  										<td>{doc.title}</td>
  										<td>{doc.shortdescription}</td>
  										<td><span className="price">{doc.price.current_price}</span></td>
  										<td>{doc.bids.length || 0}</td>
  										<td>{doc.likes || 0}</td>
  										<td>{AuctionEndHelper(doc.date)}</td>
  										<td>
  											<i className="material-icons" onClick={ (e) => window.open(friendlyAuctionLink(doc)).focus() } title="zobacz">open_in_new</i>
  											<i className="material-icons" onClick={ (e) => this.openMailbox(doc.email, doc.title) } title="wyślij wiadomość sprzedawcy">mail_outline</i>
  											<i className="material-icons" onClick={ (e) => this.deleteAuction({id: doc._id, title: doc.title, email: doc.email})} title="usuń">delete_forever</i>
  										</td>
  									</tr>
  								))
  							}
  						</tbody>
  					</table>
  					<Pagination page={page} pages={pages} clickHandler={this.paginateTo} />
  				</div>
  			);
  		} else if (documents === null || documents === undefined) {
  			return <Progress />;
  		} else if (documents === false || documents.length === 0) {
  			return <NoDocuments />;
  		}

  	}
  }
);
