import React, { Component } from 'react';
import { Redirect, Link } from 'react-router-dom';

import { connect } from 'react-redux';
import { mapAdminAndDocumentStateToProps } from '../Admin';
import * as adminActions from '../../actions/adminActions';

import Progress from '../Progress';
import NoDocuments from './Empty';

import { Pagination } from '../Pagination';
import { fadeTableIn, fadeTableOut } from '../../functions/table';
import { username } from '../../functions/user';
import { friendlyDate } from '../../functions/date';
import { invoiceLinkAdmin, invoiceSymfonyExportLink } from '../../functions/links';

export default connect(mapAdminAndDocumentStateToProps, adminActions)(
  class extends Component {
    constructor(props) {
      super(props);

      this.state = { page: 1, pages: 1, per_page: 50, email: 'faktury@eaukcje.pl' };

      this.paginateTo = this.paginateTo.bind(this);
    }

    componentWillUpdate(newProps, newState) {
      let { documents, from, to } = newProps;

      if (from !== this.props.from || to !== this.props.to) {
        this.paginateTo(1, { from, to });
      }

      if (this.updated && parseInt(this.updated / 100) === parseInt(Date.now() / 100)) return;

      if (documents && typeof documents.count === 'number') {
        this.updated = Date.now();
        this.setState({ pages: documents.count }, fadeTableIn);
      }
    }

    componentDidMount() {
      this.paginateTo(1);
    }

    paginateTo(page, optional) {
      const
        { admin, paginateDocuments } = this.props,
        { from, to } = optional || this.props,
        { per_page } = this.state;

      if (admin) {
        fadeTableOut();
        paginateDocuments({
          admin_id: admin._id,
          doctype: 'invoice',
          page,
          per_page,
          from,
          to
        });
        this.setState({ page });
      }
    }

    render() {
      const
        invoices = this.props.documents && this.props.documents.invoices,

        { page, pages } = this.state,
        { admin } = this.props,

        iconStyle = { marginRight: 6 };

      // console.log(invoices);

      if (!admin)
        return <Redirect to="/admin" />;

      console.log(invoices);

      if (invoices && invoices.length) {
        return (
          <div className="invoice-list">
            <Pagination page={ page } pages={ pages } clickHandler={ this.paginateTo } />
            <table>
              <thead>
                <tr>
                  <th>Użytkownik</th>
                  <th>Numer</th>
                  <th>Opis</th>
                  <th>Data wystawienia</th>
                  <th></th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>

              <tbody>
                {
                  invoices.map((doc, i) => (
                    <tr key={ doc._id }>
                      <td>{ username(doc.user) }</td>
                      <td>{ doc.number }</td>
                      <td>{ doc.description }</td>
                      <td>{ friendlyDate(doc.created) }</td>
                      <td>
                        <Link
                          to={ invoiceLinkAdmin(admin, doc, true) }
                          target="_blank"
                          className="clickable"
                        >

                          <i className="material-icons" style={ iconStyle }>open_in_new</i>
                          Zobacz

                        </Link>
                      </td>
                      <td>
                        <Link
                          to={ invoiceLinkAdmin(admin, doc) }
                          target="_blank"
                          className="clickable"
                        >

                          <i className="material-icons" style={ iconStyle }>cloud_download</i>
                          Zapisz PDF

                        </Link>
                      </td>
                      <td>
                        <Link
                          to={ invoiceSymfonyExportLink(admin, doc) }
                          target="_blank"
                          className="clickable"
                        >
                          <i className="material-icons" style={ iconStyle }>save_alt</i>
                          Eksport do Symfonii

                        </Link>
                      </td>
                    </tr>
                  ))
                }
              </tbody>
            </table>

            <Pagination page={ page } pages={ pages } clickHandler={ this.paginateTo } />
          </div>
        );
      } else if (invoices == null) {
        return <Progress />;
      } else if (invoices === false || invoices.length === 0) {
        return (
          <div style={{ paddingLeft: 10, opacity: .3 }}>
            <div className="absolute-center">
              <br />
              <h3> Brak faktur w tym okresie.</h3>
            </div>
          </div>
        );
      }

      return null;
    }
  }
);
