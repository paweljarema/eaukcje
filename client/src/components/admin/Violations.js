import React from 'react';

import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import * as adminActions from '../../actions/adminActions';
import { mapAdminAndDocumentStateToProps } from '../Admin';

import Modal from '../Modal';
import Progress from '../Progress';
import NoDocuments from './Empty';
import AuctionEndHelper from '../../helpers/auctionEndHelper';

import { Pagination } from '../Pagination';
import { applyToTable, loadStyle, loadedStyle } from '../../functions/table';
import { username } from '../../functions/user';
import { numberDate } from '../../functions/date';
import { friendlyAuctionLink } from '../../functions/links';

export default connect(mapAdminAndDocumentStateToProps, adminActions)(
  class extends React.Component {
    constructor(props) {
      super(props);

      this.state = { page: 1, pages: 1, per_page: 5, mailbox: false, openRow: null };

      this.openMailbox = this.openMailbox.bind(this);
  		this.sendMessage = this.sendMessage.bind(this);
      this.expandRow = this.expandRow.bind(this);
      this.deleteAuction = this.deleteAuction.bind(this);
      this.hangAuction = this.hangAuction.bind(this);
      this.resolveViolation = this.resolveViolation.bind(this);
    }

    componentWillReceiveProps(props) {
      if (props.documents) {
        const { documents } = props;

        if (typeof documents.count === 'number') {
          applyToTable(loadedStyle);
          this.setState({ pages: documents.count })
        }
      }
    }

    componentDidMount() {
      this.paginateTo(1);
    }

    paginateTo(page) {
      const
        { admin } = this.props,
        { pages, per_page } = this.state;

      if (admin) {
        applyToTable(loadStyle);
        this.setState({ page }, () => {
          this.props.paginateDocuments({
            admin_id: admin._id,
            doctype: 'violation',
            page,
            per_page
          })
        });
      }
    }

    openMailbox(email, title) {
  		this.setState({ mailbox: { email, title }});
  	}

    sendMessage() {
  		const { title, email } = this.state.mailbox;
  		const formData = new FormData(this.messageFormRef);

      formData.append('admin_id', this.props.admin._id);
  		formData.append('email', email);
  		formData.append('title', title);

  		this.setState({ mailbox: false }, () => this.props.mailUser(formData));
  	}

    deleteAuction({ id, title, email }) {
  		const confirm = window.confirm(`Czy napewno chcesz usunąć aukcję ${title}?`);
  		if (confirm) {
  			const prompt = window.prompt('Proszę podać powód usunięcia');
  			const documents = this.props.documents['violation'];

  			const { admin } = this.props;
  			const { per_page } = this.state;

  			let page = this.state.page;
  			if (documents.length === 1) {
  				page = (page - 1) || 1;
  			}

  			this.props.deleteAuctionAndPaginate({ auction_id: id, email, title, reason: prompt, admin_id: admin._id, doctype: 'auction', page, per_page });
  		}
  	}

    expandRow(id) {
      let
        { openRow } = this.state,
        alreadyOpened = openRow === id;

      this.setState({ openRow: alreadyOpened ? null : id });
    }

    hangAuction({ id, title }) {
      const confirm = window.confirm(`Zawiesić ogłoszenie ${ title }?`)
      if (confirm) {
        const { admin } = this.props;
        const { per_page, page } = this.state;

        this.props.hangAuctionAndPaginate({ auction_id: id, admin_id: admin._id, doctype: 'violation', page, per_page });
      }
    }

    resolveViolation({ id }) {
      const confirm = window.confirm('Oznaczyć problem jako rozwiązany ?');
      if (confirm) {
        const documents = this.props.documents['violation'];

        const { admin } = this.props;
        const { per_page } = this.state;

        let page = this.state.page;
        if (documents.length === 1) {
          page = (page - 1) || 1;
        }

        this.props.resolveViolationAndPaginate({ auction_id: id, admin_id: admin._id, doctype: 'violation', page, per_page });
      }
    }

    render() {
      const
        documents = this.props.documents && this.props.documents['violation'],
        { page, pages, mailbox, openRow } = this.state,
        { admin } = this.props;

      if (!admin)
        return <Redirect to='/admin' />;

      if (documents && documents.length) {
        console.log(documents, typeof documents);

        return (
          <div className="violation-list">
            {
              mailbox && (
                <Modal
                  title={<span><i className="material-icons">mail_outline</i>Wyślij wiadomość w temacie auckji {mailbox.title}</span>}
                  actions={<button className="standard-button" onClick={this.sendMessage}>Wyślij</button>}
                  open={mailbox}
                  close={() => this.setState({mailbox:false})}
                >
                  <form ref={ (e) => this.messageFormRef = e }>
                    <input name="subject" type="text" placeholder="wpisz tytuł" />
                    <textarea name="message" placeholder="wpisz wiadomość"></textarea>
                  </form>

                </Modal>
              )
            }

            <Pagination page={page} pages={pages} clickHandler={this.paginateTo} />


            <table>
  						<thead>
  							<tr>
  								<th>Sprzedawca</th>
  								<th>Przedmiot</th>
  								<th>Opis</th>
  								<th>Cena</th>
  								<th>Licytujący</th>
  								<th>Polubienia</th>
                  <th>Naruszenia</th>
  								<th className="icon"><i className="material-icons">alarm</i></th>
                  <th>Status</th>
  								<th>Akcje</th>
  							</tr>
  						</thead>
  						<tbody>
  							{
  								documents.map((doc, index) => {
                    if (!doc.auction) return null;
                    return (
                      <React.Fragment key={doc.auction._id}>
      									<tr>
      										<td>{username(doc.auction._user)}</td>
      										<td>{doc.auction.title}</td>
      										<td>{doc.auction.shortdescription}</td>
      										<td><span className="price">{doc.auction.price.current_price}</span></td>
      										<td>{doc.auction.bids.length || 0}</td>
      										<td>{doc.auction.likes || 0}</td>
                          <td
                            className="violation-cell"
                            onClick={ () => this.expandRow(doc.auction._id) }
                          >
                             ( {doc.auction.violations.reported || 0} ) pokaż zgłoszenia
                            <i
                              className="material-icons"
                              title="Zobacz naruszenia"
                            >
                              { ( openRow === doc.auction._id ? 'expand_more' : 'chevron_right' ) }
                            </i>

                          </td>
      										<td>{AuctionEndHelper(doc.auction.date)}</td>
                          <td>{ doc.auction.violations.hanged ? 'zawieszona' : 'aktywna w serwisie' }</td>
      										<td>
      											<i className="material-icons" onClick={ (e) => window.open(friendlyAuctionLink(doc.auction)).focus() } title="zobacz">open_in_new</i>
      											<i className="material-icons" onClick={ (e) => this.openMailbox(doc.auction._user.contact.email, doc.auction.title) } title="wyślij wiadomość sprzedawcy">mail_outline</i>
                            <i className="material-icons" onClick={ () => this.hangAuction({ id: doc.auction._id, title: doc.auction.title }) } title="zablokuj aukcję na 3 dni">block</i>
                            <i className="material-icons" onClick={ () => this.resolveViolation({ id: doc.auction._id }) } title="rozwiąż zgłoszenie i odblokuj aukcję">check</i>
                            <i className="material-icons" onClick={ (e) => this.deleteAuction({id: doc.auction._id, title: doc.auction.title, email: doc.auction._user.contact.email})} title="usuń aukcję">delete_forever</i>
                          </td>
      									</tr>
                        {
                          openRow === doc.auction._id && (
                            doc.reasons.map((reason, i) => {
                              return (
                                <tr className="report-row" key={ doc.users[ i ] }>
                                  <td colSpan="3">
                                    <div className="date">
                                      { numberDate(doc.dates[ i ]) }
                                    </div>

                                    <div className="group">
                                      { doc.groups[ i ] }
                                    </div>
                                  </td>

                                  <td colSpan="6">
                                    <div className="report">
                                      <div className="reason">
                                        { reason }
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                              )
                            })
                          )
                        }
                      </React.Fragment>
                    )
  								})
  							}
  						</tbody>
  					</table>


            <Pagination page={page} pages={pages} clickHandler={this.paginateTo} />
          </div>
        )
      } else if (documents === null || documents === undefined) {
        return <Progress />;
      } else if (documents === false || documents.length === 0) {
        return <NoDocuments />;
      }

    }
  }
);
