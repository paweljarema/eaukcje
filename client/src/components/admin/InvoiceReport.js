import React, { Component } from 'react';
import { nicePrice, discardVat, discardVatClosure, sumUp } from '../../functions/number';

export default class extends Component {

  constructor(props) {
    super(props);

    this.state = {};
    this.fetch = this.fetch.bind(this);
  }

  componentWillMount() {
    this.fetch();
  }

  componentWillUpdate(newProps) {
    let { from, to } = newProps;

    if (from !== this.props.from || to !== this.props.to) {
      this.fetch({ from, to });
    }
  }

  fetch(optional) {
    if (this.props.admin) {
      let
        { admin } = this.props,
        { from, to } = optional || this.props;

      fetch('/api/get_invoice_report', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
          admin_id: admin._id,
          from: from,
          to: to
        })
      })
      .then(res => res.json())
      .then(data => this.setState({ data }));
    }
  }

  render() {
    const { data } = this.state;

    if (!data || !data.length) return null;

    return (
      <div className="stats">
        <table>
          <thead>
            <th>Ilość faktur</th>
            <th>VAT</th>
            <th>Kwota netto</th>
            <th>Kwota brutto</th>
          </thead>
          <tbody>
            {
              data.map(( vatGroup, i ) => {

                let noVat = discardVatClosure(vatGroup.vat);

                return (
                  <tr key={ 'group_' + vatGroup.vat }>
                    <td>{ vatGroup.count }</td>
                    <td>{ vatGroup.vat }%</td>
                    <td>{ nicePrice( noVat(vatGroup.total)) }</td>
                    <td>{ nicePrice( vatGroup.total ) }</td>
                  </tr>
                );
              })
            }
            {
              data.length > 1 && (
                <tr>
                  <td></td>
                  <td>Razem:</td>
                  <td>{ nicePrice( noVatSpeciffic(data) ) }</td>
                  <td>{ nicePrice( sumUp(data, 'total')) }</td>
                </tr>
              )
            }
          </tbody>
        </table>
      </div>
    );
  }

};

function noVatSpeciffic(store) {
  return store.reduce((a, b) => a + b.total - (b.total * (b.vat / 100)), 0);
}
