import React, { Component } from 'react';

import { connect } from 'react-redux';
import { mapAdminStateToProps } from '../Admin';
import * as adminActions from '../../actions/adminActions';

import { Redirect } from 'react-router-dom';

export default connect(mapAdminStateToProps, adminActions)(
  class extends Component {
  	constructor(props) {
  		super(props);
  		this.bulkSendMessage = this.bulkSendMessage.bind(this);
  	}

  	bulkSendMessage(e) {
  		e.preventDefault();
  		const { admin } = this.props;

  		if (admin) {
  			const formData = new FormData(this.messageFormRef);
  			formData.append('admin_id', admin._id);
  			this.props.bulkSendMail(formData);
  		}
  	}

  	render() {
  		const { admin } = this.props;

  		if (!admin)
  			return <Redirect to="/admin" />

  		return (
  			<div className="mail-base">
  				<h1>Baza mailingowa</h1>
  				<h3><i className="material-icons">mail_outline</i>Wyślij wiadomość do wszystkich użytkowników</h3>
  				<form ref={ (e) => this.messageFormRef = e }>
  					<input name="subject" type="text" placeholder="wpisz tytuł" />
  					<textarea name="message" placeholder="wpisz wiadomość" />
  					<button className="standard-button" onClick={this.bulkSendMessage}><i className="material-icons">mail_outline</i>Wyślij</button>
  				</form>
  			</div>
  		);
  	}
  }
);
