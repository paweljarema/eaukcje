import React from 'react';

export default () => (
  <div className="TechBreak absolute-center">
    <i className="material-icons">power_off</i>
    <h1>Przerwa Techniczna</h1>
    <p>Zapraszamy później</p>
  </div>
);
