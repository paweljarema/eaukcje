import React, { Component } from 'react';

import { connect } from 'react-redux';
import { mapAdminAndDocumentStateToProps } from '../Admin';
import * as adminActions from '../../actions/adminActions';

import Modal from '../Modal';
import Progress from '../Progress';
import NoDocuments from './Empty';

import { Pagination } from '../Pagination';
import { Redirect } from 'react-router-dom';
import { applyToTable } from '../../functions/table';

export default connect(mapAdminAndDocumentStateToProps, adminActions)(
  class extends Component {
  	constructor(props) {
  		super(props);

  		this.state = { page: 1, pages: 1, per_page: 10, mailbox: false };

  		this.paginateTo = this.paginateTo.bind(this);
  		this.openMailbox = this.openMailbox.bind(this);
  		this.sendMessage = this.sendMessage.bind(this);
  	}

  	componentWillReceiveProps(props) {
  		if (props.documents) {
  			const { documents } = props;

  			if (typeof documents.count === 'number') {
  				this.setState({ pages: documents.count });
  				applyToTable((e) => e.style.opacity = 1);
  			}
  		}
  	}

  	componentDidMount() {
  		this.paginateTo(1);
  	}

  	openMailbox(email) {
  		this.setState({ mailbox: { email } });
  	}

  	sendMessage() {
  		const { email } = this.state.mailbox;
  		const { admin } = this.props;

  		const formData = new FormData(this.messageFormRef);
  		formData.append('email', email);
  		formData.append('admin_id', admin._id);

  		this.setState({ mailbox: false }, () => this.props.mailUser(formData));
  	}

  	deleteUser({ id, name }) {
  		const confirm = window.confirm(`Czy napewno chcesz usunąć użytkownika ${name} i wszystkie aukcje powiązane z jego kontem?`);
  		if (confirm) {
  			const prompt = window.prompt('Proszę podać powód usunięcia');
  			const { admin, documents } = this.props;
  			const { per_page } = this.state;

  			let page = this.state.page;
  			if (documents.length === 1) {
  				page = (page - 1) || 1;
  			}

  			this.props.deleteUserAndPaginate({ user_id: id, reason: prompt, admin_id: admin._id, doctype: 'user', page, per_page });
  		}
  	}

  	paginateTo(page) {
  		const { admin } = this.props;
  		const { per_page } = this.state;

  		if (admin) {
  			applyToTable((e) => e.style.opacity = 0.8);
  			this.props.paginateDocuments({ admin_id: admin._id, doctype: 'user', page, per_page });
  			this.setState({ page });
  		}
  	}

  	render() {
  		const documents = this.props.documents && this.props.documents['user']; //users
  		const { page, pages, mailbox } = this.state;
  		const { admin } = this.props;

  		if (!admin)
  			return <Redirect to="/admin" />;

  		if (documents && documents.length) {
  			return (
  				<div className="user-list">
  					{
  						mailbox && (
  							<Modal
  								title={<span><i className="material-icons">mail_outline</i>Wyślij wiadomość na {mailbox.email}</span>}
  								actions={<button className="standard-button" onClick={this.sendMessage}>Send</button>}
  								open={mailbox}
  								close={() => this.setState({mailbox:false})}
  							>
  								<form ref={ (e) => this.messageFormRef = e }>
  									<input name="subject" type="text" placeholder="wpisz tytuł" />
  									<textarea name="message" placeholder="wpisz wiadomość"></textarea>
  								</form>

  							</Modal>
  						)
  					}
  					<Pagination page={page} pages={pages} clickHandler={this.paginateTo} />
  					<table>
  						<thead>
  							<tr>
  								<th>Imię i nazwisko</th>
  								<th>Email</th>
  								<th>Adres</th>
  								<th>Konto</th>
  								<th>Zgoda na korespondencję</th>
  								<th>Opinie</th>
  								<th>Akcje</th>
  							</tr>
  						</thead>
  						<tbody>
  							{
  								documents.map((doc, index) => (
  									<tr key={"user_" + index}>
  										<td>{ `${doc.firstname || ''} ${doc.lastname || (!doc.firstname ? 'Anonim' : '')}` }</td>
  										<td>{ (doc.contact ? `${doc.contact.email}` : 'użytkownik nie dodał E-maila' ) }</td>
  										<td>{ (doc.address ? `${doc.address.street}\n${doc.address.postal} ${doc.address.city}` : 'użytkownik nie dodał danych adresowych') }</td>
  										<td>{ (doc.balance ? `${doc.balance.account_number}` : 'użytkownik nie dodał konta') }</td>
  										<td>{ (doc.agreements ? (doc.agreements.corespondence ? 'Tak' : 'Nie') : 'Nie określono') }</td>
  										<td title='głosy / średnia ocena'>{ `${doc.opinion.count} / ${doc.opinion.rate}` }</td>
  										<td>
  											{
  												( doc.agreements && doc.agreements.corespondence ? <i className="material-icons action" onClick={() => this.openMailbox(doc.contact.email)} title="wyślij wiadomość">mail_outline</i> : null)
  											}
  											<i className="material-icons action" onClick={() => this.deleteUser({ id: doc._id, name: doc.firstname })} title="usuń">delete_forever</i>
  										</td>
  									</tr>
  								))
  							}
  						</tbody>
  					</table>
  					<Pagination page={page} pages={pages} clickHandler={this.paginateTo} />
  				</div>
  			);
  		} else if (documents === null || documents === undefined) {
  			return <Progress />;
  		} else if (documents === false || documents.length === 0) {
  			return <NoDocuments />;
  		} else {
  			return null;
  		}
  	}
  });
