import React from 'react';
import { monthPl } from '../../functions/date';

import './MonthPicker.css';

export default ({ onChange }) => {
  let
    month = new Date().getMonth(),
    range = Array.from( { length: month + 1 }, (v, k) => k );

  return (
    <select
      className="invoice-month-picker"
      onChange={ onChange }
      defaultValue={ month }
    >
      {
        range.map(val => (
          <option key={ 'm_' + val } value={ val }>{ monthPl(val) }</option>
        ))
      }
    </select>
  );
};
