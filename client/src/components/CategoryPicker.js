import React, { Component } from 'react';
import PropertyPicker from './PropertyPicker';
import { isArray, arrayIsEmpty, arrayIsNotEmpty } from '../functions/array';

const
  CATEGORY_ID_KEY = 'id_for_category',
  EMPTY_SELECT = 'Wybierz...';

function categoryLabels(i) {
  if (i === 0) return 'podkategoria';
  if (i === 1) return 'podpodkategoria';
  return 'kategoria ' + (i + 2) + 'go rzędu';
}

class CategoryPicker extends Component {
  constructor(props) {
    super(props);

    this.state = {
      picked: []
    };

    this.handleCategory = this.handleCategory.bind(this);
    this.getCategoryObjects = this.getCategoryObjects.bind(this);
  }

  componentWillReceiveProps(props) {

    if (props.update && props.categoryData && !this.editPropertiesLoaded) {
      this.editPropertiesLoaded = true;

      const
        { categoryData } = props;

      if (categoryData.length)
        this.setState({ picked: categoryData });
    }
  }

  handleCategory(event) {
    const
      select  = event.target,
      { name, value } = select;

    if (name === 'category')
      this.setState({ picked: [ value ] });

    let
      index = Number( name.split('_')[1] );

    this.setState(({ picked }) => ({ picked: picked.slice(0, index).concat([ value ]) }));
  }

  getCategoryObjects() {
    let
      { picked } = this.state,

      set = this.props.categories,
      categoryObjects = [],
      properties = [];

    if (arrayIsEmpty(picked)) return { categoryObjects, properties };

    for (let name of picked) {
      if (isArray(set)) {
        let index = set.findIndex(c => c.name === name);
        if (index >= 0) {
          let current = set[ index ];
          categoryObjects.push( current );

          if (current.properties) {
            for (let prop of current.properties) {
              let newProp = {
                ...prop,
                path: picked
                  .slice(0, picked.indexOf(name) + 1)
                  .join('.')
              };

              properties.push(newProp);
            }
          }

          // if (current.properties)
          //   properties = properties.concat( current.properties );

          set = current.subcategories;
        }
      }
    }

    return { categoryObjects, properties };
  }

  render() {
    const
      { update, categories, categoryData, propertyData } = this.props,
      { picked } = this.state;

    if (!categories) return null;

    let
      { categoryObjects, properties } = this.getCategoryObjects();


    return (
      <div>

        <p>
          <select name="category" onChange={ this.handleCategory } value={ picked[0] || null } defaultValue={ EMPTY_SELECT }>
              {
                  categories.map(category => (
                    <option key={ category.name }>
                      { category.name }
                    </option>
                  ))
              }
              <option>{ EMPTY_SELECT }</option>
          </select>

          <span className="label">Kategoria główna</span>
          <input type="hidden" name={ CATEGORY_ID_KEY + '_0' } value={ findCategoryId(categories, picked, 0) } />
        </p>

        {
          categoryObjects.map((object, i) => {
            if (!object.subcategories) return null;

            let index = i + 1;

            return (
              <p key={ object._id }>
                <select name={ 'subcategory_' + index } onChange={ this.handleCategory } value={ picked[ index ] || null } defaultValue={ EMPTY_SELECT }>
                    {
                      object.subcategories.map(subcategory => (
                        <option key={ subcategory.name }>
                          { subcategory.name }
                        </option>
                      ))
                    }
                    <option>{ EMPTY_SELECT }</option>
                </select>

                <span className="label">{ categoryLabels(i) }</span>
                <input type="hidden" name={ CATEGORY_ID_KEY + '_' + index } value={ findCategoryId(categories, picked, index) } />
              </p>
            );
          })
        }

        {
          arrayIsNotEmpty(properties) && <PropertyPicker properties={ properties } update={ update } propertyData={ propertyData } />
        }

      </div>
    );
  }
}

function isSet(number) {
  if (typeof number === 'number' && number >= 0) return true;
  return false;
}

function isNotEmpty(object) {
  if (Object.prototype.toString.call(object) === '[object Array]') {
    return object.length > 0;
  }

  return Boolean(object);
}

function findCategoryId(set, path, index) {

  if (!set) return null;
  const category = set.find(cat => cat.name === path[0]);
  if (!category) return null;

  if (index === 0) return category._id;

  return findCategoryId(category.subcategories, path.slice(1), index - 1);
}

export { isSet, isNotEmpty, EMPTY_SELECT };
export default CategoryPicker;
