import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import { arrayIsNotEmpty } from '../../functions/array';
import { NEW_AUCTION_PATH, LOGIN_PATH } from '../../constants/urls';

import './NewAuctionButton.css';

export default withRouter(
  connect(({ user }) => ({ user }))(
    class extends Component {
      handleRedirect() {
        let
          { user } = this.props,
          path;

        if (user)
          path = NEW_AUCTION_PATH;
        else
          path = LOGIN_PATH;

        this.props.history.push(path);

        if (this.props.onClick)
          this.props.onClick();
      }

      render() {
        return (
          <span className="new-auction-button" onClick={ this.handleRedirect.bind(this) }>
            <i className="new-auction-button__icon material-icons">add</i> Dodaj aukcję
          </span>
        )
      }
    }
  )
);
