import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import RawImage from './RawImage';
import sizeMe from 'react-sizeme';
import { ImageProgress } from '../Progress';
import PriceHelper from '../../helpers/priceHelper';
import { friendlyAuctionLink } from '../../functions/links';
import { bigWordsIn } from '../../functions/words';

import { IMAGE_ASPECT_RATIO } from './constants';
import { BACKGROUND_SIZE } from '../../constants/display';

class AuctionPhoto extends Component {
    render() {
        const
            { auction, height } = this.props;

        return (
            <div ref={(e) => this.photoRef = e} className="photo" style={ height ? { height } : null }>
                <ImageProgress />
                {
                    <RawImage link={auction} backgroundSize={BACKGROUND_SIZE} />
                }
            </div>
        );
    }
}

class Auction extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    onSize(size) {
        const height = size.width / IMAGE_ASPECT_RATIO;
        this.setState({ photoHeight: height });
    }

    render() {
        const { auction } = this.props;
        if (!auction) return null;

        const
          current_price = auction.price.current_price || auction.price.start_price,
          priceIsBig = String(current_price).length > 4,

          title = auction.title,
          titleIsBig = bigWordsIn(title);

        return (
            <Link to={ friendlyAuctionLink(auction) }>
                <div className="auction">
                    <AuctionPhoto auction={ auction } onSize={ this.onSize.bind(this) } height={ this.state.photoHeight } />
                    <div className={ `title${ titleIsBig ? ' long' : '' }` }>
                        <h3>{auction.title}</h3>
                    </div>
                    <div className="description">
                        <p>{auction.shortdescription}</p>
                    </div>
                    <div className="price-div">
                        <span className="price">
                          Aktualna cena:
                          <span className={ `value${ priceIsBig ? ' long' : '' }` }>
                            { PriceHelper.write(current_price) }
                          </span>
                        </span>
                    </div>
                </div>
            </Link>
        );
    }
}

AuctionPhoto = sizeMe({ monitorHeight: true })(AuctionPhoto);
export default Auction;
