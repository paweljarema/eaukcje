import React, { Component } from 'react';
import NameHelper from '../../helpers/nameHelper';
import PriceHelper from '../../helpers/priceHelper';
import { datetimeFromMillis } from '../../functions/date';

import './Bids.css';

class AuctionBids extends Component {
    render() {
        const { user, auction, bidders } = this.props;
        const current_price = auction.price.current_price || auction.price.start_price;
        return (
            <div className="auction-bids">
                {
                    auction.bids.length ? (
                        <div className="all-bids">
                            <h2 style={{ textAlign: 'center' }}><i className="material-icons">gavel</i> Stan licytacji</h2>
                            <table>
                              <thead>
                                <tr>
                                  <th>użytkownik</th>
                                  <th>cena</th>
                                  <th>data</th>
                                </tr>
                              </thead>
                              <tbody>
                                {
                                    auction.bids.map((bid, index) => {
                                        let
                                          myName = 'Ty', // NameHelper.name(bidders[bid._user])
                                          price = index === 0 ? PriceHelper.write(current_price) : PriceHelper.write(bid.price),
                                          date = datetimeFromMillis(bid.date);

                                        if (bidders[bid._user]) {
                                            return (
                                                <tr
                                                    key={'bid_' + index}
                                                    className={`bidder ${bidders[bid._user]._id === user._id ? 'me' : ''}`}
                                                >

                                                    <td>{bidders[bid._user]._id === user._id ? myName : NameHelper.covername(bidders[bid._user])}</td>
                                                    <td className="price">{ price }</td>
                                                    <td className="datetime">{ date }</td>
                                                </tr>
                                            );
                                        } else {
                                            return (
                                                <tr key={'bid_' + index}>

                                                    <td>..............</td>
                                                    <td className="price">{ price }</td>
                                                    <td className="datetime">{ date }</td>
                                                </tr>
                                            );
                                        }

                                    })
                                  }
                                </tbody>
                            </table>
                        </div>
                    )
                    :
                    (
                        <div className="no-result">
                            <i className="material-icons">gavel</i>
                            <h2>{ (auction.ended ? 'Nikt nie zalicytował' : 'Nikt nie licytuje') }</h2>
                            <p>{ (auction.ended ? '' : 'Podbij stawkę minimalną i bądź pierwszy!') }</p>
                        </div>
                    )
                }
            </div>
        )
    }
}

export default AuctionBids;
