import React, { Component } from 'react';
import Auction from './Auction';

import { Swipeable } from 'react-swipeable';
import { arrayIsNotEmpty, arraysAreDifferent } from '../../functions/array';
import { getRequest, terminateRequestsFor } from '../../helpers/requestHelper';

import './AuctionTicker.css';
import './AuctionTickerMobile.css';

const COMPONENT_GROUP_ID = 'auction_tickers';

/*
  props.options:

    category - name of category to fetch
    size - count of auctions in view
    limit - count of total auctions in slider
    sliderArea - slider field in percents %
    * placement - slider placement, right or left - currently derrived from request completionId
    image - slider background,
    index - category index,
    tick - (boolean) shift slider automatically
    interval - delay between ticks

*/

export default class extends Component {
  constructor(props) {
    super(props);

    this.state = { auctions: [], chunk: null };

    this.setAuctions = this.setAuctions.bind(this);
    this.renderSectionTitle = this.renderSectionTitle.bind(this);
    this.currentChunk = this.currentChunk.bind(this);
    this.renderSliderControl = this.renderSliderControl.bind(this);
    this.setChunk = this.setChunk.bind(this);
    this.tick = this.tick.bind(this);
    this.right = this.right.bind(this);
    this.left = this.left.bind(this);

    this.tickAhead = props.options.tick;
    this.TICK_INTERVAL = props.options.interval;
  }

  setAuctions(auctions, completionId) {
    if (this.unmounted) return;
    if (arrayIsNotEmpty(auctions))
      this.setState(
        { auctions, placement: placement(completionId) },
        this.appendInterval
      );
  }

  setChunk(a, b) {
    this.removeInterval();
    this.setState(
      { chunk: [ a, b ] },
      this.appendInterval
    );
  }

  componentDidMount() {
    getRequest({
      group_id: COMPONENT_GROUP_ID,
      url: frontPageAuctionsUrl(this.props.options),
      callback: this.setAuctions
    });
  }

  componentWillUpdate(nextProps) {
    if ((this.props.options || {}).size !== (nextProps.options || {}).size) {
      this.setState({ chunk: [0, nextProps.options.size] });
    }
  }

  componentWillUnmount() {
    this.unmounted = true;
    this.removeInterval();
    terminateRequestsFor(COMPONENT_GROUP_ID);
  }

  appendInterval() {
    if (!this.tickInterval && this.tickAhead)
      this.tickInterval = setInterval(this.tick, this.TICK_INTERVAL);
  }

  removeInterval() {
    if (this.tickInterval) {
      clearInterval(this.tickInterval);
      this.tickInterval = null;
    }
  }

  right() {
    this.removeInterval();
    this.tick();
    this.appendInterval();
  }

  left() {
    this.removeInterval();
    const
      { chunk, auctions } = this.state,

      [ a, b ] = chunk,

      size = b - a;

    let newChunk;

    if (a - size >= 0)
      newChunk = [a - size, b - size];
    else {
      const
        chunks = Math.ceil(auctions.length / size),
        end = chunks * size;

      newChunk = [end - size, end];
    }

    this.setState({ chunk: newChunk });
    this.appendInterval();
  }

  tick() {
    let
      { size } = this.props.options,
      [ a = 0, b = size ] = this.state.chunk || [],

      newChunk;

    if ( a + size < this.state.auctions.length )
      newChunk = [ a + size, b + size ];
    else
      newChunk = [ 0, size ];

    if (arraysAreDifferent(this.state.chunk, newChunk))
      this.setState({ chunk: newChunk });
  }

  renderSectionTitle({ placement }) {

    if (this.state.auctions.length) {
      let
        sliderOnLeft = placement === 'left',
        style = {
          position: 'absolute',
          [ (sliderOnLeft ? 'left' : 'right') ]: 0,
          fontSize: 80,
          marginTop: 0,
          // [ (sliderOnLeft ? 'marginRight' : 'marginLeft') ]: 40,
          color: 'white',
          textShadow: '-1px 0 3px #100, 0 6px 3px #100, 1px 0 3px #100, 0 -1px 3px #100'
        };

      return (
        <h2 className="auction-ticker-title" style={ style }>
          { this.props.options.category }
        </h2>
      );
    }

  }

  renderSection(auctions, options = {}) {
    options.placement = this.state.placement;
    if (arrayIsNotEmpty(auctions)) return (
      <>
        <div
          className={ `auction-ticker page-padding-container${ options.placement === 'right' ? ' right' : ' left' }` }
          style={{ ...sliderPlacement(options), ...sliderBackground(options) }}
        >

          {
            this.renderSectionTitle(options)
          }

          <Swipeable
  					trackMouse
  					preventDefaultTouchmoveEvent
  					onSwipedLeft={ this.right }
  					onSwipedRight={ this.left }>

            <div className="auction-ticker-slide-area" style={ sliderArea(options) }>
              <div className={ `${ category(options) } auction-ticker-column ${ columnSize(options) }` }>
                {
                  arrayIsNotEmpty(auctions) && (
                    auctions.map(auction => (
                      <div
                        key={ auction.key || auction._id }
                        className="column">

                        <Auction auction={ auction } />
                      </div>
                    ))
                  )
                }
              </div>
              {
                this.renderSliderControl()
              }
            </div>
          </Swipeable>

        </div>
      </>
    );
    else return null;
  }

  renderSliderControl() {
    let
      { size } = this.props.options,

      [ a = 0, b = size ] = this.state.chunk || [],

      allSize = this.state.auctions.length,
      currentChunk = b / size,
      controlCount = Math.ceil(allSize / size),
      controls = [];

    controls.length = controlCount;
    controls.fill(0, 0);

    if (!allSize || allSize <= size) return null;
    else return (
      <div className="auction-ticker-controls">
        {
          controls.map((ctl, i) => (
            <div
              key={ 'control_' + i }
              className={ `auction-ticker-control${ ( currentChunk === i + 1 ) ? ' active' : '' }` }
              onClick={ () => this.setChunk(i * size, (i + 1) * size) }>

            </div>
          ))
        }
      </div>
    );
  }

  currentChunk() {
    let [ a = 0, b = this.props.options.size ] = this.state.chunk || [];
    return this.state.auctions.slice(a, b);
  }

  render() {
    return this.renderSection(
      this.currentChunk(),
      this.props.options
    );
  }
};

function frontPageAuctionsUrl({ category, limit }) {
  return `/frontpage/${ category }/${ limit }`;
}

function sliderArea({ sliderArea, placement }) {
  return {
    flex: `0 0 ${ sliderArea }%`,
    display: 'flex',
    ...sliderPlacement({ placement })
  };
}

function sliderPlacement({ placement }) {
  if (placement === 'right')
    return { justifyContent: 'flex-end' };
  else
    return { justifyContent: 'flex-start' };
}

function sliderBackground({ background, placement, index }) {
  return {
    backgroundColor: columnColor(index),
    backgroundImage: `url('/assets/categories/${ background }')`,
    backgroundSize: 'contain',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: placement === 'left' ? 'right' : 'left'
  };
}

function columnColor(index) {
  // return '#100';
  return '#282c30';

  // switch(index) {
  //   case 0: return '#100';
  //   case 1: return '#1a1a1a';
  //   // case 2: return '#8f968f';
  //   case 3: return '#617483';
  //   // case 4: return '#544339';
  //   case 5: return '#7da3c7';
  //   default: return '#282c30';
  // }

  // switch(index) {
  //   case 0: return '#00070f';
  //   case 1: return '#1a1a1a';
  //   case 2: return '#9da099'; //'#969386';
  //   case 3: return '#768b9c';
  //   case 4: return '#3c4041'; //'#7e724a';
  //   case 5: return '#898586';
  //   case 6: return '#827673';
  //   case 7: return '#413826';
  //   case 8: return '#bfada3';
  //   case 9: return '#7c5f46';
  //   case 10: return '#efefee';
  //   case 11: return '#282c30';
  //   case 12: return '#6a6361';
  //   case 13: return '#8c665e';
  // }
}

function category({ category }) {
  return category;
}

function columnSize({ size }) {
  switch(size || 3) {
      case 1:
        return 'one-column';
      case 2:
        return 'two-column';
      case 3:
        return 'three-column';
      case 6:
        return 'six-column';

      default:
        return 'three-column';
  }
}

function placement(id) {
  if (id >= 0)
    return id % 2 === 0 ? 'right' : 'left';
  return null;
}
