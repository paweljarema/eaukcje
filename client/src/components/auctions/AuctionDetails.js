import React, { Component } from 'react';
import { Redirect, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import * as auctionActions from '../../actions/auctionActions';
import * as photosActions from '../../actions/photosActions';
import * as otherUserActions from '../../actions/otherUserActions';
import * as viewsActions from '../../actions/viewsActions';

import RawImage from './RawImage';
import Pay from './Pay';
import LikeAuction from './Like';
import AuctionBids from './Bids';
import Violations from './Violations'
import { Seller, AskSeller, Deliveries, Opinions } from '../OtherUser';
import Progress, { ImageProgress } from '../Progress';

import sizeMe from 'react-sizeme';
import { IMAGE_ASPECT_RATIO } from './constants';

import PriceHelper from '../../helpers/priceHelper';
import AuctionEndHelper from '../../helpers/auctionEndHelper';

import { Helmet } from 'react-helmet';
import { getUnits, isBidder, isNotEmpty, pluralize, auctionCat } from './functions';
import { auctionLd, thisWasRendered } from '../../jsonld/schemas';
import { concatClasses } from '../../functions/style';
import {
  friendlyAuctionLink,
  auctionTimestampFromMatchParams,
  matchParamsAreDifferent,
  auctionPhotoLink
} from '../../functions/links';
import { LOGIN_PATH, PROFILE_PATH, PAGE_404_PATH } from '../../constants/urls';

import Observer, { PARAMS_IN_BREADCRUMBS } from '../../helpers/eventHelper';
import business from '../../constants/business';

import './AuctionDetails.css';
import './AuctionDetailsMobile.css';

const { REGULATION_BID_EXCERPT } = require('../../documents/regulations');

class BigPhoto extends Component {
    constructor(props) {
        super(props);
        this.state = { enlarge: false };
    }

    toggleEnlarge() {
        this.setState(({ enlarge }) => ({ enlarge: !enlarge }));
    }

    render() {
        const { photo, height, title } = this.props;

        return (
            <div className={"photo-big" + (this.state.enlarge ? ' enlarge' : '')} style={ height ? { height } : null } onClick={ this.toggleEnlarge.bind(this) }>
                <div className="image-wrapper">
                    {
                        photo
                        ?
                        <RawImage data={ photo } title={ title } />
                        :
                        <div><ImageProgress /></div>
                    }
                </div>
            </div>
        );
    }
}

class AuctionDetails extends Component {
    constructor(props) {
        super(props);
        this.state = { auction: '', photo: 0, pay: false, views: 0, activeViews: 0 };
        this.seePhoto = this.seePhoto.bind(this);
        this.buyNow = this.buyNow.bind(this);
        this.submit = this.submit.bind(this);
        this.payCallback = this.payCallback.bind(this);
        this.fetchTotalViews = this.fetchTotalViews.bind(this);
        this.fetchViews = this.fetchViews.bind(this);

        this.paramNotifier = new Observer();
    }

    componentDidMount() {
        // const auction_id = this.props.match.params.id;
        // const auction_timestamp = auctionTimestampFromMatchParams(this.props.match.params);
        const
          params = (this.props.match || {}).params,
          paramCount = Object.keys(params || {}).length || 0,
          shortid = params.shortid;

        this.paramNotifier.trigger(PARAMS_IN_BREADCRUMBS, paramCount - 1); // just shortid

        this.props.clearOtherUser();
        this.props.clearAuction();
        this.props.clearPhotos();
        this.props.fetchAuctionViaShortid(shortid);
        this.props.fetchPhotosViaShortid(shortid);

        this.props.registerViewViaShortid(shortid)
        .then(() => this.fetchViews(shortid));

        this.props.addViewViaShortid(shortid)
        .then(() => this.fetchTotalViews(shortid));
    }

    fetchTotalViews(shortid) {
        fetch(`/api/get_total_views_via_shortid/${ shortid }`)
            .then(res => res.json())
            .then(data => this.setState({ views: data.views }));
    }

    fetchViews(shortid) {
        fetch(`/api/views_via_shortid/${ shortid }`)
            .then(res => res.json())
            .then(data => this.setState({ activeViews: data.viewers }));
    }

    componentWillUnmount() {
        this.props.unregisterViewViaShortid(this.props.match.params.shortid);
        this.paramNotifier.trigger(PARAMS_IN_BREADCRUMBS, 0);
        this.paramNotifier = null;
    }

    componentWillReceiveProps(props) {
        if (props.auctions) {
            if (!this.state.auction || String(props.auctions._id) !== String(this.state.auction._id)) {
                this.setState({ auction: props.auctions });
                this.props.fetchOtherUser(props.auctions._user);
            }
        }
        if (matchParamsAreDifferent(props.match.params, this.props.match.params)) {
            this.setState(
              { auction: null },
              () => this.props.fetchAuctionViaShortid(
                props.match.params.shortid
              ));
        }
    }

    seePhoto(index) {
        //this.setState(prev => ({ photo: prev.photo <= index ? index + 1 : index }));
        this.setState({ photo: index });
    }

    handleTab(event) {
        event.preventDefault();
        const link = event.target;
        const tab = document.getElementById(link.href.split('#')[1]);
        const view_area = tab.parentNode;

        if (link.className.indexOf('active') === -1) {
            let prev = document.querySelectorAll('.tab-view .active');
            for (let i = 0, l = prev.length; i < l; i++) {
                prev[i].className = prev[i].className.replace(/\s*active/g, '');
            }

            link.className += 'active';
            tab.className += 'active';
        }

        tab.scrollIntoView({behavior: 'smooth', block: 'center'});
    }

    buyNow(event) {
        if (event)
          event.preventDefault();

        const user = this.props.user;
        const confirmed = userIsConfirmed(user);

        if (!user) {
            alert('Zaloguj się, aby kupić przedmiot.');
            this.props.history.push(LOGIN_PATH);
            return;
        }

        if (!user.security || !user.security.verified) {
          alert('Aby licytować, zweryfikuj adres email');
          // this.props.history.push(HOME);
          return;
        }

        if (!confirmed) {
            alert('Aby kupić przedmiot, musisz uzupełnić swoje dane w ustawieniach.');
            this.props.history.push(PROFILE_PATH)
            return;
        }

        const auction = this.state.auction;
        const reply = window.confirm(REGULATION_BID_EXCERPT + ` CZY CHCESZ KUPIĆ ${auction.title} ZA ${auction.price.buy_now_price} zł ?`);
        if (reply) {
            this.props
                .buyNow(auction._id)
                .then(() => {
                 // const timestamp = auctionTimestampFromMatchParams(this.props.match.params);
                 this.props
                  .fetchAuctionViaShortid(this.props.match.params.shortid)
                  .then(() => {
                    const socket = this.props.socket;
                    socket.emit('message_user', user._id);
                    socket.emit('message_user', auction._user);
                    this.setState({ auction: this.props.auctions })
                  });

                // const auction_id = this.props.match.params.id;
                // this.props
                //     .fetchAuction(auction_id)
                //     .then(() => { this.setState({ auction: this.props.auctions }); });
                });
        }
    }

    payCallback(result) {
      if (result) {
        const
          shortid = this.props.match.params.shortid,
          { clearAuction, fetchAuctionViaShortid } = this.props;

        this.setState(
          { auction: '', pay: false },
          () => fetchAuctionViaShortid(shortid)
        );
      } else {
        this.setState({ pay: false });
      }
    }

    submit(event) {
        event.preventDefault();

        const user = this.props.user;
        const confirmed = userIsConfirmed(user);
        const auction = this.state.auction;
        const formData = new FormData(this.formBidRef);
        const bid_value = Number(this.bidInputRef.value);

        if (!user) {
            alert('Aby wziąć udział w licytacji musisz się zalogować.');
            this.props.history.push(LOGIN_PATH);
            return;
        }

        if (!user.security || !user.security.verified) {
          alert('Aby licytować, zweryfikuj adres email');
          // this.props.history.push(HOME);
          return;
        }

        if (!confirmed) {
            alert('Aby wziąć udział w licytacji, musisz uzupełnić swoje dane w ustawieniach.');
            this.props.history.push(PROFILE_PATH)
            return;
        }

        const confirmation = window.confirm(REGULATION_BID_EXCERPT + '\n' + 'CZY CHCESZ PRZYSTĄPIĆ DO LICYTACJI?');
        if (!confirmation) return;

        if (!bid_value) {
            alert('Podaj stawkę licytacji');
            return;
        }

        if (bid_value <= auction.price.current_price || bid_value < auction.price.start_price) {
            alert('Musisz przebić obecną stawkę');
            return;
        }

        if (bid_value > auction.price.current_price + 10 && auction.bids && auction.bids.length && auction.bids[0]._user === String(this.props.user._id)) {
            const reply = window.confirm('Już prowadzisz w licytacji. Napewno chcesz podbić cenę ?');
            if (!reply) return;
        }

        window.scrollTo(0, 0);
        this.props.showSpinner();
        this.props
            .postBid(auction._id, formData)
            .then(() => {
                const shortid = this.props.match.params.shortid;
                this.props
                    .fetchAuctionViaShortid(shortid)
                    .then(() => { this.setState({ auction: this.props.auctions }); });
                });
    }

    render() {
        const { auction, pay } = this.state;

        if (auction.error) return null;
        //<Redirect to={ PAGE_404_PATH } />;

        const { bidders } = auction || { bidders: {} };
        const { user, other_user, photos, categories } = this.props;

        const active_photo = this.state.photo;
        //const thumbnails = auction ? auction.photos.slice(0, active_photo).concat(auction.photos.slice(active_photo + 1)) : null;
        //const thumbnails = photos ? photos.slice(0, active_photo).concat(photos.slice(active_photo + 1)) : null;
        const thumbnails = photos && photos.length > 1 ? photos : null;

        //const buy_now = auction ? !auction.ended && user._id !== auction._user && auction.price.buy_now_price && auction.price.buy_now_price >= auction.price.current_price && auction.bids.filter(bid => bid._user === user._id).length : false;
        const buy_now = auction ? !auction.ended && user._id !== auction._user && auction.price.buy_now_price : false; //&& auction.price.buy_now_price >= auction.price.current_price && auction.bids.filter(bid => bid._user === user._id).length : false;
        const min_price = auction ? !auction.ended && auction.price.min_price : false;
        const hide_min_price = auction.price && auction.price.hide_min_price;
        const payee = auction ? auction.payees && auction.payees.indexOf(user._id) !== -1 : false;
        const buy_now_payee = auction ? auction.buynowpayees && auction.buynowpayees.indexOf(user._id) !== -1 : false;
        const count = buy_now_payee ? auction.buynowpayees.filter(id => String(id) === String(user._id)).length : 1;

        const current_price = auction ? auction.price.current_price || auction.price.start_price : false;
        const min_price_reached = min_price >= 0 && min_price <= current_price;
        const extended_view = true || window.innerWidth > 1579;
        const iAmBidder = isBidder(user);
        const isPremium = auction && auction.premium ? auction.premium.isPremium : false;
        // const premium = auction ? Boolean(auction.premium) : false; // legacy auctions
        const premiumForever = isPremium ? auction.premium.forever : false;

        const
            viewers = this.state.activeViews,
            personArr = ['osoba', 'osoby', 'osób'];

        // if (auction) {
        //   console.log('AUCTION', auction);
        //   console.log('owner', other_user);
        //   console.log('user', user);
        //   console.log(auction._user === other_user._id);
        // }

        // thisWasRendered(this);

        return (
            <div className="AuctionDetails">
                {
                    !auction ? <Progress /> : (
                        <div className="auction-view">
                          <Helmet>
                            <title>{ auction.title }</title>
                            <meta name="description" content={ auction.shortdescription } />
                            <meta property="og:type" content="website" />
                            <meta property="og:title" content={ auction.title } />
                            <meta property="og:description" content={ auction.shortdescription } />
                            <meta property="og:url" content={ `${ business.host }${ friendlyAuctionLink(auction) }` } />
                            <meta property="og:image" content={ auctionPhotoLink(auction) } />
                            <meta property="og:image:width" content="1024" />
                            <meta property="og:image:height" content="752" />

                            <link rel="canonical" href={`${ business.host }${ friendlyAuctionLink(auction) }`} />
                          </Helmet>

                          {
                            auctionLd(auction, other_user)
                          }
                            {
                                pay && <Pay user={ user } auction={ auction } callback={ this.payCallback } />
                            }
                            <div className="basic-info">
                                <div className="photos">
                                    <BigPhoto
                                      photo={ photos[active_photo] }
                                      onSize={ (size) => this.setState({ photoHeight: size.width / IMAGE_ASPECT_RATIO }) }
                                      height={ this.state.photoHeight }
                                      title={ `${ auction.title }${ auctionCat(auction) }` }
                                    />
                                    <div className="photos-small">
                                        {
                                            thumbnails && thumbnails.length
                                            ?
                                            thumbnails.map((thumb, index) => (
                                                <div
                                                  key={"thumbnail_" + index}
                                                  className={ concatClasses("auction-details__thumbnail-wrapper", active_photo === index ? 'active' : '') }
                                                  onClick={() => this.seePhoto(index)}>

                                                    <RawImage data={thumb} title={ `Thumbnail ${ auction.title }` } />
                                                </div>
                                            ))
                                            :
                                            null
                                        }
                                    </div>
                                </div>

                                <div className="text">
                                    <div className="content">
                                        { isPremium && <figure className="premium-tag">Aukcja Wyróżniona</figure> }
                                        <h1>{ auction.title }</h1>
                                        <p>{ auction.shortdescription }<LikeAuction user={user} auction={auction} /></p>
                                        <p className="attribute-tags">
                                            <span className="attribute">Ilość<span>{ auction.quantity }</span></span>
                                        </p>
                                        <div className={ (auction.ended ? 'transparent' : '') }>
                                            <div className="price">Aktualna cena: <span className="value">{ PriceHelper.write(current_price) }</span></div>

                                            <div><span className="time-span">do końca { AuctionEndHelper(auction.date) }</span></div>
                                            {
                                              min_price
                                              ?
                                              hide_min_price
                                              ?
                                              min_price_reached
                                              ?
                                              null
                                              :
                                              <div className="min-price">
                                                Cena minimalna nie została osiągnięta.
                                              </div>
                                              :
                                              <div className="min-price">Cena minimalna:
                                                <span className="price-value">
                                                  { PriceHelper.write(auction.price.min_price) }
                                                </span>
                                                <br /><br /><br />
                                              </div>
                                              :
                                              null
                                            }
                                            {
                                                auction._user !== user._id && (<form ref={ (e) => this.formBidRef = e } method="post">
                                                    <span className="price-input-wrapper">
                                                      <input
                                                        ref={ (e) => this.bidInputRef = e }
                                                        name="bid"
                                                        type="number"
                                                        lang="pl"
                                                        placeholder="Kwota licytacji"
                                                        defaultValue={ Math.round(current_price) + 1 }
                                                      />
                                                    </span>
                                                    <button
                                                      type="submit"
                                                      onClick={this.submit}
                                                      disabled={auction.ended === true}>

                                                      <i className="material-icons">gavel</i>Licytuj

                                                    </button>
                                                    {
                                                      buy_now && auction.price.buy_now_price > current_price ? (
                                                        <div className="buy-now">
                                                          <button
                                                            className="standard-button"
                                                            onClick={ this.buyNow }>

                                                            * Kup teraz za<br/>
                                                            <span className="price-value">
                                                              {
                                                                PriceHelper.write(auction.price.buy_now_price)
                                                              }
                                                            </span>
                                                          </button>
                                                        </div>
                                                      ) : null
                                                    }
                                                </form>)
                                            }

                                            <div style={{ margin: '26px 0 20px 0' }}>
                                              <AskSeller
                                                user={ user }
                                                auction={ auction }
                                                socket={ this.props.socket }
                                              />
                                            </div>
                                        </div>
                                        {
                                            auction.ended && <div className="end-tag">Aukcja Zakończona</div>
                                        }
                                        {
                                            (payee || buy_now_payee) && (
                                              <div className="pay">
                                                <br />Kupiłeś ten przedmiot.
                                                <button
                                                  className="standard-button"
                                                  onClick={() => this.setState({ pay: true })}
                                                  style={{ padding: '0 26.5px' }}>

                                                    Zapłać {payee ? PriceHelper.write(current_price) : PriceHelper.write(auction.price.buy_now_price * count)} zł
                                                </button>
                                              </div>
                                            )
                                        }

                                        <span className="auction-views">
                                          <span>Aukcję obserwuje { pluralize(auction.likes, personArr) }</span>
                                          <span>Wszystkie wyświetlenia: { this.state.views }</span>
                                          <span>Aktualne wyświetlenia: { pluralize(viewers, personArr) }</span>
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div className="under-actions">
                              <Violations
                                user={ user }
                                auction={ auction }
                              />
                            </div>

                            <div className="tab-view">
                                <div>
                                <div className="tab-controls">
                                    <a className="active" href="#description" onClick={this.handleTab}>Opis</a>
                                    <a href="#properties" onClick={ this.handleTab }>Cechy</a>
                                    <a href="#bids" onClick={this.handleTab}>Licytacja</a>
                                    <a href="#seller" onClick={this.handleTab}>Sprzedawca</a>
                                    <a href="#opinions" onClick={this.handleTab}>Opinie</a>
                                    <a href="#shipping" onClick={this.handleTab}>Dostawa</a>
                                </div>
                                <div className="tab-content-area">
                                    <div id="bids">
                                        <AuctionBids user={user} auction={auction} bidders={bidders} />
                                        <div className="background">
                                            <i className="material-icons">gavel</i>
                                        </div>
                                    </div>
                                    <div className="active" id="description">
                                        {
                                            auction.description && auction.description.length > 20 ?
                                            <div className="html-description" dangerouslySetInnerHTML={{ __html: auction.description }}></div>
                                            :
                                            <div className="no-result">
                                                <i className="material-icons">sentiment_dissatisfied</i>
                                                <h2>Brak dodatkowych informacji</h2>
                                                <p>Sprzedawca nie dodał opisu.<br />Zadaj pytanie o przedmiot w zakładce 'Sprzedawca'</p>
                                            </div>
                                        }
                                        <div className="background">
                                            <i className="material-icons">description</i>
                                        </div>
                                    </div>
                                    <div id="properties">
                                      <div style={{ padding: 20 }}>
                                        <h3>Cechy przedmiotu</h3>
                                        <p className="properties__attribute-tags attribute-tags">
                                            <span className="attribute">Ilość<span>{ auction.quantity }</span></span>
                                            {
                                                isNotEmpty(auction.properties) && auction.properties.map(attr => (
                                                    attr.value
                                                    ?
                                                    <span
                                                      key={`${attr.name}_${attr.value}`}
                                                      className="attribute">

                                                        {attr.name}
                                                        <br />
                                                        <span>{ displayPropValue(attr.value) }</span>
                                                    </span>
                                                    :
                                                    null
                                                ))
                                            }
                                            {
                                                isNotEmpty(auction.int_properties) && auction.int_properties.map(attr => (
                                                    typeof attr.value === 'number'
                                                    ?
                                                    <span
                                                      key={`${attr.name}_${attr.value}`}
                                                      className="attribute">

                                                        {attr.name}
                                                        <br/><br/>
                                                        <span>{ attr.value } <span className="unit">{ getUnits(attr.name) }</span></span>
                                                    </span>
                                                    :
                                                    null
                                                ))
                                            }
                                        </p>
                                      </div>

                                      <div className="background">
                                          <i className="material-icons">assignment_turned_in</i>
                                      </div>
                                    </div>
                                    <div id="opinions">
                                        <Opinions other_user={other_user}/>
                                        <div className="background">
                                            <i className="material-icons">star_outline</i>
                                        </div>
                                    </div>
                                    <div id="seller">
                                        <Seller auction={ auction } user={ user } socket={ this.props.socket } showAllData={ iAmBidder } />
                                        <div className="background">
                                            <i className="material-icons">account_circle</i>
                                        </div>
                                    </div>
                                    <div id="shipping">
                                        <Deliveries auction={ auction } />
                                        <div className="background">
                                            <i className="material-icons">local_shipping</i>
                                        </div>
                                    </div>
                                </div>
                                </div>

                                    {
                                        extended_view && (<div className="extra-tab">
                                             <AuctionBids user={user} auction={auction} bidders={bidders} />
                                        </div>)
                                    }

                            </div>
                        </div>
                    )
                }
            </div>
        );
    }
}

BigPhoto = sizeMe({ monitorHeight: true })(BigPhoto);;


function userIsConfirmed(user) {
  if (!user) return false;
  return user.firstname && user.lastname && user.address && user.address.street && user.address.postal && user.address.city && user.contact && user.contact.email;
}

function displayPropValue(value) {
  if (Array.isArray(value)) {
    let len = value.length;

    //return value.join(', ');
    return value.map((val, i) => <span key={`val_${i}`}> { val }{ comma(i, len) } </span>);
  } else
    return value;
}

function comma(i, len) {
  if (len === 1 || i === len - 1) return null;
  else return ', ';
}

function mapAuctionsPhotosUserAndOtherUserStateToProps({ auctions, photos, user, other_user }) {
    return { auctions, photos, user, other_user };
}

AuctionDetails = connect(mapAuctionsPhotosUserAndOtherUserStateToProps, {...auctionActions, ...photosActions, ...otherUserActions, ...viewsActions})(AuctionDetails);
export default AuctionDetails;
