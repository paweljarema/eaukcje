import React, { Component } from 'react';
import { BACKGROUND_SIZE } from '../../constants/display';
import { auctionCat } from './functions';

import './RawImage.css';

class RawImage extends Component {
    componentDidMount() {
        setTimeout(() => { if (this.imgRef) this.imgRef.style.opacity = 1; } , 310);
    }

    render() {
        const { data, link, blob, backgroundSize, title } = this.props;

        if (data) {
          return(
            <img
              ref={(e) => this.imgRef = e}
              className="image absolute-center img-image-block"
              src={ `data:${data.type || 'image/jpeg'};base64,${data.data}` }
              style={{ objectFit: BACKGROUND_SIZE }}
              alt={ title || 'Zdjęcie główne' }
            />
          );
            // return (
            //   <div
            //     ref={(e) => this.imgRef = e}
            //     className="image absolute-center div-image-block"
            //     style={{
            //       borderStyle: 'none',
            //       backgroundImage: `url(data:${data.type || 'image/jpeg'};base64,${data.data})`
            //     }}
            //     alt={ title || 'Zdjęcie główne' }
            //   />
            // );
        } else if (link) {
            return (
              <img
                ref={(e) => this.imgRef = e}
                className="image absolute-center img-image-block"
                src={`/auction/${link._id}/photo`}
                style={{ objectFit: BACKGROUND_SIZE }}
                alt={ `${ link.title }${ auctionCat(link) }` }
              />
            );
            // return (
            //   <div
            //     ref={(e) => this.imgRef = e}
            //     className="image absolute-center div-image-block"
            //     style={{ borderStyle: 'none', backgroundImage: `url(/auction/${link._id}/photo)`, backgroundSize: (backgroundSize || 'cover') }}
            //     alt={ `${ link.title }${ auctionCat(link) }` }
            //   />
            // );
        } else if (blob) {
            return (
              <img
                ref={(e) => this.imgRef = e}
                className="absolute-center img-image-block"
                src={ blob }
              />
            );
        }

        return null;
    }
}

export default RawImage;
