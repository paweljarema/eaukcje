import React, { Component } from 'react';
import CategoryFilters, { CategoryInfo } from '../CategoryFilters';
import Filters, { FilterWrapper } from '../Filters';

import { Helmet } from 'react-helmet';
import { FilteredList } from '../Auctions';
import { catPathToArray, catDataFromPath } from '../../functions/links';
import { withRouter } from 'react-router-dom';
import { PROMOTED_AUCTIONS_PATH, NEW_AUCTIONS_PATH } from '../../constants/urls';
import { DEFAULT, NEW, PROMOTED } from '../../constants/sort';
import business from '../../constants/business';

import Observer from '../../helpers/eventHelper';

let prevPath = '';

export default withRouter(class AuctionListSearch extends Component {
    constructor(props) {
        super(props);
        this.state = { page: 1, pages: 1, per_page: 10, folded: false };

        this.setPage = this.setPage.bind(this);
        this.setPages = this.setPages.bind(this);
        this.perPageCallback = this.perPageCallback.bind(this);
        this.sortCallback = this.sortCallback.bind(this);
        this.markCategories = this.markCategories.bind(this);

        this.filterButtonObserver = new Observer();

        let sort = '';
        switch(this.props.location.pathname) {
          case PROMOTED_AUCTIONS_PATH:
            sort = PROMOTED;
            break;
          case NEW_AUCTIONS_PATH:
            sort = NEW;
            break;
          default:
            sort = NEW;
        }

        this.state.sort = sort;

        this.onLocationChange(this.props.location);
    }

    componentWillUpdate(nextProps, nextState) {
      this.markCategories(nextProps);
      if (nextProps.location.search !== this.props.location.search ||
          nextProps.location.pathname !== this.props.location.pathname)
            this.onLocationChange(nextProps.location);
    }

    onLocationChange = (location) => {
      if (!!location && !!location.search) {
        const { search } = location;
        const obj = search.slice(1).split('&').reduce((store, kv) => {
          const keyVal = kv.split('=');
          const key = keyVal[0];
          const val = keyVal[1];
          store[key] = val;
          return store;
        }, {});

        this.state.page = obj.page || 1;
        this.state.per_page = obj.per_page || 10;
      }
    }

    setPage(page) {
      this.setState({ page });
    }

    setPages(pages) {
      if (pages >= this.state.page) {
          this.setState({ pages });
      } else {
          this.setState({ page: pages, pages });
      }
    }

    perPageCallback(per_page) {
        this.setState({ per_page });
    }

    sortCallback(sort) {
        this.setState({ sort });
    }

    componentDidMount() {
      this.markCategories();
    }

    componentWillUnmount() {
      if (this.filterButtonObserver) {
        this.filterButtonObserver = null;
      }
    }

    markCategories(props) {
      props = props || this.props;

      let { path } = props.match;

      if (path !== prevPath) {
        prevPath = path;

        this.props.categoryCallback({
          categories: catPathToArray(
            path,
            this.props.categories
          ),
          urlchange: Date.now()
        });
      }
    }

    render() {
        const
          { page, pages, per_page, sort } = this.state,
          { user, categories, match, query, setQuery, category, categoryData, categoryCallback, windowWidth } = this.props,
          { path } = match || {},

          onMobile = windowWidth <= 1295, // TODO change if css breakpoint changes

          { _categories, _active_category, _active_count } = catDataFromPath(path, categories);

        return (
            <div className="AuctionListSearch">
              <Helmet>
                <link rel="canonical" href={ `${ business.host }${ window.location.pathname }` } />
              </Helmet>

              <FilterWrapper onMobile={ onMobile }>
                <Filters
                  match={ this.props.match }
                  page={ page }
                  pages={ pages }
                  per_page={ per_page }
                  query={ query }
                  setQuery={ setQuery }
                  category={ category }
                  user={ user }
                  categories={ categories }
                  categoryData={ categoryData }
                  categoryCallback={ categoryCallback }
                  filterButtonObserver={ this.filterButtonObserver }
                  sort={ sort }
                />

                <CategoryFilters
                  activeCategory={ _active_category }
                  categories={ categories }
                  categoryCallback={ categoryCallback }
                  category={ category }
                  categoryData={ categoryData }
                  filterButtonObserver={ this.filterButtonObserver }
                  windowWidth={ windowWidth }
                />
              </FilterWrapper>

              <div className="list-search-with-categories">

                <CategoryInfo
                  categories={ _categories }
                  activeCategory={ _active_category }
                  categoryCount={ _active_count }
                />

                <FilteredList
                  page={ page }
                  pages={ pages }
                  per_page={ per_page }
                  sort={ sort }
                  setPage={ this.setPage }
                  setPages={ this.setPages }
                  perPageCallback={ this.perPageCallback }
                  sortCallback={ this.sortCallback }
                />

              </div>
            </div>
        );
    }
})
