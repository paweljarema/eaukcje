import React from 'react';
import { connect } from 'react-redux';
import * as violationActions from '../../actions/violationActions';

import './Violations.css';

import { VIOLATIONS } from '../../constants/violations';

import axios from 'axios';

export default connect(null, violationActions)(
  class extends React.Component {
    constructor(props) {
      super(props);

      this.state = { report: false };

      this.toggleReport = this.toggleReport.bind(this);
      this.submitReport = this.submitReport.bind(this);
      this.handleInput = this.handleInput.bind(this);
    }

    toggleReport() {
      const
        { user } = this.props,
        { report } = this.state;

      if (!user) {
        alert('Aby zgłosić naruszenie musisz być zalogowany');
        return;
      }

      if (!report) {
        this.areaRef.scrollIntoView({ block: 'center', behavior: 'smooth' });
      }

      this.setState({ report: !report });
    }

    handleInput(e) {
      let { name, value } = e.target;
      this.setState({ [ name ]: value });
    }

    submitReport() {
      const
        REASON_MIN_LENGTH = 15,
        REASON_MAX_LENGTH = 160,

        { auction, user } = this.props,
        { reason, group } = this.state;

      if (!reason || !group) {
        alert('Prosimy dokładnie wypełnić zgłoszenie');
        return;
      }

      if (reason.length < REASON_MIN_LENGTH) {
        alert('Maksymalna dopuszczalna ilość znaków to ' + REASON_MIN_LENGTH + '. Skróć opis i spróbuj ponownie.');
        return;
      }

      if (reason.length > REASON_MAX_LENGTH) {
        alert('Opis naruszenia jest za długi.');
        return;
      }

      this.props.postViolation({
        _user: user._id,
        _auction: auction._id,

        reason: reason,
        group: group
      })
      .then(() => {
        this.toggleReport();
        window.scrollTo({ top: 0, behavior: 'smooth' });
      });
    }

    render() {
      const
        { auction, user = {} } = this.props,
        { report } = this.state,

        loaded = auction && auction._user !== user._id;

      if (!loaded) return null;

      return (
        <div className="report-violation-action" ref={ (e) => this.areaRef = e }>
          <div className="report-violation-button">
            <button className="message standard-button" onClick={ this.toggleReport }>
              <i className="material-icons">{ ( report ? 'close' : 'flag' ) }</i>
              Zgłoś naruszenie
            </button>
          </div>

          {
            report && (
              <div className="report-violation-dialog">
                <div>
                  <label htmlFor="group">Natura problemu</label>
                  <select name="group" onChange={ this.handleInput }>
                    {
                      VIOLATIONS.map(text => (
                        <option key={ text } value={ text }>
                          { text }
                        </option>
                      ))
                    }
                  </select>
                </div>

                <div>
                  <label htmlFor="reason">Opisz naruszenie</label>
                  <textarea name="reason" onChange={ this.handleInput } />
                </div>

                <div>
                  <button className="clickable" onClick={ this.submitReport }>
                    Zgłoś <i className="material-icons">send</i>
                  </button>
                </div>
              </div>
            )
          }
        </div>
      );
    }
  }
);
