import React, { Component } from 'react';
import Auction from './Auction';
import { Swipeable } from 'react-swipeable';

import './Carousel.css';
import './CarouselMobile.css';

const
  LEFT = 'left',
  RIGHT = 'right',

  SMALL_AUCTION_VIEW_SIZE = 8;

export default class extends Component {
  constructor(props) {
    super(props);

    let viewSize = getViewSize(props.windowWidth);

    this.state = {
      inView: [0, viewSize],
      lastShift: null
    };

    this.shift = this.shift.bind(this);
    this.left = this.left.bind(this);
    this.right = this.right.bind(this);
  }

  componentWillUpdate(nextProps, nextState) {
    if (this.props.windowWidth !== nextProps.windowWidth) {
      this.setState({ inView: [ 0, getViewSize(nextProps.windowWidth) ] });
    }
  }

  left() {
    this.shift(LEFT);
  }

  right() {
    this.shift(RIGHT);
  }

  shift(direction) {
    let
      { inView } = this.state,
      [ a, b ] = inView,

      auctions = this.props.auctions.length,
      viewSize = Math.min(getViewSize(this.props.windowWidth), auctions),

      deltaA, deltaB;

    switch(direction) {
      case LEFT:
        deltaA = a - viewSize;
        deltaB = b - viewSize;

        if (deltaA < 0) {
          deltaA = auctions - viewSize;
          deltaB = auctions;
          // deltaA = 0;
          // deltaB = viewSize;
        }

        break;

      case RIGHT:
        deltaA = a + viewSize;
        deltaB = b + viewSize;

        if (deltaB > auctions) {
          deltaA = 0;
          deltaB = viewSize;
          // deltaB = auctions;
          // deltaA = deltaB - viewSize;
        }

        break;
    }

    this.setState({ inView: [ deltaA, deltaB ], lastShift: direction });
  }

  render() {
    const
      { auctions, windowWidth } = this.props,
      { inView, lastShift } = this.state,

      mainView = inclusive(inView, auctions),
      bottomView = exclusive(inView, auctions),

      canShiftLeft = true, //inView[0] > 0,
      canShiftRight = true, //inView[1] < auctions.length,

      shiftRight = this.right, //canShiftRight ? this.right : undefined,
      shiftLeft = this.left; //canShiftLeft ? this.left : undefined;


    return (
      <div key={ `_${ inView[0] }${ inView[1] }` } style={{ position: 'relative' }}>
          <div className="auction-carousel-control left" onClick={ shiftLeft } disabled={ !canShiftLeft }>
            <i className="material-icons">chevron_left</i>
          </div>

          <Swipeable
  					trackMouse
  					preventDefaultTouchmoveEvent
  					onSwipedLeft={ shiftLeft }
  					onSwipedRight={ shiftRight }>

            <div style={{ position: 'relative' }} className={ `four-column auction-carousel ${ lastShift ? `shift shift-${ lastShift }` : '' }` }>
              {
                mainView.map((auction, index) => auctionJsx(auction))
              }
            </div>

          </Swipeable>

          <div className="auction-carousel-control right" onClick={ shiftRight } disabled={ !canShiftRight }>
            <i className="material-icons">chevron_right</i>
          </div>

          {
            windowWidth > 1350
            ?
            <div className="eight-column">
              {
                  bottomView.map((auction, index) => auctionJsx(auction))
              }
            </div>
            :
            null
          }
      </div>
    );
  }
};

function auctionJsx(auction) {
  return (
    <div key={ auction._id } className="column">
      <Auction auction={ auction } />
    </div>
  );
}

function inclusive(indexArr, auctions) {
  let [ a, b ] = indexArr;
  return auctions.slice(a, b);
}

function exclusive(indexArr, auctions) {
  let [ a, b ] = indexArr;
  if (a === 0) return auctions.slice(b);
  else return (
    auctions
      .slice( 0, a )
      .concat(
        auctions.slice( b )
      ).slice(0, SMALL_AUCTION_VIEW_SIZE)
  );
}

function getViewSize(screen) {
  if (screen <= 1000) return 1;
  if (screen <= 1250) return 2;
  if (screen <= 1500) return 3;
  return 4;
}
