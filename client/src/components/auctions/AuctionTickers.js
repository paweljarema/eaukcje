import React, { Component } from 'react';
import AuctionTicker from './AuctionTicker';

const TICK_INTERVAL = 10000;

export default class extends Component {
  constructor(props) {
    super(props);

    this.state = { size: getSize(props.windowWidth) };
  }

  componentWillUpdate(nextProps) {
    if (this.props.windowWidth !== nextProps.windowWidth) {
      this.setState({ size: getSize(nextProps.windowWidth) });
    }
  }

  render() {
    const
      { categories } = this.props,
      mainCategories = categories.map(cat => cat.name);

    if (!mainCategories) return null;
    else return (
      mainCategories.map((category, i) => (
        <AuctionTicker
          key={ category }
          category={ category }
          options={{
            size: this.state.size,
            limit: 9,
            category: category,
            sliderArea: 50,
            // placement: i % 2 === 0 ? 'left' : 'right',
            background: `${ '_' + linuxNaming(category) }.png`,
            index: i,
            tick: true,
            interval: TICK_INTERVAL
          }}
        />
      ))
    );
  }
}

function getSize(width) {
  if (width >= 1050) return 3;
  if (width >= 835) return 2;
  return 1;
}

function linuxNaming(string) {
  return string
    .split(' ')
    .map((s, i) => i === 0 ? s : (s || '').toLowerCase())
    .join('_');
}
