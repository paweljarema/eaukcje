import React from 'react';
import Carousel from './carousel/GenericCarousel';

import './Ads.css';

const ADS = ['dostawczaki', 'samochody', 'polmarket', 'okazja'];

function linkFor(ad) {
  return `https://${ ad }.pl`;
}

function getSplitSizeFor(w) {
  if (w <= 750) return 1;
  if (w <= 1500) return 2;
  return 4;
}

function splitContent(content, split) {
  if (split === 1) return content;

  let result = [];
  for (let i = 0, l = content.length; i < l; i += split)
    result.push(
      <React.Fragment key={ 'content_' + i }>
        { content.slice(i, i + split) }
      </React.Fragment>
    );

  return result;
}

const Ad = ({ ad }) => {
  return (
    <div className="site-link-wrapper">
      <a href={ linkFor(ad) } target="_blank">
        <div>
          <img className="generic-carousel__img" src={ `/assets/banners/${ ad }.jpg` } alt={ ad + '.pl' } />
        </div>
      </a>
    </div>
  );
}

export default ({ windowWidth }) => {
  let
    count = 1 + parseInt(windowWidth / 400),
    items = ADS
      .map((name, i) => <Ad key={ name } ad={ name } />);

  return (
    <Carousel
      className="generic-carousel__app-offers"
      items={ splitContent(items, getSplitSizeFor(windowWidth)) }
    />
  );
};
