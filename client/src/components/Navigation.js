import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import Logo from './Logo';
import { SearchField } from './Search';
import NewAuctionButton from './auctions/NewAuctionButton';
import DisplayCredits from './DisplayCredits';

import Chat from './Chat';
import Observer, { PARAMS_IN_BREADCRUMBS } from '../helpers/eventHelper';
import { thisWasRendered } from '../jsonld/schemas';

import './NavigationMobile.css';

let isChrome = /chrome/i.test(window.navigator.userAgent);

class UserLinks extends Component {
    render() {
        const
            { user, open, searchHandler, toggleMenu, callback, onMobile } = this.props,
            className = "user-links" + (open ? ' open' : ''),

            AddButton = onMobile ? undefined : <NewAuctionButton onClick={toggleMenu} />,
            Search = (
              <span className="link search-mobile" onClick={ () => { searchHandler(); toggleMenu(); }}>
                <i className="material-icons">search</i>
              </span>
            );

        if (user !== false && user !== null) {
            return (
                <div className={className}>
                    <Link to="/moje-aukcje" onClick={toggleMenu}>Moje aukcje</Link>
                    { Search }
                    <Chat socket={ this.props.socket } id={user._id} callback={callback} onClick={toggleMenu} />
                    <span className="link dropdown-toggle" onClick={toggleMenu}>
                        { isChrome && user.firstname && <span className="orange navigation-welcome">{ user.firstname }</span> }
                        <img src="/assets/icons/user.png" alt="Konto użytkownika" />
                        <div className="dropdown">
                            <Link to="/konto/ustawienia" className="account">Konto</Link>
                            <Link to="/konto/aukcje/dodaj" className="add-auction">Dodaj Aukcję</Link>
                            <a href="/api/logout" className="logout">Wyloguj</a>

                            <DisplayCredits user={ user } />
                        </div>
                    </span>

                    { AddButton }
                </div>
            );
        } else if (user !== null) {
            return (
                <div className={className}>
                    { Search }

                    <Link to="/konto/zaloguj" onClick={toggleMenu}>Zaloguj</Link>
                    <Link to="/konto/zarejestruj" onClick={toggleMenu}>Zarejestruj się</Link>

                    { AddButton }
                </div>
            );
        } else {
            return null
        }
    }
}

class MobileMenu extends Component {
    render() {
        const { open, clickHandler } = this.props;

        return (
            <div className={"MobileMenu" + (open ? ' open' : '')} onClick={clickHandler}>
                <div></div>
                <div></div>
                <div></div>
            </div>
        );
    }
}

function MobileNavFragment({ user, onMobile, query, setQuery, categoryCallback, categoryData }) {
  if (!onMobile) return null;

  return (
    <div className="navigation__mobile-fragment">
      <SearchField
        query={ query }
        setQuery={ setQuery }
        categoryCallback={ categoryCallback }
        categoryData={ categoryData }
        open={ true }
        closeMobile={ undefined }
        searchHandler={ undefined }
      />

      <NewAuctionButton user={ user } onClick={ undefined } />
    </div>
  );
}

class Navi extends Component {
    constructor(props) {
        super(props);
        this.state = { mobile: false, search: true, chatBox: null, navFolded: false };
        this.clickHandler = this.clickHandler.bind(this);
        this.searchHandler = this.searchHandler.bind(this);
        this.closeMobile = this.closeMobile.bind(this);

        this.toggleNavOnScroll = this.toggleNavOnScroll.bind(this);
    }

    closeMobile() {
      if (this.state.mobile)
        this.setState({ mobile: false });
    }

    clickHandler() {
        this.setState(prev => ({ mobile: !prev.mobile }));
    }

    searchHandler() {
        // this.setState(prev => ({ search: !prev.search }));
    }

    toggleNavOnScroll() {
      const
        y = window.scrollY

      if (y > 100 && (this.lastScroll || 0) < y)
        this.setState({ navFolded: true });
      else
        this.setState({ navFolded: false });

      this.lastScroll = y;
    }

    componentDidMount() {
      window.addEventListener('scroll', this.toggleNavOnScroll);
    }

    componentWillUnmount() {
      window.removeEventListener('scroll', this.toggleNavOnScroll);
    }

    render() {
        const { mobile, search, navFolded } = this.state;
        const { query, callback, categoryData, categoryCallback, onMobile } = this.props;
        const style = {
          willChange: 'transform',
          transform: `translateY(${ navFolded ? -350 : 0 }px)`,
          transition: 'transform .3s ease-out',
        };

        // thisWasRendered(this);

        return (
            <nav style={ style }>
                <Logo />
                <SearchField
                  open={ search }
                  query={ query }
                  setQuery={ this.props.setQuery }
                  searchHandler={ this.searchHandler }
                  categoryCallback={ categoryCallback }
                  categoryData={ categoryData }
                  closeMobile={ this.closeMobile }
                />

                <div className="nav-mobile-fragment">
                  { onMobile && <NewAuctionButton onClick={ this.closeMobile } /> }
                  <MobileMenu open={ mobile } clickHandler={ this.clickHandler } />
                </div>

                <UserLinks
                  onMobile={ onMobile }
                  callback={ callback }
                  open={ mobile }
                  socket={ this.props.socket }
                  searchHandler={ this.searchHandler }
                  toggleMenu={ this.clickHandler }
                />

            </nav>
        );
    }
}

class Breadcrumbs extends Component {
    constructor(props) {
        super(props);
        this.state = {
          current_url: '/',
          current_path: [{ link: 'Home', url: '/' }],
          gibberish: 0
        };

        this.getPath = this.getPath.bind(this);
        this.handleGibberish = this.handleGibberish.bind(this);

        this.paramObserver = new Observer();
        this.watch = this.paramObserver.watchEvent(PARAMS_IN_BREADCRUMBS, this.handleGibberish);
    }

    handleGibberish(count) {
      this.setState({ gibberish: count });
    }

    getPath() {
        const root = [{ link: 'Home', url: '/' }];
        const path = window.location.pathname;
        const match = this.props.match || {};
        const params = Object.keys(match.params || {});
        const parts = sliceAway(path, '#')
          .replace(/#_=_|#/g, '')
          .split(/\//i)
          .filter(chunk => isNaN(chunk) || !chunk);

        let endOfReadableParts = this.state.gibberish ? -this.state.gibberish : parts.length;

        const new_parts = parts
          .slice(1, endOfReadableParts)
          .map(
            part => ({
              link: part.slice(0,1).toUpperCase() + part.slice(1),
              url: part
            }));

        this.setState({ current_url: path, current_path: root.concat(new_parts) });
    }

    componentDidMount() {
        this.getPath();
    }

    componentWillUnmount() {
      this.paramObserver.unregister(PARAMS_IN_BREADCRUMBS, this.watch);
      this.paramObserver = null;
    }

    componentDidUpdate(prevProps, prevState) {
        const { current_url, gibberish } = this.state;
        if (current_url !== window.location.pathname || gibberish !== prevState.gibberish) {
            this.getPath();
        }
    }

    render() {
        const current_path = this.state.current_path
          .filter(frag => frag.link.length);

        if (current_path.length < 2) return null;

        return (
            <div className="breadcrumbs">
                {
                    current_path.map(
                        (frag, index) => (
                            <span key={"crumb_" + index}>
                                {
                                    index < current_path.length - 1 ? (
                                        <Link to={ '/' + current_path.slice(1, index + 1).map(p => p.url).join('/') }>
                                            { frag.link }
                                        </Link>
                                    ) : (
                                        <span>{ frag.link.replace('#', '') }</span>
                                    )
                                }
                            </span>
                        )
                    )
                }
            </div>
        );
    }
}

function mapUserStateToProps({ user }) {
    return { user };
}

function sliceAway(string, from) {
  let i = string.indexOf('#');

  if (i !== -1) return string.substring(0, i);
  return string;
}

UserLinks = connect(mapUserStateToProps)(UserLinks);
// Breadcrumbs = withRouter(Breadcrumbs);

export { Navi, MobileNavFragment, MobileMenu, UserLinks, Breadcrumbs };
