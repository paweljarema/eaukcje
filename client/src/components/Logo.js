import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './Logo.css';

class Logo extends Component {
    render() {
        return (
            <Link to="/" className="app-logo-link">
              <div className="logo app-logo">
                <img src="/assets/logo.svg" alt="Logo eaukcje.pl"/>
              </div>
            </Link>
        );
    }
}

export default Logo;
