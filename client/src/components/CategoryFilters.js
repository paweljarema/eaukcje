import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { isSet, isNotEmpty } from './CategoryPicker';
import { InputSingular, InputRange, InputMultiple } from './CategoryInputs';
import { catResource } from './FrontPageCategories';
import { arraysAreDifferent, arrayIsEmpty } from '../functions/array';
import { makeCatLink, makeCatLinkRelative } from '../functions/links';
import { concatClasses } from '../functions/style';
import { FILTER_BUTTON_PRESSED } from '../helpers/eventHelper';

import { Helmet } from 'react-helmet';

import './CategoryPicker.css';
import './CategoryFilters.css';
import './Breadcrumbs.css';

const

	DEFAULT_PROPS = [
		{
			"name": "Cena",
			"type": "Range",
			"unit": "zł",
			"values": []
		}
		// {
		// 	"name": "Stan",
		// 	"type": "Multiple",
		// 	"values": [
		// 		"Używany",
		// 		"Nowy"
		// 	]
		// }
	],

	ALL_CATEGORY_DESCRIPTION = 'Przeglądaj serwis w poszukiwaniu interesujących aukcji korzystając z nawigatora kategorii. Widoczne będą tylko te cechy i kategorie, pod którymi znajdują się czynne aukcje. Naciśnij przycisk Szukaj, aby zobaczyć wynik zapytania.',
	SEARCH_PROPERTIES_ID = 'search-properties';

function checkBreakpoints(windowWidth) {
	if (windowWidth > 920) return 4;
	if (windowWidth > 720) return 3;
	if (windowWidth > 405) return 2;
	return 1;
}

class PropertyInputs extends Component {
	constructor(props) {
		super(props);

		this.state = { focus: null };

		this.focus = this.focus.bind(this);
	}

	focus(inputName) {
		this.setState({ focus: inputName });
	}

	render() {
		let
			{ properties, callback, marka } = this.props,
			{ focus } = this.state;

		if (!isNotEmpty(properties)) {
			return (
				<div id={ SEARCH_PROPERTIES_ID }>
				</div>
			);
			// properties = DEFAULT_PROPS;
		};

		const
			single = [],
			range  = [];

		// zmienimy kolejność
		properties.map(prop => {
			if (prop.type === 'Range') range.push(prop);
			else single.push(prop);
		});

		const propInputs = (props) => {
			if (!props.length) return null;

			return (
				<div className="Properties">
					{
						props
							.filter(prop => prop.name === 'Cena' || prop.activeAuctions)
							.map((prop, i) => {
								const key = prop.name + '_' + i;

								switch(prop.type) {
									case 'Range':
										return <InputRange key={ key } property={ prop } callback={ callback } focus={ focus } setFocus={ this.focus } />
									case 'Multiple':
										return <InputMultiple key={ key } property={ prop } callback={ callback } marka={ prop.name === 'Model' ? marka : null } focus={ focus } setFocus={ this.focus } />
									case 'Singular':
										return <InputSingular key={ key } property={ prop } callback={ callback } focus={ focus } setFocus={ this.focus } />
								}
							})
					}
				</div>
		)};

		return (
			<div id={ SEARCH_PROPERTIES_ID }>
				<h3>Cechy</h3>
				{ propInputs(single) }
				{ propInputs(range) }
			</div>
		);
	}
}

function CatLinkRouter({ style, className, categories, children }) {
	const to = makeCatLinkRelative(categories);

	return (
		<Link
			to={ to }
			style={ style }
			className={ className }
		>

			{ children }

		</Link>
	);
}

function CatLink({ style, className, onClick, children }) {
	return (
		<a
			style={ style }
			className={ className }
			onClick={ onClick }
		>

			{ children }
		</a>
	);
}

function Links({ allCategories, activeCategories, path = [] }) {
	return (
		<div className="category-tree-alt__wrapper">
			{
				allCategories.map(function (cat, i) {
					if (!cat.activeAuctions) return null;

					const
						name = cat.name,
						route = path.concat(name),
						link = makeCatLinkRelative(route),
						active = activeCategories.includes(name),
						subcategories = Boolean(cat.subcategories);

					return (
						<ul
							className={ concatClasses("category-tree-alt__links" , active ? 'active' : '') }
							key={ name }>

							<li className="category-tree-alt__links__link">
								<Link to={ link }>

									{ name }
								</Link>
							</li>

							{
								active && subcategories && (
									<li className="category-tree-alt__links__subcategories">
										<Links
											allCategories={ cat.subcategories }
											activeCategories={ activeCategories }
											path={ route }
										/>
									</li>
								)
							}
						</ul>
					);
				})
			}
		</div>
	);
}

function CategoryNavigationAlt({ categories, data }) {
	return (
		<div className="category-tree-alt">
			<h3>Kategorie</h3>
			<Links
				allCategories={ data } activeCategories={ categories }
			/>
		</div>
	);
}

class CategoryNavigation extends Component {
	render() {
		const
			{ categories, data, level, callback, windowWidth, navigateBack } = this.props,
			isMain = level === 0,
			nextLevel = '',
			squirtle = isMain ? 'main-cat ' : '';

		if (arrayIsEmpty(data)) return null;

		return (
			<div className={ "CategoryTree " + nextLevel }>
				{
					data
						.filter(cat => isMain || cat.activeAuctions)
						.map((cat, i) => {
							let
								isActive = true, // cat.name === categories[level],
								hasAuctions = true, // cat.activeAuctions > 0,
								hasSubcategories = Boolean(cat.subcategories),
								hasChildren = isActive && hasAuctions && hasSubcategories,

								isOnTop = level === categories.length - 1,
								isNonActive = !isActive && !(level === categories.length),

								scrollToSearchProperties = isActive && hasAuctions && !hasSubcategories,

								numColsOnDisplay = checkBreakpoints(windowWidth),
								className = squirtle + cat.name + ( hasChildren ? ' active' : '') + ( isNonActive ? ' non-active' : '' ),
								onClick = isActive
									?
									() => navigateBack(level - 1)
									:
									() => callback(categories, cat.name, level);

							if (scrollToSearchProperties) scrollPropsIntoView();

							return (
								<React.Fragment key={ "cat_" + i }>

									<CatLink
										style={{ order: i }}
										className={ className }
										onClick={ onClick }
									>
										{
											isMain && <img className="extra-small-icon" alt={ cat.name } src={ catResource(cat.name) } />
										}
										{
											cat.name
										}
										{
											hasAuctions && <i className="material-icons">{ (isActive ? 'expand_more' : 'chevron_right') }</i>
										}
									</CatLink>

									{
										hasChildren && (
											<div
												className={ `nested-category-tree-row${ isOnTop ? ' top' : '' }` }
												style={{ order: (Math.floor(i / numColsOnDisplay) + 1) * numColsOnDisplay }}
											>
												<div className="subtitle">
													Zobacz ogłoszenia w kategorii <span>{ cat.name }</span>:
												</div>
												<div className="wrapper">
													<CategoryNavigation
														categories={ categories }
														data={ cat.subcategories } //dataToDisplay }
														level={ level + 1 } //level }
														callback={ callback }
														navigateBack={ navigateBack }
													/>
												</div>
											</div>
										)
									}
								</React.Fragment>
							)
						})
				}
			</div>
		);
	}
}

// class Breadcrumbs extends Component {
// 	render() {
// 		const { categories, navigateBack, activeCategory, categoryCount } = this.props;
//
// 		// if (arrayIsEmpty(categories)) return null;
//
// 		return (
// 			<div>
//
// 				<div className="breadcrumb-navigation" style={{ paddingLeft: 2 }}>
// 					{
// 						!arrayIsEmpty(categories) && <span><a onClick={ () => navigateBack('home')}>Wszystkie</a></span>
// 					}
//
// 					{
// 						categories.map((category, i) => {
// 							let isLast = ( i === categories.length - 1 );
//
// 							return (
// 								<span key={ i + category }>
// 									<a
// 										onClick={ isLast ? null : () => navigateBack(i) }
// 										style={ i === 0 ? { fontWeight: 'bold' } : null }
// 									>
// 										{ category }
// 									</a>
// 								</span>
// 							);
// 						})
// 					}
// 				</div>
//
// 				<CategoryInfo {...{ categories, activeCategory, categoryCount }} />
//
// 			</div>
// 		);
// 	}
// }

function CategoryInfo({ categories, activeCategory, categoryCount }) {

	return (
		<div className="breadcrumb-category-info">
			{
				activeCategory && (
					<Helmet>
						{ activeCategory.metaTitle && <title>{ activeCategory.metaTitle }</title> }
						<meta name="description" content={ activeCategory.metaDescription } />
						<meta property="og:type" content="website" />
						<meta property="og:title" content={ activeCategory.title || activeCategory.name } />
						<meta property="og:description" content={ activeCategory.ogDescription } />
						<meta property="og:url" content={ makeCatLink(categories) } />
						<link rel="canonical" href={ makeCatLink(categories) } />
					</Helmet>
				)
			}

			<h1>{ activeCategory ? activeCategory.name : 'Wszystkie kategorie' }</h1>

			<div className="count">
				Aktywne ogłoszenia: { activeCategory ? activeCategory.activeAuctions : categoryCount }
			</div>

			<div className="description">
				{ activeCategory ? activeCategory.description : ALL_CATEGORY_DESCRIPTION }
			</div>
		</div>
	);
}

class CategoryFilters extends Component {
	constructor(props) {
		super(props);

 		const
 			initialCat 		= props.category || null,
 			initialCatIndex = initialCat ? props.categories.map(cat => cat.name).indexOf(initialCat) : null;

 		// this.initialState = { marka: null, category: null, subcategory: null, subsubcategory: null, categoryIndex: null, subcategoryIndex: null, subsubcategoryIndex: null, data: {} };
		// this.state = { marka: null, category: initialCat, subcategory: null, subsubcategory: null, categoryIndex: initialCatIndex, subcategoryIndex: null, subsubcategoryIndex: null, data: {} };

		this.initialState = {
			marka: null,
			categories: [],
			indexes: [],
			data: {}
		};

		this.state = {
			marka: null,
			categories: initialCat ? [ initialCat ] : [],
			//indexes: initalCat ? [ initialCatIndex ] : [],
			data: {}
		};

		this.handleInput = this.handleInput.bind(this);
		this.setCategory = this.setCategory.bind(this);
		this.navigateBack = this.navigateBack.bind(this);
		this.sendData = this.sendData.bind(this);
		this.dataToDisplay = this.dataToDisplay.bind(this);
		// this.properties = this.properties.bind(this);
		this.onFilterButtonPress = this.onFilterButtonPress.bind(this);
	}

	componentWillReceiveProps(props) {
		const { category } = props;

		if (category && category !== this.state.category) {
			if (category !== 'Kategorie' && category !== 'Szukaj Sprzedawcy') {
				// this.setState({ category, categoryIndex: props.categories.map(cat => cat.name).indexOf(category) });
				this.setState({
					categories: [ category ],
					//indexes: [ props.categories.findIndex(cat => cat.name === category) ]
				})
			} else {
				// this.setState({ category: null, categoryIndex: null });
				this.setState({ categories: [], indexes: [] });
			}
		}

		if (props.categoryData) {
			const { categoryData } = props;

			if (arraysAreDifferent(categoryData.categories, this.state.categories)) {
				let categories = categoryData.categories;

				if (categories && categories.length)
					this.setState({ categories });
			}
		}
	}

	setCategory(categories, name, level) {
		let newCategories = categories.slice(0, level + 1);
		newCategories[level] = name;

		// let newCategories = categories.concat(name);

		this.setState({ categories: newCategories }, this.sendData);

		// switch(which) {
		// 	case 'category':
		// 		this.setState({ category: name, categoryIndex: index, subcategory: null, subcategoryIndex: null, subsubcategory: null, subsubcategoryIndex: null, data: {} }, this.sendData);
		// 		break;
		// 	case 'subcategory':
		// 		this.setState({ subcategory: name, subcategoryIndex: index, subsubcategory: null, subsubcategoryIndex: null, data: {} }, this.sendData);
		// 		break;
		// 	case 'subsubcategory':
		// 		this.setState({ subsubcategory: name, subsubcategoryIndex: index, data: {} }, this.sendData);
		// }
	}

	navigateBack(level) {
		if (level === 'home') this.setState(this.initialState, this.sendData);
		else {
			this.setState(
				({ categories }) => ({ categories: categories.slice(0, level + 1), data: {} }),
				this.sendData
			);
		}

		// switch(level) {
		// 	case 'home':
		// 		this.setState(this.initialState, this.sendData);
		// 		break;
		// 	case 'category':
		// 		this.setState({ subcategory: null, subcategoryIndex: null, subsubcategory: null, subsubcategoryIndex: null, data: {} }, this.sendData);
		// 		break;
		// 	case 'subcategory':
		// 		this.setState({ subsubcategory: null, subsubcategoryIndex: null, data: {} }, this.sendData);
		// 		break;
		// }
	}

	handleInput(event) {
		const
			data 	= this.state.data,
			target	= event.target,
			type	= target.type,
			name 	= target.name,
			value	= type === 'checkbox' ? target.checked : target.value;

		data[name] = value;

		this.setState({ data }, this.sendData);
	}

	sendData() {
		const
			{ categoryCallback } = this.props,
			{ data, categories } = this.state;
			// { data, category, subcategory, subsubcategory } = this.state;

		// let categories = [];
		// if (category) categories.push(category);
		// if (subcategory) categories.push(subcategory);
		// if (subsubcategory) categories.push(subsubcategory);

		// data.category = category;
		// data.subcategory = subcategory;
		// data.subsubcategory = subsubcategory;
		data.categories = categories;
		data.time = new Date().getTime(); // czas jest unikatowym parametrem identyfikującym wysłany zestaw właściwości (każdy przetwarzany ma być tylko raz)
		categoryCallback(data);

		if (data['Marka'] && data['Marka'] !== this.state.marka) {
			this.setState({ marka: data['Marka'] });
		}
	}

	dataToDisplay() {
		const { categories } = this.state;

		let
			dataToDisplay = this.props.categories,
			properties = dataToDisplay.properties || [],
			activeCategory = null;

		if (!categories.length) return { dataToDisplay, properties: [...DEFAULT_PROPS], activeCategory };

		for (let i = 0; i < categories.length; i++) {
			// if (!Array.isArray(dataToDisplay))
			// 	dataToDisplay = dataToDisplay.subcategories;
			let
				name = categories[i],
				index = dataToDisplay.findIndex(c => c.name === name);

			if (index >= 0) {
				activeCategory = dataToDisplay[ index ];
				dataToDisplay = dataToDisplay[ index ];
				if (dataToDisplay.properties)
					properties = properties.concat(dataToDisplay.properties);

				if (dataToDisplay.subcategories)
					dataToDisplay = dataToDisplay.subcategories;
			}
		}

		if (!properties.find(prop => prop.name === 'Cena')) {
			properties = DEFAULT_PROPS.concat(properties);
		}

		return { dataToDisplay, properties, activeCategory };
	}

	// properties(set) {
	// 	if (set == null) return DEFAULT_PROPS;
	//
	// 	if (set.hasOwnProperty('properties'))
	// 		return set.properties
	// 	else
	// 		return DEFAULT_PROPS;
	// }

	onFilterButtonPress() {
		if (this.props.filterButtonObserver)
			this.props.filterButtonObserver.trigger(FILTER_BUTTON_PRESSED);
	}

	render() {
		const
			{ categories } = this.state,
			// conditional props
			{ marka }			= this.state,
			{ windowWidth } = this.props,

			level = categories.length,
			{ dataToDisplay, properties, activeCategory } = this.dataToDisplay();

			// category 	   		= isSet(categoryIndex) ? categories[categoryIndex] : null,
			// subcategories  		= category ? category.subcategories : null,
			// subcategory    		= isSet(subcategoryIndex) ? category.subcategories[subcategoryIndex] : null,
			// subsubcategories	= subcategory && isNotEmpty(subcategory.sub_subcategories) ? subcategory.sub_subcategories : null,
			// subsubcategory 		= isSet(subsubcategoryIndex) ? subcategory.sub_subcategories[subsubcategoryIndex] : null,
			// level 				= isSet(subsubcategoryIndex) ? 'subsubcategory' : isSet(subcategoryIndex) ? 'subcategory' : isSet(categoryIndex) ? 'category' : null,
			// dataToDisplay 		= level === null ? categories : level === 'category' ? subcategories : level === 'subcategory' ? subsubcategories : null,
			// properties 			= subcategory && isNotEmpty(subcategory.properties) ? subcategory.properties : subsubcategory && isNotEmpty(subsubcategory.properties) ? subsubcategory.properties : null;

		const initialCategory = this.props.category;

		return (
			<div className="CategoryFilters">

				<PropertyInputs
					properties={ properties }
					callback={ this.handleInput }
					marka={ marka }
				/>

				<FilterButton onFilterButtonPress={ this.onFilterButtonPress } />

				<CategoryNavigationAlt
					categories={ categories }
					data={ this.props.categories } //dataToDisplay }
					// level={ 0 } //level }
					// callback={ this.setCategory }
					// navigateBack={ this.navigateBack }
					// windowWidth={ windowWidth }
					// activeCategory={ this.props.activeCategory }
				/>

			</div>
		);
	}
}

function FilterButton({ onFilterButtonPress }) {
	return (
		<div className="filter-button-wrapper">
			<div className="filter-button" onClick={ onFilterButtonPress }>
				Szukaj <i className="material-icons">chevron_right</i>
			</div>
		</div>
	);
}

// <Breadcrumbs
// 	navigateBack={ this.navigateBack }
// 	categories={ categories }
// 	activeCategory={ activeCategory }
// 	categoryCount={ activeCategory ? null : this.props.categories.reduce((a, b) => a + b.activeAuctions, 0) }
// />

function scrollPropsIntoView() {
	let view = document.getElementById(SEARCH_PROPERTIES_ID);
	if (view)
		view.scrollIntoView({ behavior: 'smooth', block: 'center' });
}

export { CategoryInfo };
export default CategoryFilters;
