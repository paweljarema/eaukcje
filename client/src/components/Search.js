import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { AUCTIONS_PATH, removeInvalidChars, makeCatLinkRelative } from '../functions/links';
import Observer, { FILTER_BUTTON_PRESSED } from '../helpers/eventHelper';

import './Search.css';

const
  DEFAULT_CATEGORY = 'Kategorie',
  CAT_HELP_SEPARATOR = ' -> ';

function compareText(a, b) {
  if (a < b) return -1;
  if (a > b) return 1;
  return 0;
}

function compareNums(a, b) {
  return b - a;
}

function makeCatLinks(categories) {
  let result = [];

  function indexCat(categories, parents = []) {
    for (let i = 0, l = categories.length; i < l; i++) {
      let
        category = categories[i],
        nParents = parents.concat(category.name);

      if (category.activeAuctions === 0) continue;

      result.push({
        str: nParents.join(CAT_HELP_SEPARATOR),
        val: category.activeAuctions
      });

      if (category.subcategories)
        indexCat(category.subcategories, nParents);
    }
  }

  indexCat(categories);
  return result.sort((a, b) => compareNums(a.val, b.val) || compareText(a.str, b.str));
}

const HintPopularCategories = ({ query, categories, clearQuery }) => {
    if (!query || query.length < 2) return null;

    let
      dictionary = makeCatLinks(categories),
      hints = dictionary
        .filter(link => new RegExp(query, 'i').test(link.str))
        .map(link => link.str)
        .slice(0, 5);

    if (hints.length) {
      return (
        <div className="search-category-helper">
          {
            hints.map((hint, i) => (
              <div key={ hint } className="help">
                <Link
                  to={ makeCatLinkRelative( hint.split(CAT_HELP_SEPARATOR) ) }
                  onClick={ clearQuery }
                >
                  <span className="desc">Kategoria: </span>
                  <span>{ hint }</span>
                </Link>
              </div>
            ))
          }
        </div>
      );
    } else return null;
}

class SearchField extends Component {
    constructor(props) {
        super(props);
        this.state = { category: DEFAULT_CATEGORY, select: false };

        this.openSelect = this.openSelect.bind(this);
        this.showSubcategories = this.showSubcategories.bind(this);
        this.clearCategory = this.clearCategory.bind(this);
        this.handleCategory = this.handleCategory.bind(this);
        this.handleInput = this.handleInput.bind(this);
        this.lookFor = this.lookFor.bind(this);
        this.handleEnter = this.handleEnter.bind(this);
        this.searchLink = this.searchLink.bind(this);
        this.clearQuery = this.clearQuery.bind(this);
    }

    componentDidMount() {
        this.closeSelect = () => this.setState({select: false});
        window.addEventListener('click', this.closeSelect);
    }

    componentWillUnmount() {
        window.removeEventListener('click', this.closeSelect);
    }

    openSelect(event) {
        event.stopPropagation();
        this.setState(prev => ({ select: !prev.select }));
    }

    showSubcategories(event) {
        event.stopPropagation();
        this.handleCategory(event);

        const element = event.target;

        let sibling = element.nextSibling;
        while (sibling && sibling.className.indexOf('child') !== -1) {
            let display = sibling.style.display;
            sibling.style.display = display === 'none' ? 'block' : 'none';

            sibling = sibling.nextSibling;
        }
    }

    clearCategory() {
      this.setState({ category: DEFAULT_CATEGORY });
    }

    handleCategory(event) {
        const element = event.target;
        const value = element.innerHTML === 'Wszystkie kategorie' ? 'Kategorie' : element.innerHTML;
        let categories = [];

        if (value !== 'Kategorie' || value !== 'Szukaj Sprzedawcy')
          categories.push(value);

        // this.props.categoryCallback({ categories });
        this.setState({ category: value });//, this.lookFor);
    }

    handleInput(event) {
        const input = event.target;
        const query = input.value;
        const { category } = this.state;
        let categories = [];

        if (category !== 'Kategorie' && category !== 'Szukaj Sprzedawcy')
          categories.push(category);

        this.props.setQuery(query);
        // this.props.categoryCallback({ categories });
        //this.setState({ query });//, this.lookFor);
    }

    clearQuery() {
      this.props.closeMobile();
      this.props.setQuery('');
      //this.setState({ query: '' });
    }

    handleEnter(event) {
      if (event.key === 'Enter') {
        this.props.closeMobile();
        this.props.searchHandler();

        new Observer().trigger(FILTER_BUTTON_PRESSED);

        this.props.history.push(
          this.searchLink()
        );
      }
    }

    lookFor() {
        const history = this.props.history;

        if (this.inputTimeout) clearTimeout(this.inputTimeout);
        if (this.closeTimeout) clearTimeout(this.closeTimeout);

        this.inputTimeout = setTimeout(() => {
            history.push(`/aukcje/szukaj/${this.state.category}/${this.props.query || '*'}`);
            this.closeTimeout = setTimeout(this.props.searchHandler, 4000);
        }, 300);
    }

    searchLink() {
      return `/${ AUCTIONS_PATH }${ ['Kategorie'].includes(this.state.category) ? '' : '/' + removeInvalidChars(this.state.category) }`;
    }

    render() {
        const { open, categories, searchHandler, closeMobile } = this.props;
        const className = "search-auctions" + (open ? ' open' : '');
        if (categories === null || categories === false)
            return null;

        let all_categories = [];

        categories.map(category => {
            all_categories.push({ type: 'main', name: category.name });
            // all_categories = all_categories.concat(category.subcategories.map(subcategory => ({ type: 'child', name: subcategory.name })));
        });

        const onSearch = () => {
          this.props.closeMobile();
          this.props.searchHandler();
          this.clearCategory();
          new Observer().trigger(FILTER_BUTTON_PRESSED);
        };

        return (
            <div className={className}>
                <div className="inputs">
                    <i className="material-icons">search</i>
                    <span>
                        <input
                          name="item"
                          type="text"
                          value={ this.props.query }
                          placeholder="Czego szukasz?"
                          className="item"
                          onChange={this.handleInput}
                          onKeyPress={this.handleEnter}
                        />

                        <HintPopularCategories
                          query={ this.props.query }
                          categories={ categories }
                          clearQuery={ this.clearQuery }
                        />
                    </span>
                    <span style={{ position: 'relative' }}>
                        <span className="select-value" onClick={ this.openSelect }>{ this.state.category }</span>
                        {
                            this.state.select && (
                              <div className="select">
                                {
                                    all_categories && all_categories
                                        .map((category, i) => (
                                           category.type === 'main'
                                           ?
                                           <div key={'main_' + i} className="main" onClick={this.handleCategory}>{ category.name }</div>
                                           :
                                           <div key={'sub_' + i} className="child" style={{ display: 'none' }} onClick={this.handleCategory}>{ category.name }</div>

                                        ))
                                }

                              </div>
                            )
                        }
                    </span>
                    <div>
                        <Link
                          ref={ (e) => this.searchRef = e }
                          onClick={ onSearch }
                          to={ this.searchLink() }>

                            <button className="search-button">Szukaj</button>
                        </Link>
                    </div>
                </div>
            </div>
        )
    }
}

// <div className="para" onClick={this.handleCategory}>Wszystkie kategorie</div>
// <div className="user-search" onClick={this.handleCategory}>Szukaj Sprzedawcy</div>


function mapCategoryStateToProps({ categories }) {
    return { categories };
}

SearchField = connect(mapCategoryStateToProps)(withRouter(SearchField));
export { SearchField };
