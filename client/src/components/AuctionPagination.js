import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import { DEFAULT, NEW, TITLE, CHEAP, EXPENSIVE, POPULAR, ENDING, PROMOTED } from '../constants/sort';
import './Pagination.css';

const URL_PATTERN = ':path?page=:page&per_page=:per_page';
const validPage = (page, pages) => {
  if (isNaN(page) || page < 1) return 1;
  if (page > pages) return pages;
  return page;
};

class AuctionPagination extends Component {

  url = (page) => {
      const { pages, per_page, location } = this.props;
      const { pathname } = location || {};
      return URL_PATTERN
        .replace(':path', pathname)
        .replace(':page', validPage(page, pages))
        .replace(':per_page', per_page);
  }

  prevPage = () => {
    //onClick={ () => clickHandler(+this.pageRef.value - 1) }
    return this.url(parseInt(this.props.page || 1) - 1);
  }

  nextPage = () => {
    //onClick={ () => clickHandler(+this.pageRef.value + 1) }
    return this.url(parseInt(this.props.page || 1) + 1);
  }

  select = (e) => {
    if (e.target) e.target.select();
  }

  navigate = (e) => {
    //() => { clickHandler(this.pageRef.value) }
    const value = validPage(e.target.value);
    const url = this.url(value);

    clearTimeout(this.navigationTimeout);
    this.navigationTimeout = setTimeout(
      () => this.props.history.push(url),
      600
    );
  }

  componentWillUpdate(nextProps, nextState) {
    if (this.pageRef && nextProps.page && nextProps.page != this.pageRef.value)
      this.pageRef.value = nextProps.page
  }

    render() {
        const
            { page, pages, per_page, sort, clickHandler, sortCallback, perPageCallback } = this.props;

        if (pages < 1)
            return null;

        const
            nextExists = +(page || 1) < pages,
            prevExists = +(page || 1) > 1,
            nextPage = this.nextPage(),
            prevPage = this.prevPage();

        return (
            <div className="pagination-wrapper">
              <Helmet>
                { prevExists && <link rel="prev" href={ prevPage } /> }
                { nextExists && <link rel="next" href={ nextPage } /> }
              </Helmet>
                <div className="pagination">
                    <span className="pagination-sort">
                        <select onChange={ (e) => sortCallback(e.target.value) } value={ (sort || DEFAULT) }>
                            <option>{ DEFAULT }</option>
                            <option>{ PROMOTED }</option>
                            <option>{ NEW }</option>
                            <option>{ TITLE }</option>
                            <option>{ CHEAP }</option>
                            <option>{ EXPENSIVE }</option>
                            <option>{ POPULAR }</option>
                            <option>{ ENDING }</option>
                        </select>
                        <span className="word">po</span>
                        <span>
                            <input type="number" min="1" max="50" step="1" onChange={ (e) => { perPageCallback(e.target.value) }} value={ per_page } />
                            ogłoszeń
                        </span>
                    </span>
                    <span className="pagination-pages">
                        <span className="prev-page">
                          <Link to={ prevPage }>
                            <i className={ "material-icons clickable" + (prevExists ? '' : ' off') }>chevron_left</i>
                          </Link>
                        </span>
                        <span className="change-page">
                            strona
                            <input ref={ (e) => this.pageRef = e } className="no-spinners" type="number" min="1" max={pages} step="1" onChange={ this.navigate } defaultValue={ page || 1 } onClick={ this.select } />
                            <span className="word">na</span>
                            <span className="word">
                                { pages }
                            </span>
                        </span>
                        <span className="next-page">
                          <Link to={ nextPage }>
                            <i className={ "material-icons clickable" + (nextExists ? '' : ' off') }>chevron_right</i>
                          </Link>
                        </span>
                    </span>
                </div>
            </div>
        );
    }
}

AuctionPagination = withRouter(AuctionPagination);
export { AuctionPagination };
