import React from 'react';
import { Swipeable } from 'react-swipeable';
import { concatClasses } from '../../functions/style';

import './GenericCarousel.css';

const
  CLSS = {
    active: 'active',
    prev: 'prev',
    next: 'next'
  },

  CTRL = {
    left: 'left',
    right: 'right'
  };

function animation(keyframes) {
  return `${ keyframes } .8s normal forwards ease-out`;
}

export default class Carousel extends React.Component {
  constructor(props) {
    super(props);
    this.state = { active: 0 };

    this.shift = this.shift.bind(this);
    this.left = this.left.bind(this);
    this.right = this.right.bind(this);
    this.classForItem = this.classForItem.bind(this);
    this.hasItemsToScroll = this.hasItemsToScroll.bind(this);
    this.hasLessThanThreeItems = this.hasLessThanThreeItems.bind(this);
  }

  componentWillUpdate(nextProps) {
    if (nextProps.items.length !== this.props.items.length)
      this.setState({ active: 0 });
  }

  hasItemsToScroll() {
    return this.props.items.length > 1;
  }

  hasLessThanThreeItems() {
    return this.props.items.length < 3;
  }

  shift(delta) {
    const
      { active } = this.state,
      { items } = this.props,

      last = items.length - 1,
      shift = active + delta;

    if (!this.hasItemsToScroll()) return;

    if (shift < 0) this.setState({ active: last });
    else if (shift > last) this.setState({ active: 0 });
    else this.setState({ active: shift });
  }

  right() {
    this.prevAnimation = animation('carousel__to-right');
    this.activeAnimation = animation('carousel__from-left');
    this.nextAnimation = this.hasLessThanThreeItems() ? this.prevAnimation : undefined;
    this.shift(1);

  }

  left() {
    const size = this.props.items.length;

    this.prevAnimation = animation('carousel__to-left');
    this.activeAnimation = animation('carousel__from-right');
    this.nextAnimation = this.hasLessThanThreeItems() ? this.prevAnimation : undefined;
    this.shift(-1);
  }

  classForItem(active, i) {
    const
      { items } = this.props,
      last = items.length - 1;

    if (active === i) return CLSS.active;
    if (active === 0) if (i === last) return CLSS.prev;
    if (active === last) if (i === 0) return CLSS.next;
    if (i === active - 1) return CLSS.prev;
    if (i === active + 1) return CLSS.next;
  }

  animationFor(className) {
    let animation;

    if (className === CLSS.active) animation = this.activeAnimation;
    else if (className === CLSS.prev) animation = this.prevAnimation;
    else if (className === CLSS.next) animation = this.nextAnimation;

    return { animation };
  }

  renderControl(direction) {
    const { items, windowWidth } = this.props;

    if (items.length < 2) return null; // no controls when whole content is on screen

    let className, icon, action;

    switch(direction) {
      case CTRL.left:
        className = CTRL.left;
        icon = 'chevron_left';
        action = this.left;
        break;

      case CTRL.right:
        className = CTRL.right;
        icon = 'chevron_right';
        action = this.right;
        break;
    }

    return (
      <div
        className={ concatClasses("generic-carousel__control", className,  "clickable") }
        onClick={ action }>

        <i className="material-icons">{ icon }</i>
      </div>
    );
  }

  render() {
    const
      { active } = this.state,
      { items, className } = this.props,

      controls = items.length > 1;

    return (
      <Swipeable
				trackMouse
				preventDefaultTouchmoveEvent
				onSwipedLeft={ this.left }
				onSwipedRight={ this.right } >

        <div className={ concatClasses("generic-carousel", className) }>
          {
            this.renderControl(CTRL.left)
          }

          {
            items.map((item, i) => {
              const className = this.classForItem(active, i);
              return (
                <div
                  className={ concatClasses('generic-carousel__wrapper', className) }
                  style={ this.animationFor(className) }
                  key={ 'citem_' + i }
                >

                  { item }
                </div>
              )

            })
          }

          {
            this.renderControl(CTRL.right)
          }
        </div>
      </Swipeable>
    );
  }
};
