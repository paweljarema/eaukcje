import React, { Component } from 'react';

class Page404 extends Component {
    render() {
        return (
            <div style={{ paddingTop: 'calc((100vh - 500px) / 2)'}}>
                <div className="no-result">
                    <i className="material-icons">build</i>
                    <h1>Błąd 404</h1>
                    <p>Nie znaleziono ogłoszenia.</p>
                </div>
            </div>
        )
    }
}

// <code><b>{ this.props.location.pathname }</b></code>

export { Page404 };
