import React from 'react';
import { withRouter } from 'react-router-dom';
import { BUY_PACKETS_PATH } from '../constants/urls';

import './DisplayCredits.css';

export default withRouter(
  ({ user, history }) => {
    if (!user) return null;

    let
      { packets } = user || {},
      { auctions = 0, promos = 0, auctions_unlimited } = packets || {};

    return (
      <a className="dropdown-user-stats" onClick={ () => history.push( BUY_PACKETS_PATH ) }>
        <div className="title">Pozostałe pakiety:</div>
        <div title="kredyty wykupione na wystawianie ogłoszeń">na aukcje: { ( auctions_unlimited ? 'bez limitu' : auctions ) }</div>
        <div title="kredyty wykupione na promowanie ogłoszeń">na promocje: { promos }</div>
      </a>
    );
  }
);
