import axios from 'axios';
import { FETCH_SETTINGS } from './types';

export const fetchSettings = () => async dispatch => {
    const res = await axios.get('/settings');
    dispatch({ type: FETCH_SETTINGS, payload: res.data });
}
