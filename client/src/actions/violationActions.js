import { FETCH_MESSAGE } from './types';
const axios = require('axios');

export const postViolation = data => async dispatch => {
  const res = await axios.post('/violations/report', data);
  const message = await axios.get('/api/flash_message');

  dispatch({ type: FETCH_MESSAGE, payload: message.data });
}
