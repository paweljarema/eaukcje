import axios from 'axios';

export const registerViewViaShortid = shortid => async dispatch => {
	axios.post('/api/register_viewer_via_shortid/' + shortid);
}

export const unregisterViewViaShortid = shortid => async dispatch => {
	axios.post('/api/unregister_viewer_via_shortid/' + shortid);
}

export const addViewViaShortid = shortid => async dispatch => {
	axios.post('/api/add_view_for_via_shortid/' + shortid);
}


// LEGACY
// export const unregisterView = auction_id => async dispatch => {
// 	axios.post(`/api/unregister_viewer/${auction_id}`);
// }
//
// export const addView = auction_id => async dispatch => {
// 	axios.post(`/api/add_view_for/${auction_id}`);
// }
//
// export const registerView = auction_id => async dispatch => {
// 	axios.post(`/api/register_viewer/${auction_id}`);
// }
//
// export const registerViewByTimestamp = timestamp => async dispatch => {
// 	axios.post('/api/register_viewer_by_timestamp/' + timestamp);
// }
//
// export const unregisterViewByTimestamp = timestamp => async dispatch => {
// 	axios.post('/api/unregister_viewer_by_timestamp/' + timestamp);
// }
//
// export const addViewByTimestamp = timestamp => async dispatch => {
// 	axios.post('/api/add_view_for_by_timestamp/' + timestamp);
// }
// end of LEGACY
