import axios from 'axios';
import { FETCH_PHOTOS } from './types';

export const clearPhotos = () => async dispatch => {
	dispatch({ type: FETCH_PHOTOS, payload: false });
}

export const fetchPhotosViaShortid = shortid => async dispatch => {
	const res = await axios.get(`/auction/${ shortid }/photos_via_shortid`);
	dispatch({ type: FETCH_PHOTOS, payload: res.data });
}

// LEGACY
// export const fetchPhotosByTimestamp = timestamp => async dispatch => {
// 	const res = await axios.get(`/auction/${timestamp}/photos_by_timestamp`);
// 	dispatch({ type: FETCH_PHOTOS, payload: res.data });
// }
//
// export const fetchPhotos = (id) => async dispatch => {
// 	const res = await axios.get(`/auction/${id}/photos`);
// 	dispatch({ type: FETCH_PHOTOS, payload: res.data });
// }
// end of LEGACY
