import axios from 'axios';
import { FETCH_MESSAGE } from './types';

export const contactEaukcje = formData => async dispatch => {
	let res;
	try {
		res = await axios.post('/api/contact_eaukcje', formData);
	} catch(e401) {
		// redundant
	}

	const message = await axios.get('/api/flash_message');
	dispatch({ type: FETCH_MESSAGE, payload: message.data });
}
