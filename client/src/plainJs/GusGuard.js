const FIRM_NIP_FIELD = 'gus-guard__nip',
      FIRM_NAME_FIELD = 'gus-guard__firm-name',
      GUS_WRAPPER = 'gus-guard__spinner';

const GUS_API_ENDPOINT = '/gus/report';

export default function() {
  let fetching = false;

  const _toggleFetching = () => {
    fetching = !fetching;

    const wrapper = document.getElementById(GUS_WRAPPER);
    if (fetching && !wrapper.classList.contains('input-fetching'))
      wrapper.classList.add('input-fetching');
    else
      wrapper.classList.remove('input-fetching');
  }

  const _parseDataAndUpdate = (data) => {
    let firm_name = '';

    if (data.error) {
      alert('Nie znaleziono firmy.')

    } else {
      if (data.exists) {
        alert('Konto firmy o tym NIPie już istnieje. Użyj innego NIPu lub skontaktuj się z administratorem.');
      }

      if (data.firm_name) firm_name = data.firm_name;
    }

    let nativeInputValueSetter = Object.getOwnPropertyDescriptor(window.HTMLInputElement.prototype, "value").set;
    const nameField = document.getElementById(FIRM_NAME_FIELD);
    nativeInputValueSetter.call(nameField, firm_name);
    var change = new Event('input', { bubbles: true });
    nameField.dispatchEvent(change);
  }

  const _makeAPIcall = (nip, callback) => {
    if (fetching) return;

    _toggleFetching();
    fetch(GUS_API_ENDPOINT, {
      method: 'POST',
      body: JSON.stringify({ nip }),
      headers: {
        'Content-Type': 'application/json'
      }
    })
    .then(res => res.json())
    .then(data => {
      callback(data);
      _toggleFetching();
    });
  }

  const _nipChanged = (e) => {
    const nip = String(e.target.value).replace(/\D+/g, '').slice(0, 10);
    const field = document.getElementById(FIRM_NIP_FIELD);

    e.target.value = nip;

    if (nip.length === 10)
      _makeAPIcall(nip, _parseDataAndUpdate);

    return e;
  }

  return function nipListener(handleChange) {
    return (e) => {
      handleChange(_nipChanged(e))
    };
  }
}

export {
  FIRM_NIP_FIELD,
  FIRM_NAME_FIELD,
  GUS_WRAPPER
};
