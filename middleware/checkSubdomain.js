const Business = require('../client/src/constants/business');

module.exports = async (req, res, next) => {
  const [ subdomain, subdomain2 ] = req.subdomains;

  if (subdomain && subdomain === 'www') {
    const redirectBase = Business.domain;
    res.redirect(301, redirectBase + req.originalUrl);
    return;
  }

  if (subdomain2 && subdomain2 === 'www') {
    const redirectBase = Business.host.replace(Business.domain, subdomain + '.' + Business.domain);
    res.redirect(301, redirectBase + req.originalUrl);
    return;
  }

  next();
}
