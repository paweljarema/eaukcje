const
  mongoose = require('mongoose'),
  keys = require('./config/keys'),

  seedAll = require('./routes/functions/seedFunction');

mongoose.connect(keys.mongoURI);

seedAll();
